# RapidMiner Streaming Extension

## Changelog

### Version 0.2.0

- 

### Version 0.1.2

- 

### Version 0.1.1

- Added copyright headers to all java source files
- Version not released to Marketplace!

### Version 0.1.0

- Initial (alpha) version of the extension
- Main functionality for the deployment of streaming analytic processes implemented
    - Added the Streaming Nest operator for designing and deploying streaming analytic processes
- Added streaming operators to pull and push from/to Kafka (Kafka Source, Kafka Sink)
- Added operators to pull from Kafka and convert to ExampleSet (Read Kafka Topic) and to convert
 ExampleSet to data events and push to Kafka (Write Kafka Topic)
- Added operators to access the financial data server from SPRING (Get Quote Symbols, Get Quotes
, Get Depths)
- Added streaming operators to perform several basic streaming functionalities (Aggregate Stream
, Duplicate Stream, Join Stream, Connect Stream, Filter Stream, Map Stream, Select Stream, Parse
 Field Stream, Stringify Stream)
- Added integration with the INFORE Optimizer (Streaming optimization operator)
- Added integration with different INFORE Components (Synopsis Data Engine, Athena Online Machine
 Learning Engine, Complex Event Forecasting Engine, Maritime Event Detection) 
