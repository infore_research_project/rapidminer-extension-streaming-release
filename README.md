# RapidMiner Streaming Extension
This extension provides operators that allow RapidMiner users to design and deploy arbitrary streaming jobs.

## Version
Dependency versions throughout the project:

* Flink: 1.9.2 with Scala 2.11
* Spark: 2.4.5 with Scala 2.11
* RapidMiner: 9.5

## Project structure
The project comprises the following modules:
```
rapidminer-extension-streaming (main project src)
flink (module)
spark (module)
utility (module)
```

### Code dependencies
Below graph shows compile time dependencies between sources/modules:
```
rapidminer-extension-streaming (main project src) --
                                                    \
flink (module) ----------------------------------------> utility (module)
                                                    /
spark (module) ------------------------------------
```

### Runtime
At runtime, when we actually launch streaming jobs, the following "dependency" graph exists:
```
                                                       --> flink (module)
                                                     /
rapidminer-extension-streaming (main project src) -------> spark (module)
```

## Modules
### Utility
The *utility* module contains common functionality needed by other modules (e.g.: HDFS, Kryo).
It is also the place where the framework independent stream graph lives.
RapidMiner users interact with the GUI to build processing workflows.
Under the hood, GUI elements, upon process execution, build such a framework independent stream graph for deployment.

### Flink
The *flink* module contains Apache Flink specific code.
It is basically a deployable unit which could almost be forwarded to a Flink cluster as it is.
However, it first needs to be equipped with a framework independent stream graph, which happens at runtime (will be embedded in the JAR copy).

### Spark
The *spark* module contains Apache Spark Streaming (structured) specific code.
It is also almost deployable as it is, i.e. could be forwarded to a Spark cluster for execution.
However, the same way as *flink*, it also needs to be equipped with a framework independent stream graph, which happens at runtime here as well.
In this case, since we are already making use of a 3rd party store (e.g.: HDFS cluster) for JAR distribution, we simply move the serialized graph there too.

## Architecture
From a high level, this is how it looks like:

![High level overview](./doc/high_level_arch.svg?raw=true "High level architecture")

1. User defines data stream processing workflow in RapidMiner Studio (GUI)
2. User starts process execution as usual (big, blue play button)
3. RapidMiner collects workflow nodes, their configurations and builds a graph from them (platform independent)
4. RapidMiner serializes this workflow graph
5. RapidMiner submits platform-specific job (containing the workflow graph processor) + serialized graph
6. Cluster (e.g.: Flink, Spark) receives job (+ artifacts) and starts its execution
7. Platform-specific processor (main entry point for the job) deserializes, translates (into a platform specific representation) and executes workflow

## Job submission
How do we submit jobs ?

### Spark
The so-called _fat-JAR_ is already part of the extension package (see the Spark module section in this document).

1. User configures Spark connection, attaches it to the workflow and runs the process
2. Graph/workflow gets serialized into a temporary file
3. Connection configuration gets merged with the default configuration (```resources/rm-spark-defaults.conf```) and then serialized into a temporary file
4. Module _fat-JAR_ gets a temporary copy into which both temporary files get embedded (==> deployable)
5. The _fat-JAR_ will be copied over to the appropriate HDFS cluster (based on connection configuration)
6. Apache-Livy REST API call to start job

### Flink
The so-called _fat-JAR_ is already part of the extension package (see the Flink module section in this document).

1. User configures Flink connection, attaches it to the workflow and runs the process
2. Graph/workflow gets serialized into a temporary file
3. Module _fat-JAR_ gets a temporary copy into which graph file get embedded (==> deployable)
4. _fat-JAR_ is then uploaded via Flink REST API
5. Job gets started via Flink REST API