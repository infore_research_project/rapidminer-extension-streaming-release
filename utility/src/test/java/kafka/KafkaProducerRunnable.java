package kafka;
//

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;



/**
 * Simple standalone Kafka producer, that can send messages to a specified Kafka cluster and topic.
 * The message format, number of messages and the potential message delay can be specified inside the code
 *
 * @author David Arnu
 */
public class KafkaProducerRunnable {

    private static final  String PARAM_KAFKA_SERVER = "localhost:9092";
    private static final String PARAM_KAFKA_TOPIC = "long_1";
    private static Random random = new Random();

    // Number of messages. If -1 it will run forever
    private static int numberOfMessages = 500_000;

    // can be used to specify a delay in the messages. If not used the messages are send as fast as possible
    private static int messageInterval =1;

    public static void main(String[] args) throws InterruptedException {



        Properties kafkaProps = new Properties();




        kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, PARAM_KAFKA_SERVER);
        kafkaProps.put(ProducerConfig.ACKS_CONFIG, "all");
        kafkaProps.put(ProducerConfig.RETRIES_CONFIG, 0);
        kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        KafkaProducer< String, String > producer = new KafkaProducer < > (kafkaProps);

        int id = 0;
        while(numberOfMessages !=0){


            // select the message type here

            // String message = randomAlphabeticString(40);
            //  String message = String.valueOf(random.nextInt());
            String message = randomClassifier(5);

            String key = String.valueOf(id);
            id++;

            ProducerRecord<String, String> data = new ProducerRecord<>(PARAM_KAFKA_TOPIC, key, message);

            producer.send(data);

            if(numberOfMessages%1000==0) {
                System.out.println("Messages remaining: " + numberOfMessages + " ; Message send: " + message);
            }

            // uncomment to set a delay between each message
            //  TimeUnit.SECONDS.sleep(messageInterval);
            //  TimeUnit.MILLISECONDS.sleep(messageInterval);

            numberOfMessages--;
        }

        System.out.println("Done");
        producer.flush();
        producer.close();



    }

    /**
     * Generates a random string of the specified length
     * @param length length of the string
     * @return
     */
    private static String randomAlphabeticString(int length) {
        int leftLimit = 48; // "0"
        int rightLimit = 122; // letter 'z'
        int targetStringLength = length;


        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return  generatedString;


    }


    /**
     * creates a comma separated list of random integers
     * @param numberOAttributes
     * @return
     */
    private static String randomClassifier (int numberOAttributes) {


        String data = String.valueOf(random.nextInt());


        while (numberOAttributes >0) {
            data = data+","+String.valueOf(random.nextInt());
            numberOAttributes--;

        }
        return data;

    }


}
