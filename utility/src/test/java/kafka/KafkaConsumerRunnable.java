package kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 *
 * Simple standalone Kafka consumer that can read messages from a specified server and topic.
 * The Offset strategy is set to earliest, so it will always read all available messages of this topic.
 *
 * @author David Arnu
 */
public class KafkaConsumerRunnable {

    private static final  String PARAM_KAFKA_SERVER = "localhost:9092";
    private static final String PARAM_KAFKA_TOPIC = "long_1";


    public static void main(String[] args) throws InterruptedException {
        Properties clusterConfig = new Properties();
        clusterConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, PARAM_KAFKA_SERVER);
        clusterConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        clusterConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        clusterConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        clusterConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        ;
        // Group_ID as parameter?
        clusterConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "RapidMinerKafkaExampleConsumer");

        KafkaConsumer<Object, Object> kafkaConsumer = new KafkaConsumer<>(clusterConfig);
        TestConsumerRebalanceListener rebalanceListener = new TestConsumerRebalanceListener();
        kafkaConsumer.subscribe(Collections.singletonList(PARAM_KAFKA_TOPIC), rebalanceListener);


        // the poll duration is simply the timeout duration
        Duration dur = Duration.ofSeconds(10);

        ConsumerRecords<Object, Object> records = kafkaConsumer.poll(dur);
        List<PartitionInfo> partitions = kafkaConsumer.partitionsFor(PARAM_KAFKA_TOPIC);

        long maxOffset = 0;

        for (PartitionInfo part : partitions) {


            int partition = part.partition();
            long endPosition = kafkaConsumer.position(new TopicPartition(PARAM_KAFKA_TOPIC, partition));


            long offSet = endPosition - 20;
            maxOffset = offSet;

            kafkaConsumer.seek(new TopicPartition(PARAM_KAFKA_TOPIC, partition), offSet);

        }
        clusterConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");


        KafkaConsumer<Object, Object> kafkaConsumer_2 = new KafkaConsumer<>(clusterConfig);
        kafkaConsumer_2.subscribe(Collections.singletonList(PARAM_KAFKA_TOPIC), rebalanceListener);
        try {
            kafkaConsumer_2.seek(new TopicPartition(PARAM_KAFKA_TOPIC, 0), maxOffset);
            ConsumerRecords<Object, Object> records_2 = kafkaConsumer_2.poll(dur);

            Iterator<ConsumerRecord<Object, Object>> iter = records_2.iterator();

            int i = 0;
            while (iter.hasNext()) {
                ConsumerRecord<Object, Object> rec = iter.next();
                String value = rec.value().toString();
                System.out.println(value);
                i++;
            }
            System.out.println(i);
        } catch (Exception e) {
            e.printStackTrace();
        }


        // important to close the connection when done
        kafkaConsumer.close();
    }

    private static class TestConsumerRebalanceListener implements ConsumerRebalanceListener {
        @Override
        public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            System.out.println("Called onPartitionsRevoked with partitions:" + partitions);
        }

        @Override
        public void onPartitionsAssigned(Collection < TopicPartition > partitions) {
            System.out.println("Called onPartitionsAssigned with partitions:" + partitions);
        }
    }
}

