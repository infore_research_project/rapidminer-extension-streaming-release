/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.utility.api.infore.maritime;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * MarineTraffic (Akka) cluster request for job initiation
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class Request implements Serializable {

	@JsonProperty("kafka")
	private final KafkaConfiguration kafka;

	Request() {
		this(null, null);
	}

	public Request(TopicConfiguration in, TopicConfiguration out) {
		this.kafka = new KafkaConfiguration(in, out);
	}

}