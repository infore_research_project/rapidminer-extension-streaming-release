/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.utility;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;


/**
 * Utility for handling HDFS files
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class HdfsHandler {

	private final URI uri;

	private final Configuration configuration;

	public static final String HDFS_PATH_SEPARATOR = "/";

	public HdfsHandler(String uri, Configuration configuration) throws URISyntaxException {
		this.uri = new URI(uri);
		this.configuration = configuration;
	}

	/**
	 * Copies local file to HDFS cluster
	 *
	 * @param src local file path
	 * @param dst targeted HDFS path to copy to
	 * @return HDFS path for new file
	 * @throws IOException
	 */
	public String copyLocalFile(File src, String dst) throws IOException {
		try (final DistributedFileSystem dfs = new DistributedFileSystem()) {
			dfs.initialize(uri, configuration);

			Path srcPath = new Path(src.getPath());
			Path dstPath = new Path(dst);

			// Ensure target directory
			dfs.mkdirs(dstPath);

			// Copy from local path to HDFS
			dfs.copyFromLocalFile(srcPath, dstPath);

			return uri
				.resolve(dst)
				.resolve(src.getName())
				.toString();
		}
	}

	/**
	 * Copies file from HDFS cluster to local file
	 *
	 * @param src source HDFS path to copy from
	 * @param dst local file path
	 * @return local path to new file
	 * @throws IOException
	 */
	public String copyFileToLocal(String src, File dst) throws IOException {
		try (final DistributedFileSystem dfs = new DistributedFileSystem()) {
			dfs.initialize(uri, configuration);

			// Ensure target directory
			dst.mkdirs();

			// Copy from HDFS to local path
			dfs.copyToLocalFile(new Path(src), new Path(dst.getPath()));

			return dst.getAbsolutePath();
		}
	}

	/**
	 * Created directory structure on HDFS cluster
	 *
	 * @param dst HDFS path to create
	 * @return HDFS path to new directory
	 * @throws IOException
	 */
	public String makeDirs(String dst) throws IOException {
		try (final DistributedFileSystem dfs = new DistributedFileSystem()) {
			dfs.initialize(uri, configuration);

			dfs.mkdirs(new Path(dst));

			return uri.resolve(dst).toString();
		}
	}

}