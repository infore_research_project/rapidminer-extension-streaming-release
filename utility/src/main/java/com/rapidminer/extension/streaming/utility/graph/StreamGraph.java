/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.utility.graph;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import com.google.common.collect.Lists;


/**
 * Workflow representation (graph of nodes)
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamGraph {

	private final String name;

	private final AtomicLong id_counter = new AtomicLong(-1);

	private final List<StreamSource> sources = Lists.newArrayList();

	public StreamGraph() {
		this("StreamGraph");
	}

	public StreamGraph(String name) {
		this.name = name;
	}

	/**
	 * @return new, unique ID for this workflow
	 */
	public long getNewId() {
		return id_counter.incrementAndGet();
	}

	/**
	 * Registers the source
	 *
	 * @param source
	 */
	public void registerSource(StreamSource source) {
		sources.add(source);
	}

	/**
	 * @return collection of sources
	 */
	public List<StreamSource> getSources() {
		return Collections.unmodifiableList(sources);
	}

	/**
	 * @return name of this workflow
	 */
	public String getName() {
		return name;
	}

}