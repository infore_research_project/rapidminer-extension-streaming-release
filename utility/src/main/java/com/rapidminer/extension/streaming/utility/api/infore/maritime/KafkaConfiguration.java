/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.utility.api.infore.maritime;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Maritime specific configuration for interfacing with Kafka
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class KafkaConfiguration {

	@JsonProperty("in")
	private final TopicConfiguration in;

	@JsonProperty("out")
	private final TopicConfiguration out;

	KafkaConfiguration() {
		this(null, null);
	}

	public KafkaConfiguration(TopicConfiguration in, TopicConfiguration out) {
		this.in = in;
		this.out = out;
	}

}