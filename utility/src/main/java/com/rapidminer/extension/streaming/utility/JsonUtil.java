/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.utility;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Utility class responsible for JSON related actions
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public final class JsonUtil {
	
	private static final ObjectMapper JSON_MAPPER = new ObjectMapper();
	
	private JsonUtil() {
		// Utility
	}
	
	/**
	 * Using an internal ObjectMapper instance this method takes care of JSON serialization of any
	 * object.
	 *
	 * @param obj
	 * 		to be transformed into JSON
	 * @return JSON string equivalent of the input parameter OR null if an error occurs
	 */
	public static String toJson(Object obj) {
		try {
			return JSON_MAPPER.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			throw new IllegalStateException("Could not convert to JSON: " + obj, e);
		}
	}
	
	/**
	 * Using an internal {@link ObjectMapper} instance this method takes care of the JSON
	 * deserialization of an provided object. The {@code valueTye} of the class in which the {@code
	 * jsonString} shall be converted has to be provided as well
	 *
	 * @param jsonString
	 * 		JSON string to be deserialized
	 * @param valueTye
	 * 		Class type in which the JSON string shall be converted
	 * @param <T>
	 * 		Object class in which the JSON string is converted
	 * @return deserialized instance of type &lt;T&gt; of the provided {@code jsonString}
	 * @throws IOException
	 * 		if the deserialization went wrong
	 */
	public static <T> T jsonStringToObject(String jsonString, Class<T> valueTye) throws
			IOException {
		return JSON_MAPPER.readValue(jsonString, valueTye);
	}
	
	/**
	 * Using an internal {@link ObjectMapper} instance this method takes care of JSON serialization
	 * of any object. The returned JSON string is thereby formatted with the default pretty printer
	 * of the {@link ObjectMapper} (see {@link ObjectMapper#writerWithDefaultPrettyPrinter()}.
	 *
	 * @param object
	 * 		to be transformed into JSON
	 * @return JSON string equivalent of the input parameter
	 * @throws JsonProcessingException
	 * 		if the object can not be converted into a JSON string
	 */
	public static String objectToJsonString(Object object) throws JsonProcessingException {
		return JSON_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
	}
	
}