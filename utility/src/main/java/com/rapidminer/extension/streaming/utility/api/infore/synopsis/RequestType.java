/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.utility.api.infore.synopsis;

import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Available synopsis request types for SDE
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public enum RequestType {

	ADD(1),
	REMOVE(2),
	ESTIMATE(3),
	CONTINUOUS(5);

	private final int id;

	RequestType(int id) {
		this.id = id;
	}

	@JsonValue
	public int getId() {
		return id;
	}

}