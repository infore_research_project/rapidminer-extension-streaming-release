/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.optimizer.settings;

import java.util.ArrayList;
import java.util.List;

import com.rapidminer.extension.streaming.operator.StreamAggregate;
import com.rapidminer.extension.streaming.operator.StreamConnect;
import com.rapidminer.extension.streaming.operator.StreamDuplicate;
import com.rapidminer.extension.streaming.operator.StreamFilter;
import com.rapidminer.extension.streaming.operator.StreamJoin;
import com.rapidminer.extension.streaming.operator.StreamKafkaSink;
import com.rapidminer.extension.streaming.operator.StreamKafkaSource;
import com.rapidminer.extension.streaming.operator.StreamMap;
import com.rapidminer.extension.streaming.operator.StreamParseField;
import com.rapidminer.extension.streaming.operator.StreamSelect;
import com.rapidminer.extension.streaming.operator.StreamStringifyField;
import com.rapidminer.extension.streaming.operator.athena.StreamAthenaOMLOperator;
import com.rapidminer.extension.streaming.operator.athena.StreamSDEOperator;
import com.rapidminer.extension.streaming.operator.demokritos.StreamCEFOperator;
import com.rapidminer.extension.streaming.operator.marinetraffic.StreamMaritimeEventDetectionOperator;
import com.rapidminer.operator.IOMultiplier;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorCreationException;
import com.rapidminer.operator.io.RepositorySource;
import com.rapidminer.tools.OperatorService;


/**
 * This is a container class holding the dictionary of operators available for optimization by the
 * INFORE optimizer. The method {@link #create()} provides the functionality to create such a
 * default {@link OperatorDictionary} for the current (version 0.1.0) streaming operators.
 *
 * @author Fabian Temme
 * @since 0.1.0
 */
public class OperatorDictionary {
	
	private String dictionaryName = null;
	private List<DictOperator> operators = null;
	
	/**
	 * Creates a new {@link OperatorDictionary} instance with the fields set to default values.
	 */
	public OperatorDictionary() {}
	
	/**
	 * Returns the name of this {@link OperatorDictionary} configuration.
	 *
	 * @return name of this {@link OperatorDictionary} configuration
	 */
	public String getDictionaryName() {
		return dictionaryName;
	}
	
	/**
	 * Sets name of this {@link OperatorDictionary} configuration to the provided one. Returns
	 * itself, so that set methods can be chained.
	 *
	 * @param dictionaryName
	 * 		new name of this {@link OperatorDictionary} configuration
	 * @return this {@link OperatorDictionary}
	 */
	public OperatorDictionary setDictionaryName(String dictionaryName) {
		this.dictionaryName = dictionaryName;
		return this;
	}
	
	/**
	 * Returns the list of {@link DictOperator}s of this {@link OperatorDictionary} configuration.
	 *
	 * @return list of {@link DictOperator}s of this {@link OperatorDictionary} configuration
	 */
	public List<DictOperator> getOperators() {
		return operators;
	}
	
	/**
	 * Sets list of {@link DictOperator}s of this {@link OperatorDictionary} configuration to the
	 * provided one. Returns itself, so that set methods can be chained.
	 *
	 * @param operators
	 * 		new list of {@link DictOperator}s of this {@link OperatorDictionary} configuration
	 * @return this {@link OperatorDictionary}
	 */
	public OperatorDictionary setOperators(List<DictOperator> operators) {
		this.operators = operators;
		return this;
	}
	
	/**
	 * Creates a default {@link OperatorDictionary} configuration for the current (version 0.1.0)
	 * streaming operators.
	 * <p>
	 * The method loops through the {@link Operator}s returned by {@link
	 * #createAllStreamingOperators()}. For each Operator a {@link DictOperator} is created, with
	 * the classKey of the Operator and a {@link DictPlatforms} instance with entries for flink, and
	 * spark. For the {@link StreamMap} operator only a spark entry is added, for the {@link
	 * StreamFilter} operator, only an entry for flink is added. This means the dictionary describes
	 * all operators to have available implementations in flink and spark, except for {@link
	 * StreamMap} and {@link StreamFilter}.
	 * <p>
	 * The name of the {@link OperatorDictionary} configuration is set to the provided {@code
	 * name}.
	 *
	 * @return {@link OperatorDictionary} configuration for the current (version 0.1.0) streaming
	 * operators
	 * @throws OperatorCreationException
	 */
	public static OperatorDictionary create(String name) throws OperatorCreationException {
		List<DictOperator> dictOperators = new ArrayList<>();
		for (Operator operator : createAllStreamingOperators()) {
			
			String classKey = operator.getOperatorDescription().getKey();
			DictPlatforms dictPlatforms = new DictPlatforms();
			if (!( operator instanceof StreamMap )) {
				dictPlatforms.setFlink(new DictPlatform().setOperatorName("flink_" + classKey));
			}
			if (!( operator instanceof StreamFilter )) {
				dictPlatforms.setSpark(new DictPlatform().setOperatorName("spark_" + classKey));
			}
			dictOperators.add(new DictOperator().setClassKey(classKey).setPlatforms(dictPlatforms));
		}
		return new OperatorDictionary().setDictionaryName(name).setOperators(dictOperators);
	}
	
	/**
	 * Returns the list of the current operators available for optimization.
	 *
	 * <ul>
	 *     <li>{@link StreamAggregate}</li>
	 *     <li>{@link StreamConnect}</li>
	 *     <li>{@link StreamDuplicate}</li>
	 *     <li>{@link StreamFilter}</li>
	 *     <li>{@link StreamJoin}</li>
	 *     <li>{@link StreamKafkaSink}</li>
	 *     <li>{@link StreamKafkaSource}</li>
	 *     <li>{@link StreamMap}</li>
	 *     <li>{@link StreamParseField}</li>
	 *     <li>{@link StreamSelect}</li>
	 *     <li>{@link StreamStringifyField}</li>
	 *     <li>{@link StreamAthenaOMLOperator}</li>
	 *     <li>{@link StreamSDEOperator}</li>
	 *     <li>{@link StreamCEFOperator}</li>
	 *     <li>{@link StreamMaritimeEventDetectionOperator}</li>
	 *     <li>{@link IOMultiplier}</li>
	 *     <li>{@link RepositorySource}</li>
	 * </ul>
	 *
	 * @return list of the current operators available for optimization
	 * @throws OperatorCreationException
	 */
	private static List<Operator> createAllStreamingOperators() throws OperatorCreationException {
		List<Operator> result = new ArrayList<>();
		
		result.add(OperatorService.createOperator(StreamAggregate.class));
		result.add(OperatorService.createOperator(StreamConnect.class));
		result.add(OperatorService.createOperator(StreamDuplicate.class));
		result.add(OperatorService.createOperator(StreamFilter.class));
		result.add(OperatorService.createOperator(StreamJoin.class));
		result.add(OperatorService.createOperator(StreamKafkaSink.class));
		result.add(OperatorService.createOperator(StreamKafkaSource.class));
		result.add(OperatorService.createOperator(StreamMap.class));
		result.add(OperatorService.createOperator(StreamParseField.class));
		result.add(OperatorService.createOperator(StreamSelect.class));
		result.add(OperatorService.createOperator(StreamStringifyField.class));
		
		result.add(OperatorService.createOperator(StreamAthenaOMLOperator.class));
		result.add(OperatorService.createOperator(StreamSDEOperator.class));
		result.add(OperatorService.createOperator(StreamCEFOperator.class));
		result.add(OperatorService.createOperator(StreamMaritimeEventDetectionOperator.class));
		
		result.add(OperatorService.createOperator(IOMultiplier.class));
		result.add(OperatorService.createOperator(RepositorySource.class));
		
		return result;
	}
	
}
