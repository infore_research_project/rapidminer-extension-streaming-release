/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.optimizer.settings;

import java.util.Arrays;


/**
 * This is a container class for the request send to the INFORE Optimizer Service
 *
 * @author Fabian Temme
 * @since 0.1.0
 */
public class OptimizerRequest {
	
	/**
	 * This enum provides the currently supported versions of the INFORE optimizer.
	 *
	 * @author Fabian Temme
	 * @since 0.1.0
	 */
	public enum OptimizerVersion {
		VERSION_1_0("1.0", "1.0");
		
		private final String description;
		private final String key;
		
		OptimizerVersion(String description, String key) {
			this.description = description;
			this.key = key;
		}
		
		/**
		 * Returns a descriptive name of the version, which can be used as the value in the
		 * parameter category.
		 *
		 * @return descriptive name of the version, which can be used as the value in the *
		 * parameter category
		 */
		public String getDescription() {
			return description;
		}
		
		/**
		 * Returns the key used by the INFORE optimizer for this version.
		 *
		 * @return key used by the INFORE optimizer for this version
		 */
		public String getKey() {
			return key;
		}
		
		/**
		 * Returns the {@link OptimizerVersion} for which {@link OptimizerVersion#toString()} is
		 * equal to the provided String.
		 *
		 * @param text
		 * 		String which is checked against {@link OptimizerVersion#toString()}.
		 * @return {@link OptimizerVersion} for which {@link OptimizerVersion#toString()} is equal
		 * to the provided String
		 * @throws IllegalArgumentException
		 * 		if provided String is not equal to any {@link OptimizerVersion#toString()} method
		 * 		calls
		 */
		public static OptimizerVersion getMethod(String text) {
			for (OptimizerVersion method : OptimizerVersion.values()) {
				if (method.description.equals(text)) {
					return method;
				}
			}
			throw new IllegalArgumentException(
					"Provided text (" + text + ") does not match with an OptimizerVersion. " + "Allowed texts are: " + Arrays
							.toString(OptimizerAlgorithm.values()));
		}
	}
	
	/**
	 * This enum provides the currently supported optimization algorithms of the INFORE optimizer.
	 *
	 * @author Fabian Temme
	 * @since 0.1.0
	 */
	public enum OptimizerAlgorithm {
		/**
		 * fast but with large memory footprint
		 */
		A_STAR("a*", "a*"),
		/**
		 * slow but optimal
		 */
		EXHAUSTIVE("exhaustive", "exhaustive"),
		/**
		 * Fast but approximate solutions
		 */
		GREEDY("greedy", "greedy"),
		/**
		 * Automatic (Recommended)
		 */
		AUTO("automatic", "auto");
		
		private final String description;
		private final String key;
		
		OptimizerAlgorithm(String description, String key) {
			this.description = description;
			this.key = key;
		}
		
		/**
		 * Returns a descriptive name of the algorithm, which can be used as the value in the
		 * parameter category.
		 *
		 * @return descriptive name of the algorithm, which can be used as the value in the
		 * parameter category
		 */
		public String getDescription() {
			return description;
		}
		
		/**
		 * Returns the key used by the INFORE optimizer for this algorithm.
		 *
		 * @return key used by the INFORE optimizer for this algorithm
		 */
		public String getKey() {
			return key;
		}
		
		/**
		 * Returns the {@link OptimizerAlgorithm} for which {@link OptimizerAlgorithm#toString()} is
		 * equal to the provided String.
		 *
		 * @param text
		 * 		String which is checked against {@link OptimizerAlgorithm#toString()}.
		 * @return {@link OptimizerAlgorithm} for which {@link OptimizerAlgorithm#toString()} is
		 * equal to the provided String
		 * @throws IllegalArgumentException
		 * 		if provided String is not equal to any {@link OptimizerAlgorithm#toString()} method
		 * 		calls
		 */
		public static OptimizerAlgorithm getMethod(String text) {
			for (OptimizerAlgorithm method : OptimizerAlgorithm.values()) {
				if (method.description.equals(text)) {
					return method;
				}
			}
			throw new IllegalArgumentException(
					"Provided text (" + text + ") does not match with an OptimizerAlgorithm. " + "Allowed texts are: " + Arrays
							.toString(OptimizerAlgorithm.values()));
		}
	}
	
	private String id = null;
	
	private String networkName = null;
	
	private String dictionaryName = null;
	
	private String workflowName = null;
	
	private String version = null;
	
	private String algorithm = null;
	
	private Long parallelism = 0L;
	
	private String description = null;
	
	private long timeout_ms = 0;
	
	/**
	 * Creates a new {@link OptimizerRequest} instance with the fields set to default values.
	 */
	public OptimizerRequest() {
	}
	
	/**
	 * Returns the id of the {@link OptimizerRequest}.
	 *
	 * @return id of the {@link OptimizerRequest}
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id of the {@link OptimizerRequest} to the provided one. Returns itself, so that set
	 * methods can be chained.
	 *
	 * @param id
	 * 		id of the {@link OptimizerRequest}
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setId(String id) {
		this.id = id;
		return this;
	}
	
	/**
	 * Returns the name of the network to be used.
	 *
	 * @return name of the network to be used
	 */
	public String getNetworkName() {
		return networkName;
	}
	
	/**
	 * Sets the name of the network to be used to the provided one. Returns itself, so that set
	 * methods can be chained.
	 *
	 * @param networkName
	 * 		name of the network to be used
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setNetworkName(String networkName) {
		this.networkName = networkName;
		return this;
	}
	
	/**
	 * Returns the name of the dictionary to be used.
	 *
	 * @return name of the dictionary to be used
	 */
	public String getDictionaryName() {
		return dictionaryName;
	}
	
	/**
	 * Sets the name of the dictionary to be used to the provided one. Returns itself, so that set
	 * methods can be chained.
	 *
	 * @param dictionaryName
	 * 		name of the dictionary to be used
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setDictionaryName(String dictionaryName) {
		this.dictionaryName = dictionaryName;
		return this;
	}
	
	/**
	 * Returns the name of the workflow to be used.
	 *
	 * @return name of the workflow to be used
	 */
	public String getWorkflowName() {
		return workflowName;
	}
	
	/**
	 * Sets the name of the workflow to be used to the provided one. Returns itself, so that set
	 * methods can be chained.
	 *
	 * @param workflowName
	 * 		name of the workflow to be used
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
		return this;
	}
	
	/**
	 * Returns the version of the optimizer to be used.
	 *
	 * @return version of the optimizer to be used
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * Sets the version of the optimizer to be used to the provided one. Returns itself, so that set
	 * methods can be chained.
	 *
	 * @param version
	 * 		version of the optimizer to be used
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setVersion(String version) {
		this.version = version;
		return this;
	}
	
	/**
	 * Returns the name of the optimization algorithm of the optimizer to be used.
	 *
	 * @return name of the optimization algorithm of the optimizer to be used
	 */
	public String getAlgorithm() {
		return algorithm;
	}
	
	/**
	 * Sets the name of the optimization algorithm of the optimizer to be used to the provided one.
	 * Returns itself, so that set methods can be chained.
	 *
	 * @param algorithm
	 * 		name of the optimization algorithm of the optimizer to be used
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
		return this;
	}
	
	/**
	 * Returns the name of the cost estimation algorithm of the optimizer to be used.
	 *
	 * @return name of the cost estimation algorithm of the optimizer to be used
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the name
	 *
	 * @param description
	 * 		name
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setDescription(String description) {
		this.description = description;
		return this;
	}
	
	/**
	 * Returns the parallelism level of the optimizer to be used.
	 *
	 * @return parallelism level of the optimizer to be used
	 */
	public Long getParallelism() {
		return parallelism;
	}
	
	/**
	 * Sets the parallelism level of the optimizer to be used to the provided one. Returns itself,
	 * so that set methods can be chained.
	 *
	 * @param parallelism
	 * 		parallelism level of the optimizer to be used
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setParallelism(Long parallelism) {
		this.parallelism = parallelism;
		return this;
	}
	
	/**
	 * Returns the timeout of the optimizer.
	 *
	 * @return timeout of the optimizer
	 */
	public long getTimeout_ms() {
		return timeout_ms;
	}
	
	/**
	 * Sets the timeout of the optimizer to the provided one. Returns itself, so that set methods
	 * can be chained.
	 *
	 * @param timeout_ms
	 * 		timeout of the optimizer
	 * @return this {@link OptimizerRequest}
	 */
	public OptimizerRequest setTimeout_ms(long timeout_ms) {
		this.timeout_ms = timeout_ms;
		return this;
	}
	
	/**
	 * Creates an {@link OptimizerRequest} configuration with the provided values (and some
	 * hardcoded values, which could be changed after by using the corresponding set methods). The
	 * hardcoded values are
	 *
	 * <ul>
	 *     <li>parallelism level: 1.0</li>
	 *     <li>timeout: 10000 ms</li>
	 *     <li>description: "" (empty string)</li>
	 * </ul>
	 *
	 * @return dummy {@link OptimizerRequest} configuration with hardcoded values
	 */
	public static OptimizerRequest create(String id, String networkName, String dictionaryName,
			String workflowName, OptimizerVersion version, OptimizerAlgorithm algorithm) {
		return new OptimizerRequest().setId(id)
									 .setNetworkName(networkName)
									 .setDictionaryName(dictionaryName)
									 .setWorkflowName(workflowName)
									 .setVersion(version.getKey())
									 .setAlgorithm(algorithm.getKey())
									 .setParallelism(1L)
									 .setTimeout_ms(10000)
									 .setDescription("Test");
	}
}
