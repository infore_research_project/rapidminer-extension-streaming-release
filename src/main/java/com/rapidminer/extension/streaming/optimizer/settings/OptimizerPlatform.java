/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.optimizer.settings;

/**
 * This is a container class to hold the information of a single platform (streaming technology) on
 * an {@link OptimizerSite} which are needed by the INFORE optimizer to perform the optimization and
 * decide on the placement of the operators on the different platforms and sites.
 *
 * @author Fabian Temme
 * @since 0.1.0
 */
public class OptimizerPlatform {
	
	public String platformName = null;
	
	public int driverMemoryMB = 0;
	
	private int executors = 0;
	
	private int executorCores = 0;
	
	private int executorMemoryMB = 0;
	
	private String topicKey = null;
	
	/**
	 * Creates a new {@link OptimizerPlatform} instance with the fields set to default values.
	 */
	public OptimizerPlatform() {
	}
	
	/**
	 * Returns the name of the {@link OptimizerPlatform}. This is also the streaming technology of
	 * the platform.
	 *
	 * @return name of the {@link OptimizerPlatform}
	 */
	public String getPlatformName() {
		return platformName;
	}
	
	/**
	 * Sets the name of the {@link OptimizerPlatform} to the provided one. This is also the
	 * streaming technology of the platform. Returns itself, so that set methods can be chained.
	 *
	 * @param platformName
	 * 		new name of the {@link OptimizerPlatform}
	 * @return this {@link OptimizerPlatform}
	 */
	public OptimizerPlatform setPlatformName(String platformName) {
		this.platformName = platformName;
		return this;
	}
	
	/**
	 * Returns the available driver memory in MB of the {@link OptimizerPlatform}.
	 *
	 * @return available driver memory in MB of the {@link OptimizerPlatform}
	 */
	public int getDriverMemoryMB() {
		return driverMemoryMB;
	}
	
	/**
	 * Sets the available driver memory in MB of the {@link OptimizerPlatform} to the provided one.
	 * Returns itself, so that set methods can be chained.
	 *
	 * @param driverMemoryMB
	 * 		new available driver memory in MB of the {@link OptimizerPlatform}
	 * @return this {@link OptimizerPlatform}
	 */
	public OptimizerPlatform setDriverMemoryMB(int driverMemoryMB) {
		this.driverMemoryMB = driverMemoryMB;
		return this;
	}
	
	/**
	 * Returns the number of available executors of the {@link OptimizerPlatform}.
	 *
	 * @return number of available executors of the {@link OptimizerPlatform}
	 */
	public int getExecutors() {
		return executors;
	}
	
	/**
	 * Sets the number of available executors of the {@link OptimizerPlatform} to the provided one.
	 * Returns itself, so that set methods can be chained.
	 *
	 * @param executors
	 * 		new number of available executors of the {@link OptimizerPlatform}
	 * @return this {@link OptimizerPlatform}
	 */
	public OptimizerPlatform setExecutors(int executors) {
		this.executors = executors;
		return this;
	}
	
	/**
	 * Returns the number of available executor cores of the {@link OptimizerPlatform}.
	 *
	 * @return number of available executor cores of the {@link OptimizerPlatform}
	 */
	public int getExecutorCores() {
		return executorCores;
	}
	
	/**
	 * Sets the number of available executor cores of the {@link OptimizerPlatform} to the provided
	 * one. Returns itself, so that set methods can be chained.
	 *
	 * @param executorCores
	 * 		new number of available executor cores of the {@link OptimizerPlatform}
	 * @return this {@link OptimizerPlatform}
	 */
	public OptimizerPlatform setExecutorCores(int executorCores) {
		this.executorCores = executorCores;
		return this;
	}
	
	/**
	 * Returns the available executor memory in MB of the {@link OptimizerPlatform}.
	 *
	 * @return available executor memory in MB of the {@link OptimizerPlatform}
	 */
	public int getExecutorMemoryMB() {
		return executorMemoryMB;
	}
	
	/**
	 * Sets the available executor memory in MB of the {@link OptimizerPlatform} to the provided
	 * one. Returns itself, so that set methods can be chained.
	 *
	 * @param executorMemoryMB
	 * 		new available executor memory in MB of the {@link OptimizerPlatform}
	 * @return this {@link OptimizerPlatform}
	 */
	public OptimizerPlatform setExecutorMemoryMB(int executorMemoryMB) {
		this.executorMemoryMB = executorMemoryMB;
		return this;
	}
	
	/**
	 * Returns the Kafka topic key used to sent and receive data to and from this {@link
	 * OptimizerPlatform}.
	 *
	 * @return Kafka topic key used to sent and receive data to and from this {@link
	 * OptimizerPlatform}
	 */
	public String getTopicKey() {
		return topicKey;
	}
	
	/**
	 * Sets the Kafka topic key used to sent and receive data to and from this {@link
	 * OptimizerPlatform} to the provided one. Returns itself, so that set methods can be chained.
	 *
	 * @param topicKey
	 * 		new Kafka topic key used to sent and receive data to and from this {@link
	 *        OptimizerPlatform}
	 * @return this {@link OptimizerPlatform}
	 */
	public OptimizerPlatform setTopicKey(String topicKey) {
		this.topicKey = topicKey;
		return this;
	}
	
}
