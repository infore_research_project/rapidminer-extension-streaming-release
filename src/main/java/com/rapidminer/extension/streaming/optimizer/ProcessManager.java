/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.optimizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import com.rapidminer.extension.streaming.optimizer.agnostic_workflow.AWPlacementPlatform;
import com.rapidminer.extension.streaming.optimizer.agnostic_workflow.AWPlacementSite;
import com.rapidminer.extension.streaming.optimizer.agnostic_workflow.AgnosticWorkflow;
import com.rapidminer.extension.streaming.optimizer.settings.Network;
import com.rapidminer.extension.streaming.optimizer.settings.OperatorDictionary;
import com.rapidminer.extension.streaming.optimizer.settings.OptimizerConfig;


/**
 * This ProcessManager is used to start and control the INFORE optimizer service.
 *
 * @author Fabian Temme, Edwin Yaqub
 * @since 0.1.0
 * @deprecated since 0.1.0 (INF-61), cause the INFORE optimizer is not started anymore from RM, but
 * a connection to an already running INFORE optimizer is established. See also {@link
 * OptimizerConnection}.
 */
@Deprecated
public class ProcessManager {
	
	/**
	 * This path is used only for the testing {@link #main(String[])} function.
	 */
	private static final String HARDCODED_PATH = "C:\\Users\\ThomasFabianTemme\\Work\\repositories\\infore_optimizer";
	
	/**
	 * Path of directory where optimizer.jar is placed.
	 */
	private final File path;
	/**
	 * Internal field for logging
	 */
	private final Logger logger;
	/**
	 * Internal field for the INFORE optimizer service process.
	 */
	private Process optimizerProcess = null;
	
	/**
	 * Template for the URI for sending requests to the optimizer service
	 */
	private static final String URI_TEMPLATE = "http://%s:%s/%s/%s";
	/**
	 * Hardcoded host for the communication with the optimizer service
	 */
	private static final String HOST = "localhost";
	/**
	 * Hardcoded port for the communication with the optimizer service
	 */
	private static final String PORT = "8000";
	/**
	 * Control key for sending a "submit" command to the optimizer service
	 */
	private static final String SUBMIT_COMMAND = "submit";
	/**
	 * Control key for sending a "control" command to the optimizer service
	 */
	private static final String CONTROL_COMMAND = "control";
	
	/**
	 * Creates a new {@link ProcessManager} with the provided {@code path} to the INFORE optimizer
	 * jar and the {@link Logger} instance for logging purposes.
	 *
	 * @param path
	 * 		path to the INFORE optimizer jar
	 * @param logger
	 *        {@link Logger} instance used by the {@link ProcessManager} for logging purposes
	 */
	public ProcessManager(File path, Logger logger) {
		this.path = path;
		this.logger = logger;
	}
	
	/**
	 * This is for testing only.
	 *
	 * @param args
	 */
	@Deprecated
	public static void main(String[] args) {
		Logger logger = Logger.getGlobal();
		ProcessManager myProcessExecutor = new ProcessManager(new File(HARDCODED_PATH), logger);
		
		try (BufferedReader readerConfig = new BufferedReader(
				new FileReader(HARDCODED_PATH + "\\optimizer_files\\config.json"));
			 BufferedReader readerNetwork = new BufferedReader(
					 new FileReader(HARDCODED_PATH + "\\optimizer_files\\network.json"));
			 BufferedReader readerDictionary = new BufferedReader(
					 new FileReader(HARDCODED_PATH + "\\optimizer_files\\dictionary.json"));
			 BufferedReader readerWorkflow = new BufferedReader(
					 new FileReader(HARDCODED_PATH + "\\optimizer_files\\workflow.json"))) {
			
			String config = readerConfig.lines().collect(Collectors.joining(" "));
			String network = readerNetwork.lines().collect(Collectors.joining(" "));
			String dictionary = readerDictionary.lines().collect(Collectors.joining(" "));
			String workflow = readerWorkflow.lines().collect(Collectors.joining(" "));
			
			String optimizedWorkflow = myProcessExecutor.optimizeWorkflow(network, config,
																		  dictionary, workflow);
			
			logger.info(optimizedWorkflow);
		} catch (IOException e) {
			logger.warning("Exception! Message: " + e.getLocalizedMessage());
		}
	}
	
	/**
	 * This process starts the INFORE optimizer service ({@link #startServerProcess()}) and submits
	 * the provided configuration JSON strings to the service (by calling {@link
	 * #sendSubmitRequest(HttpClient, String, String)}).
	 * <p>
	 * The INFORE optimizer is started by sending the start command ({@link
	 * #sendControlRequest(HttpClient, String)}. The response for the provided workflow JSON is
	 * received and post processed. The optimizer service is destroyed and the optimizer response
	 * returned.
	 *
	 * @param networkJSON
	 * 		The JSON string containing the information about the available Network for optimization
	 * 		(see {@link Network})
	 * @param configJSON
	 * 		The JSON string containing the information about the configuration of the optimizer (see
	 *        {@link OptimizerConfig})
	 * @param dictionaryJSON
	 * 		The JSON string containing the information about the available Operators for optimization
	 * 		(see {@link OperatorDictionary})
	 * @param outputWorkflowJSON
	 * 		The JSON string representation of the logical workflow to be optimized (see {@link
	 *        AgnosticWorkflow})
	 * @return The optimized workflow (an {@link AgnosticWorkflow} with updated fields in {@link
	 * AWPlacementSite} and {@link AWPlacementPlatform})
	 * @throws IOException
	 */
	public String optimizeWorkflow(String networkJSON, String configJSON, String dictionaryJSON,
			String outputWorkflowJSON) throws IOException {
		startServerProcess();
		
		HttpClient httpClient = HttpClients.createDefault();
		
		Stream<String> response = sendSubmitRequest(httpClient, "network", networkJSON);
		logger.log(Level.INFO, "{0}", response.collect(Collectors.joining()));
		
		response = sendSubmitRequest(httpClient, "optcfg", configJSON);
		logger.log(Level.INFO, "{0}", response.collect(Collectors.joining()));
		
		response = sendSubmitRequest(httpClient, "dictionary", dictionaryJSON);
		logger.log(Level.INFO, "{0}", response.collect(Collectors.joining()));
		
		response = sendControlRequest(httpClient, "start");
		logger.log(Level.INFO, "{0}", response.collect(Collectors.joining()));
		
		response = sendSubmitRequest(httpClient, "workflow", outputWorkflowJSON);
		String responseString = response.collect(Collectors.joining());
		
		String output = responseString.replace("{\"status\":\"SUCCESS\",\"message\":\"", "")
									  .replaceAll("\\\\", "");
		output = output.substring(0, output.length() - 2);
		
		getOptimizerProcess().destroyForcibly();
		return output;
	}
	
	/**
	 * Sends a {@value #SUBMIT_COMMAND} request with the provided {@code command} and {@code
	 * jsonString} to the INFORE optimizer service by using the provided {@link HttpClient}.
	 *
	 * @param client
	 *        {@link HttpClient} used for sending the request
	 * @param command
	 * 		command control key of the INFORE optimizer service
	 * @param jsonString
	 * 		JSON string argument for the submit request
	 * @return Response of the INFORE optimizer service
	 * @throws IOException
	 */
	private Stream<String> sendSubmitRequest(HttpClient client, String command,
			String jsonString) throws IOException {
		RequestBuilder builder = RequestBuilder.post(
				String.format(URI_TEMPLATE, HOST, PORT, SUBMIT_COMMAND, command));
		if (jsonString != null) {
			builder.setEntity(
					new StringEntity(jsonString, ContentType.APPLICATION_FORM_URLENCODED));
		}
		HttpResponse response = client.execute(builder.build());
		return new BufferedReader(new InputStreamReader(response.getEntity().getContent())).lines();
	}
	
	/**
	 * Sends a {@value #CONTROL_COMMAND} request with the provided {@code command} to the INFORE
	 * optimizer service by using the provided {@link HttpClient}.
	 *
	 * @param client
	 *        {@link HttpClient} used for sending the request
	 * @param command
	 * 		command control key of the INFORE optimizer service
	 * @return Response of the INFORE optimizer service
	 * @throws IOException
	 */
	private Stream<String> sendControlRequest(HttpClient client, String command) throws
			IOException {
		HttpUriRequest request = RequestBuilder.get(
				String.format(URI_TEMPLATE, HOST, PORT, CONTROL_COMMAND, command)).build();
		HttpResponse response = client.execute(request);
		return new BufferedReader(new InputStreamReader(response.getEntity().getContent())).lines();
	}
	
	/**
	 * Starts the INFORE optimizer service by executing the jar file in {@link #path} with the
	 * {@link #HOST} and {@link #PORT} options. An {@link ExecutorService} is used for execution of
	 * the Optimizer Service.
	 */
	public void startServerProcess() {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		ProcessBuilder processBuilder = new ProcessBuilder("java", "-jar", "optimizer.jar", "-i",
														   HOST, "-p", PORT);
		processBuilder = processBuilder.directory(path);
		try {
			optimizerProcess = processBuilder.start();
			ProcessReadTask task = new ProcessReadTask(optimizerProcess.getInputStream());
			executorService.submit(task);
		} catch (Exception e) {
			logger.warning("Exception! Message: " + e.getLocalizedMessage());
		} finally {
			executorService.shutdown();
		}
	}
	
	/**
	 * Returns the {@link Process} holding the INFORE optimizer service. Can be {@code null} if the
	 * service is not started yet.
	 *
	 * @return {@link Process} holding the INFORE optimizer service. Can be {@code null} if the
	 * service is not started yet
	 */
	public Process getOptimizerProcess() {
		return optimizerProcess;
	}
	
}

/**
 * This class couples a reading tasks from an input stream into a class.
 *
 * @author Fabian Temme
 * @version 0.1.0
 * @deprecated since 0.1.0 (INF-61), cause the INFORE optimizer is not started anymore from RM, but
 * a connection to an already running INFORE optimizer is established. See also {@link
 * OptimizerConnection}.
 */
@Deprecated
class ProcessReadTask implements Callable<List<String>> {
	
	private final InputStream inputStream;
	
	public ProcessReadTask(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	@Override
	public List<String> call() {
		return new BufferedReader(new InputStreamReader(inputStream)).lines()
																	 .collect(Collectors.toList());
	}
}
