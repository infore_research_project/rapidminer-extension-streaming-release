/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.optimizer;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

import java.io.IOException;
import java.util.logging.Logger;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;


/**
 * @author Fabian Temme
 * @since 0.1.0
 */
public class OptimizerConnection {
	

	/**
	 * Host of the Optimizer service. Default location should be 45.10.26.123 (the Spring cluster)
	 */
	private final String host;
	/**
	 * Port of the Optimizer service. Default value should be 8080.
	 */
	private final String port;
	/**
	 * Username used to authenticate to the Optimizer Service. Default value is infore_user
	 */
	private final String username;
	/**
	 * Password used to authenticate to the Optimizer Service. Default value is infore_pass
	 */
	private final String password;
	
	/**
	 * Internal field for logging
	 */
	private final Logger logger;
	
	public OptimizerConnection(String host, String port, String username, String password,
			Logger logger) {
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.logger = logger;
	}
	
	public String optimizeWorkflow(String networkJSON, String dictionaryJSON, String workflowJSON,
			String requestJSON) {
		String url = String.format("http://%s:%s", host, port);
		RestTemplate restTemplate = initRestTemplate();
		HttpHeaders authHeaders = initHeaders();
		ResponseEntity<String> resNetwork = restTemplate.exchange(url + "/resource/network",
																  HttpMethod.PUT,
																  new HttpEntity<>(networkJSON,
																				   authHeaders),
																  String.class);
		String networkBody = resNetwork.getBody();
		
		ResponseEntity<String> resDict = restTemplate.exchange(url + "/resource/dictionary",
															   HttpMethod.PUT,
															   new HttpEntity<>(dictionaryJSON,
																				authHeaders),
															   String.class);
		String dictBody = resDict.getBody();
		
		ResponseEntity<String> resWorkflow = restTemplate.exchange(url + "/resource/workflow",
																   HttpMethod.PUT,
																   new HttpEntity<>(workflowJSON,
																					authHeaders),
																   String.class);
		String workflowBody = resWorkflow.getBody();
		
		//Submit the optimization request
		ResponseEntity<String> optimizedWorkflow = restTemplate.postForEntity(
				url + "/request/optimize", new HttpEntity<>(requestJSON, authHeaders),
				String.class);
		String optimizedWorkflowBody = optimizedWorkflow.getBody();
		
		return optimizedWorkflowBody;
	}
	
	private HttpHeaders initHeaders() {
		HttpHeaders authHeaders = new HttpHeaders();
		authHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		authHeaders.add(HttpHeaders.ACCEPT, MediaType.ALL_VALUE);
		authHeaders.setBasicAuth(username, password);
		return authHeaders;
	}
	
	private RestTemplate initRestTemplate() {
		SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
		clientHttpRequestFactory.setConnectTimeout(300_000);
		clientHttpRequestFactory.setReadTimeout(300_000);
		
		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			
			@Override
			public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
				return ( httpResponse.getStatusCode()
									 .series() == CLIENT_ERROR || httpResponse.getStatusCode()
																			  .series() == SERVER_ERROR );
			}
			
			@Override
			public void handleError(ClientHttpResponse httpResponse) throws IOException {
				if (httpResponse.getStatusCode().series() == SERVER_ERROR) {
					// handle SERVER_ERROR
				} else if (httpResponse.getStatusCode().series() == CLIENT_ERROR) {
					// handle CLIENT_ERROR
				}
			}
		});
		return restTemplate;
	}
}

