/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.optimizer.settings;

/**
 * This is a container class holding the dictionary information of a single operator class in the
 * {@link OperatorDictionary} used by the INFORE optimizer.
 *
 * @author Fabian Temme
 * @since 0.1.0
 */
public class DictOperator {
	
	private String classKey = null;
	
	private DictPlatforms platforms = null;
	
	/**
	 * Creates a new {@link DictOperator} instance with the fields set to default values.
	 */
	public DictOperator() {
	}
	
	/**
	 * Returns the class key (the key used in RapidMiner to identify the class of the operator) of
	 * the {@link DictOperator}.
	 *
	 * @return class key (the key used in RapidMiner to identify the class of the operator) of the
	 * {@link DictOperator}
	 */
	public String getClassKey() {
		return classKey;
	}
	
	/**
	 * Sets the class key (the key used in RapidMiner to identify the class of the operator) of the
	 * {@link DictOperator} to the provided one. Returns itself, so that set methods can be
	 * chained.
	 *
	 * @param classKey
	 * 		new class key (the key used in RapidMiner to identify the class of the operator) of the
	 *        {@link DictOperator}
	 * @return this {@link DictOperator}
	 */
	public DictOperator setClassKey(String classKey) {
		this.classKey = classKey;
		return this;
	}
	
	/**
	 * Returns the platforms for which physical implementations of the {@link DictOperator} are
	 * available.
	 *
	 * @return platforms for which physical implementations of the {@link DictOperator} are
	 * available
	 */
	public DictPlatforms getPlatforms() {
		return platforms;
	}
	
	/**
	 * Sets the platforms for which physical implementations of the {@link DictOperator} are
	 * available to the provided ones. Returns itself, so that set methods can be chained.
	 *
	 * @param platforms
	 * 		new {@link DictPlatforms} for which physical implementations of the {@link DictOperator} are available
	 * @return this {@link DictOperator}
	 */
	public DictOperator setPlatforms(DictPlatforms platforms) {
		this.platforms = platforms;
		return this;
	}
	
}
