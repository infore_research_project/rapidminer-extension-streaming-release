/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import static com.rapidminer.extension.streaming.PluginInitStreaming.getPluginLoader;
import static com.rapidminer.extension.streaming.connection.StreamingConnectionHelper.createStreamConnectionSelector;
import static java.util.concurrent.CompletableFuture.runAsync;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.util.ConnectionInformationSelector;
import com.rapidminer.extension.streaming.connection.StreamConnectionHandler;
import com.rapidminer.extension.streaming.connection.StreamingConnectionHelper;
import com.rapidminer.extension.streaming.deploy.StreamRunner;
import com.rapidminer.extension.streaming.deploy.StreamRunnerException;
import com.rapidminer.extension.streaming.deploy.StreamRunnerFactory;
import com.rapidminer.extension.streaming.deploy.StreamRunnerType;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.operator.OperatorChain;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ProcessStoppedException;
import com.rapidminer.operator.UserError;
import com.rapidminer.operator.ports.PortPairExtender;
import com.rapidminer.operator.ports.metadata.SubprocessTransformRule;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.ClassLoaderSwapper;
import com.rapidminer.tools.LogService;


/**
 * Nest operator that wraps a stream workflow, this is the main entry-point for a stream workflow
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamingNest extends OperatorChain {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String PARAMETER_JOB_NAME = "job_name";

	private static final StreamRunnerFactory RUNNER_FACTORY = new StreamRunnerFactory();

	private final ConnectionInformationSelector connectionSelector = createStreamConnectionSelector(this, "connection");

	private final PortPairExtender inputPortPairExtender = new PortPairExtender(
		"in", getInputPorts(), getSubprocess(0).getInnerSources());

	private final PortPairExtender outputPortPairExtender = new PortPairExtender(
		"out", getSubprocess(0).getInnerSinks(), getOutputPorts());

	private StreamGraph graph;

	public StreamingNest(OperatorDescription description) {
		super(description, "Streaming");
		inputPortPairExtender.start();
		outputPortPairExtender.start();
		getTransformer().addRule(inputPortPairExtender.makePassThroughRule());
		getTransformer().addRule(new SubprocessTransformRule(getSubprocess(0)));
		getTransformer().addRule(outputPortPairExtender.makePassThroughRule());
	}

	@Override
	public void doWork() throws OperatorException {
		inputPortPairExtender.passDataThrough();
		graph = new StreamGraph(getParameterAsString(PARAMETER_JOB_NAME));
		LOGGER.fine("Processing NEST for: " + graph.getName());

		// Certain libraries depend on the thread's context loader, so we set the plugin's loader to be that
		try (ClassLoaderSwapper cls = ClassLoaderSwapper.withContextClassLoader(getPluginLoader())) {
			// Execute children operators
			super.doWork();
			outputPortPairExtender.passDataThrough();

			// Connection and appropriate handler (based on the connection)
			ConnectionConfiguration connConfig = connectionSelector.getConnection().getConfiguration();
			String connType = connConfig.getType();
			StreamConnectionHandler handler = StreamingConnectionHelper.CONNECTION_HANDLER_MAP.get(connType);

			// Transform connection info into properties using the handler + get appropriate stream runner type
			Properties runnerConfig = handler.buildClusterConfiguration(connConfig);
			StreamRunnerType runnerType = handler.getRunnerType();

			// Create runner + Start execution asynchronously and periodically check for stop
			StreamRunner runner = RUNNER_FACTORY.createRunner(runnerType, runnerConfig);
			CompletableFuture<Void> future = runAsync(() -> executeGraph(runner), Executors.newSingleThreadExecutor());
			try {
				while (!future.isDone()) {
					checkForStop();
					Thread.sleep(500);
				}
				// Get potentially exceptional 'result'
				future.get();
			} catch (ProcessStoppedException pse) {
				LOGGER.warning("Process stopped for streaming job execution: '" + graph.getName() + "'");
				future.cancel(true);
				throw pse;
			} catch (InterruptedException ie) {
				LOGGER.severe("Process for streaming job '" + graph.getName() + "' got interrupted: " + ie.getMessage());
				future.cancel(true);
				Thread.currentThread().interrupt();
			} catch (ExecutionException ee) {
				LOGGER.warning("Error while executing streaming job '" + graph.getName() + "': " + ee.getMessage());
				throw new UserError(this, ee, "stream_connection.unsuccessful");
			}
		} catch (StreamRunnerException e) {
			LOGGER.warning("Error while executing streaming job '" + graph.getName() + "': " + e.getMessage());
			throw new UserError(this, e, "stream_connection.unsuccessful");
		}
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> params = super.getParameterTypes();

		ParameterType jobName = new ParameterTypeString(PARAMETER_JOB_NAME, "Name of the stream job.", false);
		params.add(jobName);

		return params;
	}

	public PortPairExtender getInputPortPairExtender() {
		return inputPortPairExtender;
	}

	public PortPairExtender getOutputPortPairExtender() {
		return outputPortPairExtender;
	}

	/**
	 * Package private
	 *
	 * @return graph object for this workflow
	 */
	StreamGraph getGraph() {
		return graph;
	}

	/**
	 * Initiates execution of the workflow using the given StreamRunner implementation
	 *
	 * @param runner
	 */
	private void executeGraph(StreamRunner runner) {
		try {
			LOGGER.fine("Starting execution using: " + runner.getClass().getName());
			runner.execute(graph);
		} catch (StreamRunnerException ex) {
			throw new CompletionException(ex);
		}
	}

}