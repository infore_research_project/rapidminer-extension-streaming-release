/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.rapidminer.BreakpointListener;
import com.rapidminer.Process;
import com.rapidminer.ProcessStateListener;
import com.rapidminer.extension.streaming.connection.financialserver.ConnectionManager;
import com.rapidminer.operator.ExecutionUnit;
import com.rapidminer.operator.OperatorChain;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.CollectingPortPairExtender;
import com.rapidminer.operator.ports.PortPairExtender;
import com.rapidminer.operator.ports.metadata.SubprocessTransformRule;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.ParameterTypePassword;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.UndefinedParameterError;
import com.rapidminer.parameter.conditions.BooleanParameterCondition;
import com.rapidminer.tools.LogService;


/**
 * Experimental (beta) version of a nested operator for handling communication with a socket server. To support
 * asynchronous form of socket communication, it spins up a thread to stay alive, which should be killed by inserting a
 * hook to stop the loop in RapidMiner framework GUI method for Stop button.
 * <p>
 * Various functionalities planned to be added step by step to extend the RapidMiner's default process execution (which
 * by default is strictly sequential for operators in an operator chain).
 *
 * @author Edwin Yaqub
 * @since 0.1.0
 */
public class SocketNestOperator extends OperatorChain {

	// tmporarily keeping credential params here (to be taken out to connection management)
	private static final String PARAMETER_USERNAME = "username";
	private static final String PARAMETER_PASSWORD = "password";
	private static final int SERVER_CONNECT_READ_TIMEOUT = 300000;
	private static final String PARAMETER_POLLING_DESIRED = "poll on server";
	public static final String PARAMETER_POLLING_TIME_MILLISECONDS = "polling time (milliseconds)";
	public static final int DEFAULT_POLLING_TIME_MILLISECONDS = 1000;
	// other parameters and properties
	private static final Logger LOGGER = LogService.getRoot();

	private final PortPairExtender inputPortPairExtender = new PortPairExtender("input", getInputPorts(),
		getSubprocess(0).getInnerSources());
	private final CollectingPortPairExtender outExtender = new CollectingPortPairExtender("output",
		getSubprocess(0).getInnerSinks(), getOutputPorts());

	private ConnectionManager connectionHandler = null;
	// The interval between check for stops (in ms)
	// private static final int CHECK_FOR_STOP_PERIOD = 1000;

	/**
	 * @param description
	 */
	public SocketNestOperator(OperatorDescription description) {
		super(description, "ReadSocket Executed");
		inputPortPairExtender.start();
		outExtender.start();
		getTransformer().addRule(inputPortPairExtender.makePassThroughRule());
		getTransformer().addRule(new SubprocessTransformRule(getSubprocess(0)));
		getTransformer().addRule(outExtender.makePassThroughRule());
	}

	// Hold RM framework default behaviour, communicate with Socket server run subProcess upon arrival of an
	// Event
	@Override
	public void doWork() throws OperatorException {
		// Hold framework to not terminate process execution
		overrideProcessStateListeners();
		// overrideProcessSetupListener();
		outExtender.reset();
		inputPortPairExtender.passDataThrough();

		// show progress to user for visual feedback
		getProgress().setIndeterminate(true);
		// addCountDownLatch();

		// Initialize connection handler thread with this process
		connectionHandler = new ConnectionManager(getProcess());
		connectionHandler.setPollingTime(getPollingTime());
		// Connect to server, start listening and login.
		if (connectionHandler.connect() == ConnectionManager.OK) {
			loginToSpringServer();
			/*
			 * try { getSubprocess(0).execute(); } catch (OperatorException e) { e.printStackTrace(); }
			 */
		}
		outExtender.collect();
	}

	private int getPollingTime() {
		int pollingTime = DEFAULT_POLLING_TIME_MILLISECONDS;
		try {
			if (getParameterAsBoolean(PARAMETER_POLLING_DESIRED)) {
				pollingTime = getParameterAsInt(PARAMETER_POLLING_TIME_MILLISECONDS);
			}
		} catch (UndefinedParameterError e) {
			e.printStackTrace();
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
		return pollingTime;
	}

	/**
	 * TODO: Ask Spring to provide a logout method. Currently, if RM process is restarted with same user credentials,
	 * the server only sends the response "User already logged in" and no more messages are received. So till we have a
	 * logout method, we can have a work-around to check before logging-in whether the user is already logged in for
	 * this client.
	 */
	private void loginToSpringServer() {
		String username = null;
		String password = null;
		try {
			username = getParameter(PARAMETER_USERNAME);
			password = getParameter(PARAMETER_PASSWORD);
			LOGGER.log(Level.INFO, "** Connecting with Server using these credentials: @" + username + ":" + password);
		} catch (UndefinedParameterError e) {
			e.printStackTrace();
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
		// TODO: check out streaming possibilities with Collection framework later:
		// List<ExecutionUnit> listofExecutionUnitsOfSubProcesses = getProcess().getRootOperator().getSubprocesses();
		// Stream<ExecutionUnit> streamOfExecutionUnitsOfSubProcesses =
		// listofExecutionUnitsOfSubProcesses.parallelStream();
		// LOGGER.log(Level.INFO, "** Stream of Sub-Processes is by default Parallel : "+
		// streamOfExecutionUnitsOfSubProcesses.isParallel());

		// Send a command to Server
		try {
			connectionHandler.login(username, password);
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.log(Level.SEVERE, "-- LOGIN FAILED!" + e.getMessage() + " - " + e);
		}

	}

	private void overrideProcessStateListeners() {
		// how many subProcesses do we have?
		int numberOfSubprocesses = getProcess().getRootOperator().getNumberOfSubprocesses();
		LOGGER.log(Level.INFO, "** NumberOfSubprocesses (method 1) = " + numberOfSubprocesses);
		// List<ExecutionUnit> subProcesses = getSubprocesses();
		// LOGGER.log(Level.INFO, "** NumberOfSubprocesses (method 2) = " + numberOfSubprocesses);
		// Add the Breakpoint After for the first time:
		setBreakpoint(BreakpointListener.BREAKPOINT_AFTER, true);
		// Add Process State Listener to grab a handle on the default execution lifecycle of operator (not changing,
		// removing or reducing any pre-post paraphernalia but add control on top of default)
		this.getProcess().addProcessStateListener(new ProcessStateListener() {
			@Override
			public void stopped(Process process) {
				LOGGER.log(Level.INFO, "++ Stopping Server Listener Thread, Streams and Socket");
				// connectionHandler.closeCommunication();
				connectionHandler.stopRunning();
				LOGGER.log(Level.INFO, "++ Stopping Breakpoint");
				setBreakpoint(BreakpointListener.BREAKPOINT_AFTER, false);
				// getProcess().setExperimentState(Process.PROCESS_STATE_STOPPED);
				// getProcess().shouldStop();

				LOGGER.log(Level.INFO, "++ Process is Stopped");
				/*
				// Intentionally specifying no process location
				try {
					getRoot().getProcess().save();
				} catch (IOException e) {
					e.printStackTrace();
					LOGGER.log(Level.SEVERE, "++ Exception while Saving Process in Stop()");
				}
				if (!RapidMiner.getExecutionMode().isHeadless()) {
					RapidMinerGUI.getMainFrame().processHasBeenSaved();
					SaveAction.save(getProcess());
				}
				 */
			}

			@Override
			public void resumed(Process process) {
				LOGGER.log(Level.INFO, "++ Process is Resumed");
				LOGGER.log(Level.INFO, "++++ Executing from resumed() ++++");
				ExecutionUnit unit = getSubprocesses().get(0);
				try {
					unit.execute();
				} catch (OperatorException e) {
					e.printStackTrace();
					LOGGER.log(Level.INFO, "++++ Exception Executing: " + e.getMessage() + " - " + e);
				}
				// setBreakpoint(BreakpointListener.BREAKPOINT_AFTER, true);// no effect, already added.

				// The only public method to change process state externally (outside RM framework)
				getProcess().setExperimentState(Process.PROCESS_STATE_PAUSED);
				LOGGER.log(Level.INFO, "++ Process Resume is ending!");
			}

			@Override
			public void paused(Process process) {
				LOGGER.log(Level.INFO, "++ Process is Paused");
				// LOGGER.log(Level.INFO, "++++ Executing from paused() ++++");
				// ExecutionUnit unit = getSubprocesses().get(0);
				// Only public method to change process state externally (outside RM framework)
				process.setExperimentState(Process.PROCESS_STATE_PAUSED);
				// setBreakpoint(BreakpointListener.BREAKPOINT_AFTER, false);
				LOGGER.log(Level.INFO, "++ Process Pause is ending!");
			}

			@Override
			public void started(Process process) {
				LOGGER.log(Level.INFO, "++ Process is Started");

				if (connectionHandler == null) {
					connectionHandler = new ConnectionManager(getProcess());
					connectionHandler.setPollingTime(getPollingTime());
					int responseCode = connectionHandler.connect();
					if (responseCode == ConnectionManager.OK) {
						LOGGER.log(Level.INFO, "++ Connection Handler reset with new Socket and I/O Streams!");
					} else {
						LOGGER.log(Level.SEVERE,
							"++ Connection Handler reset was not Successfull - code = " + responseCode);
					}
				}

				LOGGER.log(Level.INFO, "++ Process Start is ending!");
			}
		});
	}

	/*
	private void customExecute(ExecutionUnit unit) throws OperatorException {
		Logger logger = unit.getEnclosingOperator().getLogger();
		if (logger.isLoggable(Level.FINE)) {
			logger.fine("Executing subprocess " + unit.getEnclosingOperator().getName() + "." + unit.getName()
			+ ". Execution order is: " + unit.getOperators());
		}
		Process process = unit.getEnclosingOperator().getProcess();
		Enumeration<Operator> opEnum = unit.getOperatorEnumeration();
		Operator lastOperator = null;
		Operator operator = opEnum.hasMoreElements() ? opEnum.nextElement() : null;
		while (operator != null) {

			// fire event that we are about to start the next operator
			if (process != null) {
				// gather input data for connected ports
				List<FlowData> input = new LinkedList<>();
				if (operator.getInputPorts() != null) {
					for (InputPort inputPort : operator.getInputPorts().getAllPorts()) {
						if (inputPort.isConnected()) {
							IOObject data = inputPort.getAnyDataOrNull();
							if (data != null) {
								data = GisaFlowCleaner.INSTANCE.checkCleanup(data, inputPort);
								input.add(new FlowData(data, inputPort));
							}
						}
					}
				}
				process.fireProcessFlowBeforeOperator(lastOperator, operator, input);
			}

			// execute the operator
			operator.execute();

			lastOperator = operator;
			operator = opEnum.hasMoreElements() ? opEnum.nextElement() : null;

			// fire event that we finished last operator
			if (process != null) {
				// gather output data for connected ports
				List<FlowData> output = new LinkedList<>();
				if (lastOperator.getOutputPorts() != null) {
					for (OutputPort outputPort : lastOperator.getOutputPorts().getAllPorts()) {
						if (outputPort.isConnected()) {
							IOObject data = outputPort.getAnyDataOrNull();
							if (data != null) {
								output.add(new FlowData(data, outputPort));
							}
						}
					}
				}
				process.fireProcessFlowAfterOperator(lastOperator, operator, output);
			}
			lastOperator.freeMemory();
		}
	}
	 */

	/*
	private void overrideProcessSetupListener() {
		this.getProcess().addProcessSetupListener(new ProcessSetupListener() {

			@Override
			public void operatorRemoved(Operator operator, int oldIndex, int oldIndexAmongEnabled) {
				LOGGER.log(Level.INFO, "++ OperatorRemoved");
			}

			@Override
			public void operatorChanged(Operator operator) {
				LOGGER.log(Level.INFO, "++ OperatorChanged");
			}

			@Override
			public void operatorAdded(Operator operator) {
				LOGGER.log(Level.INFO, "++ OperatorAdded");
			}

			@Override
			public void executionOrderChanged(ExecutionUnit unit) {
				LOGGER.log(Level.INFO, "++ ExecutionOrderChanged");
			}
		});
	}
	 */

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();

		ParameterType type = new ParameterTypeString(PARAMETER_USERNAME, "Enter username to connect with socket server",
			false, false);
		types.add(type);

		type = new ParameterTypePassword(PARAMETER_PASSWORD, "Enter password for your username");
		type.setOptional(false);
		type.setExpert(false);
		types.add(type);

		type = new ParameterTypeBoolean(PARAMETER_POLLING_DESIRED,
			"Select if it is desired to poll the server. Polling prevents unnecessary CPU usage but a large value can result in loss of data if storage buffer overflows.",
			false);
		types.add(type);
		// create and register dependent parameter
		type = new ParameterTypeInt(PARAMETER_POLLING_TIME_MILLISECONDS,
			"Polling time (in milliseconds) to read from server.", 0, Integer.MAX_VALUE,
			DEFAULT_POLLING_TIME_MILLISECONDS, false);
		type.registerDependencyCondition(new BooleanParameterCondition(this, PARAMETER_POLLING_DESIRED, true, true));
		types.add(type);

		// PARAMETER_POLLING_DESIRED
		return types;
	}

	/*
	private void addCountDownLatch() {
		CountDownLatch latch = new CountDownLatch(1);
		Operator thisOperator = getProcess().getCurrentOperator();
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				try {
					thisOperator.checkForStop();
				} catch (ProcessStoppedException e) {
					latch.countDown();
					timer.cancel();
				}
			}
		}, CHECK_FOR_STOP_PERIOD, CHECK_FOR_STOP_PERIOD);
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				latch.countDown();
				timer.cancel();
			}
		}, 1000);
		try {
			latch.await();
		} catch (InterruptedException e) {
			timer.cancel();
			Thread.currentThread().interrupt();
		}
	}
	 */

}