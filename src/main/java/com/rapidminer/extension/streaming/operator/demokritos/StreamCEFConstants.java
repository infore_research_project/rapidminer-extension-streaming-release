/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator.demokritos;


/**
 * Constants for CEF operators
 *
 * @author Mate Torok
 * @since 0.1.0
 */
final class StreamCEFConstants {

	static final String PARAMETER_JOB_JAR = "job_jar";

	static final String PARAMETER_THRESHOLD = "threshold";

	static final String PARAMETER_MAX_SPREAD = "max_spread";

	static final String PARAMETER_HORIZON = "horizon";

	static final String PARAMETER_PATTERNS = "patterns";

	static final String PARAMETER_DECLARATIONS = "declarations";

	static final String PARAMETER_PARALLELISM = "parallelism";

	static final String PARAMETER_INPUT_TOPIC = "input_topic";

	static final String PARAMETER_OUTPUT_TOPIC = "output_topic";

	static final String CEF_JOB_MAIN_CLASS = "ui.WayebCLI";

	static final String CEF_FLINK_JOB_ARG_THRESHOLD = "--threshold";

	static final String CEF_FLINK_JOB_ARG_MAX_SPREAD = "--maxSpread";

	static final String CEF_FLINK_JOB_ARG_HORIZON = "--horizon";

	static final String CEF_FLINK_JOB_ARG_PATTERNS = "--patterns";

	static final String CEF_FLINK_JOB_ARG_DECLARATIONS = "--declarations";

	static final String CEF_FLINK_JOB_ARG_PARALLELISM = "--parallelism";

	static final String CEF_FLINK_JOB_ARG_INPUT_TOPIC = "--inputTopic";

	static final String CEF_FLINK_JOB_ARG_OUTPUT_TOPIC = "--outputTopic";

	static final String CEF_FLINK_JOB_ARG_BOOTSTRAPS = "--bootstrapServers";

	static final String CEF_FLINK_JOB_ARG_ONLINE_FORECASTING = "onlineForecasting";

	private StreamCEFConstants() {
	}

}