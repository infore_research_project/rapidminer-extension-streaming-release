/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator.demokritos;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static com.rapidminer.extension.streaming.connection.StreamingConnectionHelper.createFlinkSelector;
import static com.rapidminer.extension.streaming.connection.StreamingConnectionHelper.createKafkaSelector;
import static com.rapidminer.extension.streaming.deploy.flink.FlinkConstants.RM_CONF_FLINK_CLUSTER_HOST;
import static com.rapidminer.extension.streaming.deploy.flink.FlinkConstants.RM_CONF_FLINK_CLUSTER_PORT;
import static com.rapidminer.extension.streaming.operator.demokritos.StreamCEFConstants.*;
import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.admin.NewTopic;

import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.util.ConnectionInformationSelector;
import com.rapidminer.extension.streaming.connection.FlinkConnectionHandler;
import com.rapidminer.extension.streaming.connection.KafkaConnectionHandler;
import com.rapidminer.extension.streaming.deploy.KafkaClient;
import com.rapidminer.extension.streaming.deploy.flink.FlinkRestClient;
import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.StreamProducer;
import com.rapidminer.extension.streaming.utility.graph.sink.KafkaSink;
import com.rapidminer.extension.streaming.utility.graph.source.KafkaSource;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.UserError;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeDouble;
import com.rapidminer.parameter.ParameterTypeFile;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.LogService;


/**
 * Operator to deploy an CEF job and involve that in the workflow
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamCEFOperator extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private final ConnectionInformationSelector kafkaConn = createKafkaSelector(this, "kafka-connection");

	private final ConnectionInformationSelector flinkConn = createFlinkSelector(this, "flink-connection");

	private final InputPort input = getInputPorts().createPort("input stream", StreamDataContainer.class);

	private final OutputPort output = getOutputPorts().createPort("output stream");

	private KafkaClient kafkaClient;

	private FlinkRestClient flinkClient;

	private Properties kafkaConfig;

	private Properties flinkConfig;

	/**
	 * Constructor
	 *
	 * @param description
	 */
	public StreamCEFOperator(OperatorDescription description) {
		super(description);
		getTransformer().addPassThroughRule(input, output);
	}

	@Override
	public void doWork() throws OperatorException {
		StreamGraph graph = input.getData(StreamDataContainer.class).getStreamGraph();
		LOGGER.fine("Processing StreamCEFOperator for: " + graph.getName());

		// Setup cluster connection configurations + clients for them
		ConnectionConfiguration kafkaConnConfig = kafkaConn.getConnection().getConfiguration();
		ConnectionConfiguration flinkConnConfig = flinkConn.getConnection().getConfiguration();
		kafkaConfig = KafkaConnectionHandler.getINSTANCE().buildClusterConfiguration(kafkaConnConfig);
		flinkConfig = FlinkConnectionHandler.getINSTANCE().buildClusterConfiguration(flinkConnConfig);

		kafkaClient = new KafkaClient(kafkaConfig);
		flinkClient = new FlinkRestClient(
			flinkConfig.getProperty(RM_CONF_FLINK_CLUSTER_HOST),
			flinkConfig.getProperty(RM_CONF_FLINK_CLUSTER_PORT));

		// Extend graph with CEF parts
		StreamProducer lastNode = extendGraph();

		// Setup Kafka topics for CEF
		setupKafkaTopics();

		// Deploy job (fat-JAR)
		deployJob();

		StreamDataContainer outData = new StreamDataContainer(graph, lastNode);
		output.deliver(outData);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = newArrayList();

		types.add(new ParameterTypeDouble(PARAMETER_THRESHOLD, "Threshold", 0.0d, 1.0, false));
		types.add(new ParameterTypeInt(PARAMETER_MAX_SPREAD, "Max spread", 0, Integer.MAX_VALUE, false));
		types.add(new ParameterTypeInt(PARAMETER_HORIZON, "Horizon", 0, Integer.MAX_VALUE, false));
		types.add(new ParameterTypeFile(PARAMETER_PATTERNS, "Pattern file", "*", false));
		types.add(new ParameterTypeFile(PARAMETER_DECLARATIONS, "Declaration file", "*", true));
		types.add(new ParameterTypeInt(PARAMETER_PARALLELISM, "Job parallelism", 1, Integer.MAX_VALUE, false));
		types.add(new ParameterTypeString(PARAMETER_INPUT_TOPIC, "Input topic", false));
		types.add(new ParameterTypeString(PARAMETER_OUTPUT_TOPIC, "Output topic", false));

		// For the fat-JAR
		types.add(new ParameterTypeFile(PARAMETER_JOB_JAR, "Path to the job fat-JAR", "jar", false));

		return types;
	}

	/**
	 * Extends the graph with CEF specific logic (data producer + Kafka source for forecast consumption)
	 *
	 * @return node representing the incoming prediction data for the downstream
	 */
	private StreamProducer extendGraph() throws UserError {
		StreamDataContainer inData = input.getData(StreamDataContainer.class);
		StreamProducer lastNode = inData.getLastNode();
		StreamGraph graph = inData.getStreamGraph();

		// Produce training data for CEF
		new KafkaSink.Builder(graph)
			.withTopic(getParameterAsString(PARAMETER_INPUT_TOPIC))
			.withParent(lastNode)
			.withConfiguration(kafkaConfig)
			.build();

		// Consume predictions
		KafkaSource forecastSource = new KafkaSource.Builder(graph)
			.withTopic(getParameterAsString(PARAMETER_OUTPUT_TOPIC))
			.withConfiguration(kafkaConfig)
			.build();

		graph.registerSource(forecastSource);
		return forecastSource;
	}

	/**
	 * Creates topics for the CEF. There is a pre-check before the actual creation for early failing.
	 */
	private void setupKafkaTopics() throws OperatorException {
		LOGGER.fine("Setting up topics");
		Set<String> topics = newHashSet(getParameter(PARAMETER_INPUT_TOPIC), getParameter(PARAMETER_OUTPUT_TOPIC));

		// Check if any of the topics already exists (not atomic, just trying to help the user)
		Set<String> existingTopics = kafkaClient.checkTopics(topics);
		if (!existingTopics.isEmpty()) {
			throw new OperatorException("The following topics already exist: " + existingTopics);
		}

		// Create them now (1-1: partitions and replication factor, these will eventually become parameters themselves)
		Collection<NewTopic> newTopics = topics
			.stream()
			.map(topic -> new NewTopic(topic, 1, (short) 1))
			.collect(Collectors.toList());
		kafkaClient.createTopics(newTopics);
	}

	/**
	 * Deploys the JAR given as parameter to the Flink cluster
	 */
	private void deployJob() throws UserError {
		LOGGER.fine("Deploying job");
		try {
			// Parallelism will eventually become a parameter itself
			int parallelism = getParameterAsInt(PARAMETER_PARALLELISM);

			flinkClient.uploadAndSubmit(
				getParameterAsFile(PARAMETER_JOB_JAR).getPath(),
				buildJobArgList(),
				parallelism,
				CEF_JOB_MAIN_CLASS);
		} catch (IOException e) {
			throw new UserError(this, e, "flink_io");
		}
	}

	/**
	 * @return list of parameters for the Flink job
	 */
	private List<String> buildJobArgList() throws UserError {
		return newArrayList(
			CEF_FLINK_JOB_ARG_ONLINE_FORECASTING,
			CEF_FLINK_JOB_ARG_THRESHOLD, getParameter(PARAMETER_THRESHOLD),
			CEF_FLINK_JOB_ARG_MAX_SPREAD, getParameter(PARAMETER_MAX_SPREAD),
			CEF_FLINK_JOB_ARG_HORIZON, getParameter(PARAMETER_HORIZON),
			CEF_FLINK_JOB_ARG_PATTERNS, getFileContent(getParameterAsFile(PARAMETER_PATTERNS)),
			CEF_FLINK_JOB_ARG_DECLARATIONS, getFileContent(getParameterAsFile(PARAMETER_DECLARATIONS)),
			CEF_FLINK_JOB_ARG_PARALLELISM, getParameter(PARAMETER_PARALLELISM),
			CEF_FLINK_JOB_ARG_INPUT_TOPIC, getParameter(PARAMETER_INPUT_TOPIC),
			CEF_FLINK_JOB_ARG_OUTPUT_TOPIC, getParameter(PARAMETER_OUTPUT_TOPIC),
			CEF_FLINK_JOB_ARG_BOOTSTRAPS, kafkaConfig.getProperty(BOOTSTRAP_SERVERS_CONFIG)
		);
	}

	/**
	 * If the src is not null, reads the file's content and returns it, otherwise it returns an EMPTY string
	 * @param src file to read
	 * @return see above
	 * @throws UserError
	 */
	private String getFileContent(File src) throws UserError {
		if (src == null) {
			return StringUtils.EMPTY;
		} else {
			try {
				return StringUtils.trimToEmpty(FileUtils.readFileToString(src, Charset.defaultCharset()));
			} catch (IOException e) {
				throw new UserError(this, e, 302);
			}
		}
	}

}