/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import java.util.List;
import java.util.logging.Logger;

import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.transform.MapTransformer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.LogService;


/**
 * Map operator for stream graphs
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamMap extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String PARAMETER_KEY = "key";

	private static final String PARAMETER_VALUE = "value";

	private final InputPort input = getInputPorts().createPort("input stream", StreamDataContainer.class);

	private final OutputPort output = getOutputPorts().createPort("output stream");

	public StreamMap(OperatorDescription description) {
		super(description);

		getTransformer().addPassThroughRule(input, output);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();

		ParameterType key = new ParameterTypeString(
			PARAMETER_KEY,
			"Key of the value to be mapped.",
			false);
		types.add(key);

		ParameterType value = new ParameterTypeString(
			PARAMETER_VALUE,
			"New value for the keyed field.",
			false);
		types.add(value);

		return types;
	}

	@Override
	public void doWork() throws OperatorException {
		StreamDataContainer inData = input.getData(StreamDataContainer.class);
		StreamGraph graph = inData.getStreamGraph();
		LOGGER.fine("Processing MAP for: " + graph.getName());

		MapTransformer map = new MapTransformer.Builder(graph)
			.withKey(getParameterAsString(PARAMETER_KEY))
			.withNewValue(getParameterAsString(PARAMETER_VALUE))
			.withParent(inData.getLastNode())
			.build();

		StreamDataContainer outData = new StreamDataContainer(graph, map);
		output.deliver(outData);
	}

}