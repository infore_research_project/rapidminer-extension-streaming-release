/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator.kafka;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.AttributeFactory;
import com.rapidminer.example.utils.ExampleSetBuilder;
import com.rapidminer.example.utils.ExampleSets;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ProcessStoppedException;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.operator.ports.metadata.AttributeMetaData;
import com.rapidminer.operator.ports.metadata.ExampleSetMetaData;
import com.rapidminer.operator.ports.metadata.GenerateNewExampleSetMDRule;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.conditions.EqualTypeCondition;
import com.rapidminer.tools.Ontology;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;


/**
 *
 * Basic Operator that reads data from a registered kafka topic.
 * The request frequency and duration can be defined, but it's meant first as a simple one time reader, which can be
 * used in a batch processing fashion.
 *
 * @author David Arnu
 * @since 0.1.0
 *
 */
public class ReadKafkaTopic extends Operator {

    static final String PARAMETER_KAFKA_CLUSTER = "kafka_cluster";

    static final String PARAMETER_KAFKA_PORT = "kafka_port";

    static final String PARAMETER_TOPIC = "kafka_topic";

    //  static final String PARAMETER_MAX_POLL_RECORDS = "max_poll_records";

    static final String PARAMETER_RETRIEVE_TIMEOUT = "retrieval_time_out";

    static final String PARAMETER_AUTO_OFFSET = "offset_strategy";

    // timer or number of messages
    static final String PARAMETER_COLLECT_STRATEGY = "collection_strategy";

    // counter, either seconds or messages
    static final String PARAMETER_COLLECTION_COUNTER = "counter";

    private static final String[] OFFSET_TYPES = { "earliest", "latest"};
    //    private static final String[] OFFSET_TYPES = { "earliest", "latest", "lastX"};
    private static final int OFFSET_EARLIEST = 0;
    private static final int OFFSET_LATEST = 1;
    //  private static final int OFFSET_LAST_X = 2;

    private static final String[] COLLECTION_TYPES = { "duration", "number"};
    private static final int COLLECTION_DURATION = 0;
    private static final int COLLECTION_NUMBER = 1;

    // time out if waiting for messages
    private static final String PARAMETER_NUMBER_TIMEOUT = "time_out";

    // time out for the earliest poll of a topic
    private static final int POLL_TIMEOUT = 1;


    //private final InputPort input = getInputPorts().createPort("input stream", StreamDataContainer.class);

    private final OutputPort output = getOutputPorts().createPort("output data");


    public ReadKafkaTopic(OperatorDescription description) {

        super(description);

        getTransformer().addRule(new GenerateNewExampleSetMDRule(output) {

            @Override
            public void transformMD() {
                ExampleSetMetaData emd = new ExampleSetMetaData();
                emd.addAttribute(new AttributeMetaData("key", Ontology.STRING));
                emd.addAttribute(new AttributeMetaData("value", Ontology.STRING));
                emd.addAttribute(new AttributeMetaData("partition", Ontology.INTEGER));
                emd.addAttribute(new AttributeMetaData("offset", Ontology.INTEGER));
                output.deliverMD(emd);
            }
        });

    }




    @Override
    public void doWork() throws OperatorException {

        // FIXME: for testing the RapidMiner connection is ignored. Just to make sure every property is set correctly
//        ConnectionConfiguration connConfig = connectionSelector.getConnection().getConfiguration();
//        Properties clusterConfig = KafkaConnectionHandler.getINSTANCE().buildClusterConfiguration(connConfig);


        // building the returned example set
        List<Attribute> listOfAtts = new LinkedList<>();
        Attribute keyAttribute = AttributeFactory.createAttribute("key", Ontology.STRING);
        listOfAtts.add(keyAttribute);

        Attribute valueAttribute = AttributeFactory.createAttribute("value", Ontology.STRING);
        listOfAtts.add(valueAttribute);

        Attribute partitionAttribute = AttributeFactory.createAttribute("partition", Ontology.INTEGER);
        listOfAtts.add(partitionAttribute);

        Attribute offsetAttribute = AttributeFactory.createAttribute("offset", Ontology.INTEGER);
        listOfAtts.add(offsetAttribute);
        ExampleSetBuilder builder = ExampleSets.from(listOfAtts);


        String kafka_cluster = getParameterAsString(PARAMETER_KAFKA_CLUSTER);
        int kafka_port = getParameterAsInt(PARAMETER_KAFKA_PORT);
        String kafka_topic = getParameterAsString(PARAMETER_TOPIC);
        String kafka_offset_strategy = getParameterAsString(PARAMETER_AUTO_OFFSET);
        //  int max_poll_records = getParameterAsInt(PARAMETER_MAX_POLL_RECORDS);
        int retrieve_timeout = getParameterAsInt(PARAMETER_RETRIEVE_TIMEOUT);

        String collectStrategy = getParameterAsString(PARAMETER_COLLECT_STRATEGY);
        int counter = getParameterAsInt(PARAMETER_COLLECTION_COUNTER);
        int number_timeOut = getParameterAsInt(PARAMETER_NUMBER_TIMEOUT);

        Properties clusterConfig = new Properties();

        clusterConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,kafka_cluster+ ":" + kafka_port);
        clusterConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        clusterConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        // options here: earliest, latest
        if (kafka_offset_strategy.equals(OFFSET_TYPES[OFFSET_LATEST])) {
            clusterConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, kafka_offset_strategy);
        } else {
            clusterConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, OFFSET_TYPES[OFFSET_EARLIEST]);
        }
        clusterConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        //  clusterConfig.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, max_poll_records);
        //   clusterConfig.put(ConsumerConfig.FETCH_MAX_BYTES_CONFIG,52428800*2);
        // Group_ID as parameter?
        clusterConfig.put(ConsumerConfig.GROUP_ID_CONFIG,"RapidMinerKafkaExampleConsumer");





        KafkaConsumer<Object, Object> kafkaConsumer = new KafkaConsumer<>(clusterConfig);
        //     TestConsumerRebalanceListener rebalanceListener = new TestConsumerRebalanceListener();
        kafkaConsumer.subscribe(Collections.singletonList(kafka_topic));


// If the admin client should be used in the future, make sure that it is also closed with: client.close();
//        AdminClient client = AdminClient.create(clusterConfig);
//
//        DescribeTopicsResult topicDescription = client.describeTopics(Arrays.asList(kafka_topic));



        try {
            // FIXME: the poll duration is simply the timeout duration in case of a single poll (offset = earliest)
            Duration dur = Duration.ofSeconds(POLL_TIMEOUT);


            // start of the polling for the topic.
            // important for:
            // a) now when to stop the retrieving of earlier messages (if the producer is still running, this is the easiest option)
            // b) continues retrieving when collecting the latest messages
            Instant start = Instant.now();


//            int lastXMessages = 100;
//
//
//
//
//            if (kafka_offset_strategy.equals(OFFSET_TYPES[OFFSET_LAST_X])) {
//
//
//                List<PartitionInfo> partitions = kafkaConsumer.partitionsFor(kafka_topic);
//
//                //  kafkaConsumer.poll(dur);
//
//
//                ConsumerRecords<Object, Object> records= null;
//                while (true) {
//                    ConsumerRecords<Object, Object> tmp_records = kafkaConsumer.poll(dur);
//
////                    long maxOffset = 0;
////                    for (PartitionInfo part : partitions) {
////
////
////                        int partition = part.partition();
////                        long endPosition = kafkaConsumer.position(new TopicPartition(kafka_topic, partition));
////
////
////                        long offSet = endPosition - lastXMessages;
////                        maxOffset = offSet;
////                    }
//
//                    if (tmp_records.isEmpty() || Duration.between(start, Instant.now()).getSeconds() > retrieve_timeout) {
//                        break;
//                    } else {
//                        records = tmp_records;
//                    }
//                }
//
//                Iterator<ConsumerRecord<Object, Object>> iter = records.iterator();
//
//                while (iter.hasNext()) {
//
//
//                    checkForStop();
//                    ConsumerRecord<Object, Object> record = iter.next();
//
////                    if (record.offset() < maxOffset) {
////                        continue;
////                    }
//
//
//                    // record.offset();
//
//
//                    String key = record.key() == null ? "" : record.key().toString();
//                    String value = record.value().toString();
//
//                    double[] row = {keyAttribute.getMapping().mapString(key), valueAttribute.getMapping().mapString(value), record.partition(), record.offset()};
//                    builder.addRow(row);
//
//                }
//
//
//
//
//
//            } else
            if (kafka_offset_strategy.equals(OFFSET_TYPES[OFFSET_EARLIEST])) {

                //  Each poll might only get a part of the topic, so a loop is required to ensure you get the complete history
                while(true){
                    ConsumerRecords<Object, Object> records = kafkaConsumer.poll(dur);





                    if(records.isEmpty() || Duration.between(start, Instant.now()).getSeconds() > retrieve_timeout){
                        break;
                    }


                    for (ConsumerRecord<Object, Object> record : records) {


                        checkForStop();

                        String key = record.key() == null ? "" : record.key().toString();
                        String value = record.value().toString();

                        double[] row = {keyAttribute.getMapping().mapString(key), valueAttribute.getMapping().mapString(value), record.partition(), record.offset()};
                        builder.addRow(row);

                    }
                }
                // offest = "latest", which means keep running and retrieve incoming new records
            } else {


                while (counter > 0) {

                    checkForStop();
                    ConsumerRecords<Object, Object> records = kafkaConsumer.poll(Duration.ofSeconds(1));

                    if (!records.isEmpty()) {


                        for (ConsumerRecord<Object, Object> record : records) {


                            String key = record.key() == null ? "" : record.key().toString();
                            String value = record.value().toString();

                            double[] row = {keyAttribute.getMapping().mapString(key), valueAttribute.getMapping().mapString(value), record.partition(), record.offset()};
                            builder.addRow(row);

                            if (collectStrategy.equals(COLLECTION_TYPES[COLLECTION_NUMBER])) {
                                counter--;
                            }
                        }
                    }
                    // check how long the complete loop has run, for the timed poll
                    if (collectStrategy.equals(COLLECTION_TYPES[COLLECTION_DURATION])) {
                        if (Duration.between(start, Instant.now()).getSeconds() > counter) {
                            break;
                        }
                        // check how long the loop has run and waited for x messages so it can run into the time out
                    } else {
                        if (Duration.between(start, Instant.now()).getSeconds() > number_timeOut) {
                            break;
                        }

                    }
                }
            }
        } catch(ProcessStoppedException e){
            kafkaConsumer.unsubscribe();
            kafkaConsumer.close();
        }

        // important to close the connection when done
        kafkaConsumer.unsubscribe();
        kafkaConsumer.close();
        ExampleSet kafkaExampleSet = builder.build();
        output.deliver(kafkaExampleSet);




    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();

        ParameterType host = new ParameterTypeString(PARAMETER_KAFKA_CLUSTER, "address of kafka host", "localhost");
        types.add(host);

        ParameterType port = new ParameterTypeInt(PARAMETER_KAFKA_PORT, "port number of the kafka host",0, 65_535, 9092);
        types.add(port);

        ParameterType topic = new ParameterTypeString(PARAMETER_TOPIC, "Kafka topic to source.", false);
        types.add(topic);


        ParameterType offset = new ParameterTypeCategory(PARAMETER_AUTO_OFFSET, "Kafka offset strategy", OFFSET_TYPES, OFFSET_EARLIEST);
        types.add(offset);

//        ParameterType maxPoll = new ParameterTypeInt(PARAMETER_MAX_POLL_RECORDS, "maximum number of records to retrieve from a topic.", 1,Integer.MAX_VALUE, 5, false);
//        maxPoll.registerDependencyCondition(new EqualTypeCondition(this, PARAMETER_AUTO_OFFSET, OFFSET_TYPES, true, OFFSET_EARLIEST));
//        types.add(maxPoll);

        ParameterType wait = new ParameterTypeInt(PARAMETER_RETRIEVE_TIMEOUT, "timer to finish the retrieval of old messages", 1,Integer.MAX_VALUE, 2, false);
        wait.registerDependencyCondition(new EqualTypeCondition(this, PARAMETER_AUTO_OFFSET, OFFSET_TYPES, true, OFFSET_EARLIEST));
        types.add(wait);


        ParameterType strategy = new ParameterTypeCategory(PARAMETER_COLLECT_STRATEGY, "Waiting by time or number of records", COLLECTION_TYPES, COLLECTION_DURATION);
        strategy.registerDependencyCondition(new EqualTypeCondition(this, PARAMETER_AUTO_OFFSET, OFFSET_TYPES, true, OFFSET_LATEST));
        types.add(strategy);

        ParameterType counter = new ParameterTypeInt(PARAMETER_COLLECTION_COUNTER, "number of records or seconds to wait", 1,Integer.MAX_VALUE, 100, false);
        counter.registerDependencyCondition(new EqualTypeCondition(this, PARAMETER_AUTO_OFFSET, OFFSET_TYPES, true, OFFSET_LATEST));
        types.add(counter);


        ParameterType timeout = new ParameterTypeInt(PARAMETER_NUMBER_TIMEOUT, "max number seconds to wait for records to receive", 1,Integer.MAX_VALUE, 120, false);
        timeout.registerDependencyCondition(new EqualTypeCondition(this, PARAMETER_COLLECT_STRATEGY, COLLECTION_TYPES, true, COLLECTION_NUMBER));
        types.add(timeout);

        return types;


    }

/*    private static class TestConsumerRebalanceListener implements ConsumerRebalanceListener {
        @Override
        public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            System.out.println("Called onPartitionsRevoked with partitions:" + partitions);
        }

        @Override
        public void onPartitionsAssigned(Collection < TopicPartition > partitions) {
            System.out.println("Called onPartitionsAssigned with partitions:" + partitions);
        }
    }*/
}
