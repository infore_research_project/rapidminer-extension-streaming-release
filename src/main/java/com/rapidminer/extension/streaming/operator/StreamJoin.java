/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import java.util.List;
import java.util.logging.Logger;

import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.transform.JoinTransformer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeLong;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.LogService;


/**
 * Join operator for stream graphs
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamJoin extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String PARAMETER_LEFT_KEY = "left_key";

	private static final String PARAMETER_RIGHT_KEY = "right_key";

	private static final String PARAMETER_WINDOW_LENGTH = "window_length";

	private final InputPort input1 = getInputPorts().createPort("input stream 1", StreamDataContainer.class);

	private final InputPort input2 = getInputPorts().createPort("input stream 2", StreamDataContainer.class);

	private final OutputPort output = getOutputPorts().createPort("output stream");

	public StreamJoin(OperatorDescription description) {
		super(description);

		getTransformer().addPassThroughRule(input1, output);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();

		ParameterType leftKey = new ParameterTypeString(
			PARAMETER_LEFT_KEY,
			"Key of the value to be used on the left-stream.",
			false);
		types.add(leftKey);

		ParameterType rightKey = new ParameterTypeString(
			PARAMETER_RIGHT_KEY,
			"Key of the value to be used on the right-stream.",
			false);
		types.add(rightKey);

		ParameterType windowLength = new ParameterTypeLong(
			PARAMETER_WINDOW_LENGTH,
			"Length of the window to be used for joining.",
			0,
			Long.MAX_VALUE,
			false);
		types.add(windowLength);

		return types;
	}

	@Override
	public void doWork() throws OperatorException {
		StreamDataContainer left = input1.getData(StreamDataContainer.class);
		StreamDataContainer right = input2.getData(StreamDataContainer.class);

		StreamGraph graph = left.getStreamGraph();
		LOGGER.fine("Processing JOIN for: " + graph.getName());

		JoinTransformer join = new JoinTransformer.Builder(graph)
			.withLeftKey(getParameterAsString(PARAMETER_LEFT_KEY))
			.withRightKey(getParameterAsString(PARAMETER_RIGHT_KEY))
			.withWindowLength(getParameterAsLong(PARAMETER_WINDOW_LENGTH))
			.withLeftParent(left.getLastNode())
			.withRightParent(right.getLastNode())
			.build();

		StreamDataContainer outData = new StreamDataContainer(graph, join);
		output.deliver(outData);
	}

}