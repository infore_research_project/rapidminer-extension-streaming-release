/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator.marinetraffic;

import static com.rapidminer.extension.streaming.connection.StreamingConnectionHelper.createKafkaSelector;
import static com.rapidminer.extension.streaming.utility.JsonUtil.toJson;
import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;

import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.util.ConnectionInformationSelector;
import com.rapidminer.extension.streaming.connection.KafkaConnectionHandler;
import com.rapidminer.extension.streaming.deploy.RestClient;
import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.api.infore.maritime.Request;
import com.rapidminer.extension.streaming.utility.api.infore.maritime.TopicConfiguration;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.StreamProducer;
import com.rapidminer.extension.streaming.utility.graph.sink.KafkaSink;
import com.rapidminer.extension.streaming.utility.graph.source.KafkaSource;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.UserError;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.UndefinedParameterError;
import com.rapidminer.tools.LogService;


/**
 * Operator for interfacing with Marine Traffic's Akka cluster
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamMaritimeEventDetectionOperator extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String PARAMETER_KEY_FIELD = "key_field";

	private static final String PARAMETER_INPUT_TOPIC = "input_topic";

	private static final String PARAMETER_OUTPUT_TOPIC = "output_topic";

	private static final String PARAMETER_MT_HOST_PORT = "marine_traffic_cluster";

	private final ConnectionInformationSelector kafkaConn = createKafkaSelector(this, "kafka-connection");

	private final InputPort input = getInputPorts().createPort("input stream", StreamDataContainer.class);

	private final OutputPort output = getOutputPorts().createPort("output stream");

	private Properties kafkaConfig;

	/**
	 * Constructor
	 *
	 * @param description
	 */
	public StreamMaritimeEventDetectionOperator(OperatorDescription description) {
		super(description);
		getTransformer().addPassThroughRule(input, output);
	}

	@Override
	public void doWork() throws OperatorException {
		StreamDataContainer dataContainer = input.getData(StreamDataContainer.class);
		StreamGraph graph = dataContainer.getStreamGraph();
		LOGGER.fine("Processing StreamMaritimeEventDetectionOperator for: " + graph.getName());

		ConnectionConfiguration kafkaConnConfig = kafkaConn.getConnection().getConfiguration();
		kafkaConfig = KafkaConnectionHandler.getINSTANCE().buildClusterConfiguration(kafkaConnConfig);

		// Extend graph
		StreamProducer lastNode = extendGraph(dataContainer);

		// REST initiate AKKA job
		initiateJob();

		StreamDataContainer outData = new StreamDataContainer(graph, lastNode);
		output.deliver(outData);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = new ArrayList<>();
		types.add(new ParameterTypeString(PARAMETER_INPUT_TOPIC, "Input topic", false));
		types.add(new ParameterTypeString(PARAMETER_OUTPUT_TOPIC, "Output topic", false));
		types.add(new ParameterTypeString(PARAMETER_MT_HOST_PORT, "Host:Port for MarineTraffic cluster", false));
		types.add(new ParameterTypeString(PARAMETER_KEY_FIELD, "Name of key field to use for Kafka records", true));
		return types;
	}

	/**
	 * Extends the graph with the necessary nodes to communicate with the MarineTraffic AKKA cluster
	 *
	 * @param dataContainer
	 * @return last node
	 * @throws UndefinedParameterError
	 */
	private StreamProducer extendGraph(StreamDataContainer dataContainer) throws UndefinedParameterError {
		StreamGraph graph = dataContainer.getStreamGraph();
		StreamProducer lastNode = dataContainer.getLastNode();

		// Deliver data to MarineTraffic cluster
		new KafkaSink.Builder(graph)
			.withConfiguration(kafkaConfig)
			.withTopic(getParameterAsString(PARAMETER_INPUT_TOPIC))
			.withKey(getParameterAsString(PARAMETER_KEY_FIELD))
			.withParent(lastNode)
			.build();

		// Consume results from MarineTraffic cluster
		KafkaSource resultSource = new KafkaSource.Builder(graph)
			.withConfiguration(kafkaConfig)
			.withTopic(getParameterAsString(PARAMETER_OUTPUT_TOPIC))
			.build();

		graph.registerSource(resultSource);
		return resultSource;
	}

	/**
	 * This method initiates/deploys the AKKA cluster job on the MarineTraffic side via REST
	 *
	 * @throws UserError
	 */
	private void initiateJob() throws UserError {
		String url = "/api/job";
		String brokers = kafkaConfig.getProperty(BOOTSTRAP_SERVERS_CONFIG);
		String inTopic = getParameter(PARAMETER_INPUT_TOPIC);
		String outTopic = getParameter(PARAMETER_OUTPUT_TOPIC);

		Request req = new Request(new TopicConfiguration(brokers, inTopic), new TopicConfiguration(brokers, outTopic));

		// HTTP REST call to AKKA cluster
		try {
			String[] hostPort = StringUtils.split(getParameter(PARAMETER_MT_HOST_PORT), ':');
			RestClient client = new RestClient(hostPort[0], hostPort[1]);
			HttpEntity resp = client.post(url, toJson(req), ContentType.APPLICATION_JSON, "Could not initiate AKKA job");
			LOGGER.info("Maritime Event Detection job details: " + client.getResponseBody(resp));
		} catch (IOException|IllegalStateException e) {
			throw new UserError(this, e, "error.id");
		}
	}

}