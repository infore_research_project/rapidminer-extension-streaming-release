/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator.athena;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.util.ConnectionInformationSelector;
import com.rapidminer.extension.streaming.connection.KafkaConnectionHandler;
import com.rapidminer.extension.streaming.deploy.KafkaClient;
import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.StreamProducer;
import com.rapidminer.extension.streaming.utility.graph.infore.synopsis.BaseSynopsisNode;
import com.rapidminer.extension.streaming.utility.api.infore.synopsis.Request;
import com.rapidminer.extension.streaming.utility.api.infore.synopsis.RequestType;
import com.rapidminer.extension.streaming.utility.graph.infore.synopsis.SynopsisDataProducer;
import com.rapidminer.extension.streaming.utility.graph.infore.synopsis.SynopsisEstimateQuery;
import com.rapidminer.extension.streaming.utility.api.infore.synopsis.SynopsisType;
import com.rapidminer.extension.streaming.utility.graph.source.KafkaSource;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.UndefinedParameterError;
import com.rapidminer.parameter.conditions.BooleanParameterCondition;
import com.rapidminer.parameter.conditions.ParameterCondition;
import com.rapidminer.tools.LogService;
import org.apache.commons.lang3.StringUtils;

import static com.rapidminer.extension.streaming.connection.StreamingConnectionHelper.createKafkaSelector;
import static com.rapidminer.extension.streaming.utility.JsonUtil.toJson;


/**
 * Operator for interacting with the Synopsis Data Engine. 3 graph nodes will be created:
 * <ul>
 *     <li>{@link SynopsisEstimateQuery}: periodically or continuously queries SDE</li>
 *     <li>{@link SynopsisEstimateConsumer}: receives synopsis from SDE</li>
 *     <li>{@link SynopsisDataProducer}: producing data for SDE</li>
 * </ul>
 *
 * @author David Arnu, Mate Torok
 * @since 0.1.0
 */
public class StreamSDEOperator extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String ITEM_LIST_SEPARATOR = ",";
	private static final String PARAMETER_SYNOPSIS_TYPE = "synopsis_type";
	private static final String PARAMETER_SYNOPSIS_PARAMS = "synopsis_params";
	private static final String PARAMETER_SYNOPSIS_PARALLELISM = "synopsis_parallelism";
	private static final String PARAMETER_SYNOPSIS_DATA_SET_KEY = "data_set_key";
	private static final String PARAMETER_SYNOPSIS_U_ID = "u_id";
	private static final String PARAMETER_SYNOPSIS_STREAM_ID_KEY = "stream_id_key";
	private static final String PARAMETER_ESTIMATE_CONTINUOUS = "estimate_continuous";
	private static final String PARAMETER_ESTIMATE_FREQUENCY = "estimate_frequency";
	private static final String PARAMETER_ESTIMATE_PARAMS = "estimate_params";
	private static final String PARAMETER_REQUEST_TOPIC = "request_topic";
	private static final String PARAMETER_DATA_TOPIC = "data_topic";
	private static final String PARAMETER_OUTPUT_TOPIC = "output_topic";

	private final ConnectionInformationSelector connectionSDE = createKafkaSelector(this, "connection");

	private final InputPort input = getInputPorts().createPort("input stream", StreamDataContainer.class);

	private final OutputPort output = getOutputPorts().createPort("output stream");

	private Properties clusterConfig;

	public StreamSDEOperator(OperatorDescription description) {
		super(description);
		getTransformer().addPassThroughRule(input, output);
	}

	@Override
	public void doWork() throws OperatorException {
		StreamDataContainer inData = input.getData(StreamDataContainer.class);
		StreamGraph graph = inData.getStreamGraph();
		LOGGER.fine("Processing StreamSDEOperator for: " + graph.getName());

		// Setup cluster connection config
		ConnectionConfiguration connConfig = connectionSDE.getConnection().getConfiguration();
		clusterConfig = KafkaConnectionHandler.getINSTANCE().buildClusterConfiguration(connConfig);

		// Extend graph
		StreamProducer lastNode = extendGraph(inData);

		// Send "Add" and potentially continuous "Estimate" request to SDE
		configureSynopsisEngine();

		StreamDataContainer outData = new StreamDataContainer(graph, lastNode);
		output.deliver(outData);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();

		ParameterType synopsis = new ParameterTypeCategory(
			PARAMETER_SYNOPSIS_TYPE,
			"The synopsis applied on the stream.",
			Arrays.stream(SynopsisType.values()).map(Enum::toString).toArray(String[]::new),
			0,
			false);
		types.add(synopsis);

		ParameterType synDataSetKey = new ParameterTypeString(
			PARAMETER_SYNOPSIS_DATA_SET_KEY,
			"Data set key.",
			false);
		types.add(synDataSetKey);

		ParameterType snyParams = new ParameterTypeString(
			PARAMETER_SYNOPSIS_PARAMS,
			"Synopsis parameters (comma separated).",
			false);
		types.add(snyParams);

		ParameterType synParalellism = new ParameterTypeString(
			PARAMETER_SYNOPSIS_PARALLELISM,
			"Synopsis parallelism.",
			false);
		types.add(synParalellism);

		ParameterType synUId = new ParameterTypeInt(
			PARAMETER_SYNOPSIS_U_ID,
			"Synopsis UId.",
			0,
			Integer.MAX_VALUE,
			false);
		types.add(synUId);

		ParameterType synStreamId = new ParameterTypeString(
			PARAMETER_SYNOPSIS_STREAM_ID_KEY,
			"Key in the data that should be used as StreamId.",
			false);
		types.add(synStreamId);

		ParameterType estimateCont = new ParameterTypeBoolean(
			PARAMETER_ESTIMATE_CONTINUOUS,
			"Use continuous estimate.",
			false);
		types.add(estimateCont);
		ParameterCondition notContinuous = new BooleanParameterCondition(this, PARAMETER_ESTIMATE_CONTINUOUS, true, false);

		ParameterType estimateFreq = new ParameterTypeInt(
			PARAMETER_ESTIMATE_FREQUENCY,
			"Estimate frequency.",
			1,
			Integer.MAX_VALUE,
			false);
		estimateFreq.registerDependencyCondition(notContinuous);
		types.add(estimateFreq);

		ParameterType estimateParams = new ParameterTypeString(
			PARAMETER_ESTIMATE_PARAMS,
			"Estimate request params (comma separated).",
			true);
		estimateParams.registerDependencyCondition(notContinuous);
		types.add(estimateParams);

		ParameterType synReqTopic = new ParameterTypeString(PARAMETER_REQUEST_TOPIC, "Request topic.", false);
		types.add(synReqTopic);

		ParameterType synDataTopic = new ParameterTypeString(PARAMETER_DATA_TOPIC, "Data topic.", false);
		types.add(synDataTopic);

		ParameterType synOutputTopic = new ParameterTypeString(PARAMETER_OUTPUT_TOPIC, "Output topic.", false);
		types.add(synOutputTopic);

		return types;
	}

	/**
	 * Extends the graph with synopsis logic
	 *
	 * @param inData
	 * @return node representing the incoming synopsis data for the downstream
	 */
	private StreamProducer extendGraph(StreamDataContainer inData) throws UndefinedParameterError {
		StreamGraph graph = inData.getStreamGraph();
		StreamProducer lastNode = inData.getLastNode();

		// Data producer for SDE (sink, will be connected to the graph via its parent)
		SynopsisDataProducer dataProducer = enrichBuilder(new SynopsisDataProducer.Builder(graph))
			.withTopic(getParameterAsString(PARAMETER_DATA_TOPIC))
			.withParent(lastNode)
			.build();

		// Synopsis consumer from SDE (source)
		KafkaSource synopsisConsumer = new KafkaSource.Builder(graph)
			.withConfiguration(clusterConfig)
			.withTopic(getParameterAsString(PARAMETER_OUTPUT_TOPIC))
			.build();
		graph.registerSource(synopsisConsumer);

		// Estimate request emitter (source) if not continuous
		if (!getParameterAsBoolean(PARAMETER_ESTIMATE_CONTINUOUS)) {
			SynopsisEstimateQuery estimateQuery = enrichBuilder(new SynopsisEstimateQuery.Builder(graph))
				.withTopic(getParameterAsString(PARAMETER_REQUEST_TOPIC))
				.withFrequency(getParameterAsInt(PARAMETER_ESTIMATE_FREQUENCY))
				.withParameters(getParams(PARAMETER_ESTIMATE_PARAMS))
				.build();
			graph.registerSource(estimateQuery);
		}

		return synopsisConsumer;
	}

	/**
	 * For the sake of avoiding redundant code
	 *
	 * @param builder
	 * @throws UndefinedParameterError
	 */
	private <T extends BaseSynopsisNode.Builder<T>> T enrichBuilder(T builder) throws UndefinedParameterError {
		return builder
			.withConfiguration(clusterConfig)
			.withDataSetKey(getParameterAsString(PARAMETER_SYNOPSIS_DATA_SET_KEY))
			.withUId(getParameterAsInt(PARAMETER_SYNOPSIS_U_ID))
			.withParallelism(getParameterAsInt(PARAMETER_SYNOPSIS_PARALLELISM))
			.withStreamIdKey(getParameterAsString(PARAMETER_SYNOPSIS_STREAM_ID_KEY))
			.withSynopsis(SynopsisType.fromString(getParameterAsString(PARAMETER_SYNOPSIS_TYPE)));
	}

	/**
	 * Sends an "Add" request with the synopsis configuration to the appropriate Kafka topic thus activating the
	 * synopsis collection if not continuous. Otherwise a continuous estimation request will be dispatched.
	 */
	private void configureSynopsisEngine() throws OperatorException {
		KafkaClient kafkaClient = new KafkaClient(clusterConfig);
		String streamIdKey = getParameterAsString(PARAMETER_SYNOPSIS_STREAM_ID_KEY);
		String reqTopic = getParameterAsString(PARAMETER_REQUEST_TOPIC);
		String dataSetKey = getParameterAsString(PARAMETER_SYNOPSIS_DATA_SET_KEY);
		SynopsisType synopsis = SynopsisType.fromString(getParameterAsString(PARAMETER_SYNOPSIS_TYPE));
		int uid = getParameterAsInt(PARAMETER_SYNOPSIS_U_ID);
		String[] synParams = getParams(PARAMETER_SYNOPSIS_PARAMS);
		int parallelism = getParameterAsInt(PARAMETER_SYNOPSIS_PARALLELISM);

		// Request to send to SDE
		if (getParameterAsBoolean(PARAMETER_ESTIMATE_CONTINUOUS)) {
			LOGGER.fine("Sending Continuous ESTIMATE request to SDE");
			Request estimReq = new Request(dataSetKey, RequestType.CONTINUOUS, synopsis, uid, streamIdKey, synParams, parallelism);
			kafkaClient.send(reqTopic, estimReq.getDataSetKey(), toJson(estimReq));
		} else {
			LOGGER.fine("Sending ADD request to SDE");
			Request addReq = new Request(dataSetKey, RequestType.ADD, synopsis, uid, streamIdKey, synParams, parallelism);
			kafkaClient.send(reqTopic, addReq.getDataSetKey(), toJson(addReq));
		}
	}

	/**
	 * Takes the parameter as a string, splits it by ",", trims the elements and returns the resulting String array
	 *
	 * @param key
	 * @return see above
	 * @throws UndefinedParameterError
	 */
	private String[] getParams(String key) throws UndefinedParameterError {
		String param = getParameterAsString(key);
		if (param == null) {
			return null;
		} else {
			return Arrays.stream(StringUtils.split(param, ITEM_LIST_SEPARATOR))
			.peek(StringUtils::trim)
			.toArray(String[]::new);
		}
	}

}