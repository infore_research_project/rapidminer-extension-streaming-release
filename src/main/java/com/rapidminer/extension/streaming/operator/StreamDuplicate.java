/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import java.util.logging.Logger;

import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.transform.DuplicateStreamTransformer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.tools.LogService;


/**
 * Duplicate operator for stream graphs
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamDuplicate extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private final InputPort input = getInputPorts().createPort("input stream", StreamDataContainer.class);

	private final OutputPort output1 = getOutputPorts().createPort("output stream 1");

	private final OutputPort output2 = getOutputPorts().createPort("output stream 2");

	public StreamDuplicate(OperatorDescription description) {
		super(description);

		getTransformer().addPassThroughRule(input, output1);
		getTransformer().addPassThroughRule(input, output2);
	}

	@Override
	public void doWork() throws OperatorException {
		StreamDataContainer inData = input.getData(StreamDataContainer.class);
		StreamGraph graph = inData.getStreamGraph();
		LOGGER.fine("Processing DUPLICATE for: " + graph.getName());

		DuplicateStreamTransformer duplicate = new DuplicateStreamTransformer.Builder(graph)
			.withParent(inData.getLastNode())
			.build();

		StreamDataContainer outData1 = new StreamDataContainer(graph, duplicate);
		StreamDataContainer outData2 = new StreamDataContainer(graph, duplicate);
		output1.deliver(outData1);
		output2.deliver(outData2);
	}

}