/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import static com.rapidminer.extension.streaming.connection.StreamingConnectionHelper.createKafkaSelector;

import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.util.ConnectionInformationSelector;
import com.rapidminer.extension.streaming.connection.KafkaConnectionHandler;
import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.sink.KafkaSink;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.LogService;


/**
 * Kafka sink operator for stream graphs
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamKafkaSink extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String PARAMETER_RECORD_KEY = "record_key";

	static final String PARAMETER_TOPIC = "topic";

	private final ConnectionInformationSelector connectionSelector = createKafkaSelector(this, "connection");

	private final InputPort input = getInputPorts().createPort("input stream", StreamDataContainer.class);

	public StreamKafkaSink(OperatorDescription description) {
		super(description);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();

		ParameterType topic = new ParameterTypeString(PARAMETER_TOPIC, "Kafka topic to source.", false);
		types.add(topic);

		ParameterType key = new ParameterTypeString(PARAMETER_RECORD_KEY, "Name of the field to use for record key.", true);
		types.add(key);

		return types;
	}

	@Override
	public void doWork() throws OperatorException {
		StreamDataContainer inData = input.getData(StreamDataContainer.class);
		StreamGraph graph = inData.getStreamGraph();
		LOGGER.fine("Processing KAFKA-SINK for: " + graph.getName());

		ConnectionConfiguration connConfig = connectionSelector.getConnection().getConfiguration();
		Properties clusterConfig = KafkaConnectionHandler.getINSTANCE().buildClusterConfiguration(connConfig);

		new KafkaSink.Builder(graph)
			.withConfiguration(clusterConfig)
			.withTopic(getParameterAsString(PARAMETER_TOPIC))
			.withKey(getParameterAsString(PARAMETER_RECORD_KEY))
			.withParent(inData.getLastNode())
			.build();
	}

}
