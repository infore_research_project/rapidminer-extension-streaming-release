/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import java.util.List;
import java.util.logging.Logger;

import com.google.common.collect.Sets;
import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.transform.ParseFieldTransformer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeEnumeration;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.LogService;


/**
 * Parse field operator for stream graphs
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamParseField extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String PARAMETER_KEY = "key";

	private static final String PARAMETER_KEYS = "keys";

	private final InputPort input = getInputPorts().createPort("input stream", StreamDataContainer.class);

	private final OutputPort output = getOutputPorts().createPort("output stream");

	public StreamParseField(OperatorDescription description) {
		super(description);

		getTransformer().addPassThroughRule(input, output);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();

		ParameterType key = new ParameterTypeString(PARAMETER_KEY, "Key to be parsed", false);
		ParameterType keys = new ParameterTypeEnumeration(PARAMETER_KEYS, "Keys to be parsed.", key);
		types.add(keys);

		return types;
	}

	@Override
	public void doWork() throws OperatorException {
		StreamDataContainer inData = input.getData(StreamDataContainer.class);
		StreamGraph graph = inData.getStreamGraph();
		LOGGER.fine("Processing PARSE-FIELD for: " + graph.getName());

		ParseFieldTransformer parseField = new ParseFieldTransformer.Builder(graph)
			.withKeys(Sets.newHashSet(ParameterTypeEnumeration.transformString2List(getParameterAsString(PARAMETER_KEYS))))
			.withParent(inData.getLastNode())
			.build();

		StreamDataContainer outData = new StreamDataContainer(graph, parseField);
		output.deliver(outData);
	}

}