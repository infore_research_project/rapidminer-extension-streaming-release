/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import static com.rapidminer.extension.streaming.connection.StreamingConnectionHelper.createKafkaSelector;

import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.util.ConnectionInformationSelector;
import com.rapidminer.extension.streaming.connection.KafkaConnectionHandler;
import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.source.KafkaSource;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.LogService;


/**
 * Kafka source for stream graphs
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamKafkaSource extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String PARAMETER_START_FROM_EARLIEST = "start_from_earliest";

	static final String PARAMETER_TOPIC = "topic";

	private final ConnectionInformationSelector connectionSelector = createKafkaSelector(this, "connection");

	private final OutputPort output = getOutputPorts().createPort("output stream");

	public StreamKafkaSource(OperatorDescription description) {
		super(description);

		getTransformer().addGenerationRule(output, StreamDataContainer.class);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();

		ParameterType topic = new ParameterTypeString(PARAMETER_TOPIC, "Kafka topic to source.", false);
		types.add(topic);

		ParameterType earliest = new ParameterTypeBoolean(PARAMETER_START_FROM_EARLIEST, "Start from earliest event", false);
		types.add(earliest);

		return types;
	}

	@Override
	public void doWork() throws OperatorException {
		StreamGraph graph = ((StreamingNest) getExecutionUnit().getEnclosingOperator()).getGraph();
		LOGGER.fine("Processing KAFKA-SOURCE for: " + graph.getName());

		ConnectionConfiguration connConfig = connectionSelector.getConnection().getConfiguration();
		Properties clusterConfig = KafkaConnectionHandler.getINSTANCE().buildClusterConfiguration(connConfig);

		KafkaSource source = new KafkaSource.Builder(graph)
			.withConfiguration(clusterConfig)
			.withTopic(getParameterAsString(PARAMETER_TOPIC))
			.withStartFromEarliest(getParameterAsBoolean(PARAMETER_START_FROM_EARLIEST))
			.build();

		graph.registerSource(source);
		StreamDataContainer outData = new StreamDataContainer(graph, source);
		output.deliver(outData);
	}

}