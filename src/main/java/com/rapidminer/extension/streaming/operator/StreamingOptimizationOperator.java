/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import static com.rapidminer.connection.util.ConnectionI18N.getTypeName;
import static com.rapidminer.extension.streaming.PluginInitStreaming.getPluginLoader;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Pair;

import com.rapidminer.connection.ConnectionInformationContainerIOObject;
import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.extension.streaming.connection.KafkaConnectionHandler;
import com.rapidminer.extension.streaming.connection.StreamConnectionHandler;
import com.rapidminer.extension.streaming.connection.StreamingConnectionHelper;
import com.rapidminer.extension.streaming.optimizer.OptimizerConnection;
import com.rapidminer.extension.streaming.optimizer.agnostic_workflow.AWOperator;
import com.rapidminer.extension.streaming.optimizer.agnostic_workflow.AWOperatorConnection;
import com.rapidminer.extension.streaming.optimizer.agnostic_workflow.AgnosticWorkflow;
import com.rapidminer.extension.streaming.optimizer.agnostic_workflow.AgnosticWorkflowConversion;
import com.rapidminer.extension.streaming.optimizer.agnostic_workflow.SplittedConnection;
import com.rapidminer.extension.streaming.optimizer.settings.Network;
import com.rapidminer.extension.streaming.optimizer.settings.OperatorDictionary;
import com.rapidminer.extension.streaming.optimizer.settings.OptimizerRequest;
import com.rapidminer.extension.streaming.optimizer.settings.OptimizerRequest.OptimizerAlgorithm;
import com.rapidminer.extension.streaming.optimizer.settings.OptimizerRequest.OptimizerVersion;
import com.rapidminer.extension.streaming.optimizer.settings.OptimizerResponse;
import com.rapidminer.extension.streaming.utility.JsonUtil;
import com.rapidminer.gui.RapidMinerGUI;
import com.rapidminer.gui.flow.ProcessPanel;
import com.rapidminer.gui.flow.processrendering.view.ProcessRendererController;
import com.rapidminer.gui.flow.processrendering.view.ProcessRendererView;
import com.rapidminer.gui.tools.ResourceAction;
import com.rapidminer.operator.ExecutionUnit;
import com.rapidminer.operator.IOMultiplier;
import com.rapidminer.operator.IOObject;
import com.rapidminer.operator.IOObjectCollection;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorChain;
import com.rapidminer.operator.OperatorCreationException;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.PortUserError;
import com.rapidminer.operator.UserError;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.InputPortExtender;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.operator.ports.metadata.MetaData;
import com.rapidminer.operator.ports.metadata.SubprocessTransformRule;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeEnumeration;
import com.rapidminer.parameter.ParameterTypeFile;
import com.rapidminer.parameter.ParameterTypeLinkButton;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.conditions.BooleanParameterCondition;
import com.rapidminer.tools.ClassLoaderSwapper;
import com.rapidminer.tools.LogService;
import com.rapidminer.tools.OperatorService;


/**
 * This {@link OperatorChain} provides the possibility to design a logical streaming workflow in
 * it's subprocess and only provide a collection of connections to streaming platforms on which the
 * workflow can be deployed. The INFORE optimizer is then used to decide on an optimized placement
 * of the operators in the workflow at the different platforms.
 * <p>
 * Upon execution ({@link #doWork()} of the operator, the designed workflow (in the subprocess of
 * the operator) is converted into its {@link AgnosticWorkflow} representation. In addition all
 * configurations for the INFORE optimizer are created as well ({@link Network}, {@link
 * OperatorDictionary}, {@link OptimizerRequest}). This is provided to the INFORE optimizer, which
 * performs an optimization of the workflow based on the provided information.
 * <p>
 * The received optimized workflow is then used to update the inner subprocess (create the
 * corresponding Streaming Nest operators, place the operators inside the Nests according to the
 * optimization, restore splitted connections).
 * <p>
 * When the inner subprocess is updated it is executed, which causes the deployment of the streaming
 * workflows on the different platforms. The optimized workflow can also be inspected in the
 * RapidMiner GUI.
 * <p>
 * The connection information to the Optimizer Service have to be provided. The operator also
 * provides the option to write the different configurations ({@link AgnosticWorkflow}, {@link
 * Network}, {@link OperatorDictionary}, {@link OptimizerRequest}) and the response of the optimizer
 * to disk. If this option is selected, the input for the update of the inner subprocess (which is
 * normally the response of the INFORE optimizer) is also read from disk and the updated {@link
 * AgnosticWorkflow} is written to disk as well. All file locations can be controlled by
 * corresponding parameters. This allows for easy manipulation of the interaction with the
 * optimizer, to test things.
 * <p>
 * The operator also provides the option to perform a dummy optimization without an actual execution
 * of the optimized workflow.
 *
 * @author Fabian Temme
 * @since 0.1.0
 */
public class StreamingOptimizationOperator extends OperatorChain {
	
	/**
	 * Internal field to directly access the Logger.
	 */
	private static final Logger LOGGER = LogService.getRoot();
	
	/**
	 * This input port is used to provide the connection to the Kafka cluster which is used to
	 * communicate between the different computing sites and platforms.
	 */
	private final InputPort kafkaInputPort = getInputPorts().createPort("kafka connection");
	
	/**
	 * This input port is used to provide the collection of available streaming backends on which
	 * the workflow can be deployed.
	 */
	//	private final InputPort streamingBackendsInputPort = getInputPorts().createPort(
	//			"streaming site");
	
	private final InputPortExtender streamingBackendsInputPort = new InputPortExtender(
			"streaming site", getInputPorts());
	
	/**
	 * This output port is used to provide the kafka connection received at the {@link
	 * #kafkaInputPort} to the inner subprocess.
	 */
	private OutputPort kafkaOutputPort = null;
	
	/**
	 * work in progress, will be maybe deleted later
	 */
	public static final String PARAMETER_WRITE_AND_READ_FROM_JSON = "write_and_read_from_json";
	/**
	 * work in progress, will be maybe deleted later
	 */
	public static final String PARAMETER_NETWORK_JSON = "network_(write)";
	/**
	 * work in progress, will be maybe deleted later
	 */
	public static final String PARAMETER_DICTIONARY_JSON = "dictionary_(write)";
	/**
	 * work in progress, will be maybe deleted later
	 */
	public static final String PARAMETER_REQUEST_JSON = "request_(write)";
	/**
	 * work in progress, will be maybe deleted later
	 */
	public static final String PARAMETER_OUTPUT_WORKFLOW_JSON = "workflow_(write)";
	/**
	 * work in progress, will be maybe deleted later
	 */
	public static final String PARAMETER_OPTIMIZER_RESPONSE = "optimizer_response_(write)";
	/**
	 * work in progress, will be maybe deleted later
	 */
	public static final String PARAMETER_OUTPUT_OPTIMIZED_WORKFLOW_JSON = "optimized_workflow_(write)";
	/**
	 * work in progress, will be maybe deleted later
	 */
	public static final String PARAMETER_INPUT_OPTIMIZER_RESPONSE_JSON = "optimizer_response_(read)";
	
	public static final String PARAMETER_HOST = "host";
	public static final String PARAMETER_PORT = "port";
	public static final String PARAMETER_USER = "user";
	public static final String PARAMETER_PASSWORD = "password";
	
	public static final String PARAMETER_VERSION = "optimizer_version";
	public static final String PARAMETER_ALGORITHM = "optimizer_algorithm";
	
	public static final String PARAMETER_NETWORK_NAME = "network_name";
	public static final String PARAMETER_DICTIONARY_NAME = "dictionary_name";
	public static final String PARAMETER_WORKFLOW_NAME = "workflow_name";
	
	public static final String PARAMETER_STREAMING_SITES_NAMES = "streaming_sites_names";
	
	/**
	 * Parameter key for only performing a dummy optimization without executing the actual
	 * subprocess.
	 */
	public static final String PARAMETER_DUMMY_OPTIMIZATION = "dummy_optimization";
	
	/**
	 * Parameter key for the restore original process action.
	 */
	public static final String PARAMETER_RESTORE_ORIGINAL_PROCESS = "restore_original_process";
	
	/**
	 * Internal field to store the original logical workflow, so that it can be restored.
	 */
	private AgnosticWorkflow originalSubprocess = null;
	/**
	 * Internal variable to keep track if the optimization already happened.
	 */
	private boolean alreadyOptimized = false;
	/**
	 * Mapping between {@link ConnectionInformationContainerIOObject}s received at the {@link
	 * #streamingBackendsInputPort} and the inner output ports which are created after the
	 * optimization to provide the corresponding connection objects to the inner subprocess.
	 */
	private final Map<OutputPort,ConnectionInformationContainerIOObject> innerConnectionPorts = new LinkedHashMap<>();
	/**
	 * work in progress, will be maybe deleted later
	 */
	private boolean writeAndReadFromJSON = false;
	
	/**
	 * {@link ResourceAction} to be performed in the design phase in RapidMiner Studio to restore
	 * the original logical process after an optimization has happened.
	 */
	private final ResourceAction action = new ResourceAction(
			"streaming_optimization.restore_original_process") {
		
		private static final long serialVersionUID = -1243790237342363966L;
		
		@Override
		public void actionPerformed(ActionEvent event) {
			if (originalSubprocess != null) {
				StreamingOptimizationOperator operator = StreamingOptimizationOperator.this;
				try {
					operator.updateSubprocess(operator.originalSubprocess);
					operator.alreadyOptimized = false;
				} catch (OperatorCreationException e) {
					getRoot().getLogger().info(e.getMessage());
				}
			}
		}
		
	};
	
	public StreamingOptimizationOperator(OperatorDescription description) {
		super(description, "Streaming");
		streamingBackendsInputPort.start();
		
		getTransformer().addRule(() -> {
			// if the kafkaOutputPort was created, we can through put the meta data of the #kafkaInputPort
			if (kafkaOutputPort != null) {
				MetaData md = kafkaInputPort.getMetaData();
				if (md != null) {
					md = md.clone();
					md.addToHistory(kafkaOutputPort);
					kafkaOutputPort.deliverMD(md);
				}
			}
		});
		getTransformer().addRule(() -> {
			// if the inner output ports for the streaming connections are created, we can
			// through put the meta data of the corresponding
			// ConnectionInformationContainerIOObjects
			for (Entry<OutputPort,ConnectionInformationContainerIOObject> entry : innerConnectionPorts
					.entrySet()) {
				entry.getKey().deliverMD(MetaData.forIOObject(entry.getValue()));
			}
		});
		getTransformer().addRule(new SubprocessTransformRule(getSubprocess(0)));
	}
	
	@Override
	public void doWork() throws OperatorException {
		// Load the ClassLoader of the Streaming extension, so that we can create Streaming operators
		try (ClassLoaderSwapper cls = ClassLoaderSwapper.withContextClassLoader(
				getPluginLoader())) {
			writeAndReadFromJSON = getParameterAsBoolean(PARAMETER_WRITE_AND_READ_FROM_JSON);
			
			ConnectionInformationContainerIOObject kafkaConnection = kafkaInputPort.getData(
					ConnectionInformationContainerIOObject.class);
			// throw a UserError, if this is not a kafka connection
			if (!kafkaConnection.getConnectionInformation()
								.getConfiguration()
								.getType()
								.equals(KafkaConnectionHandler.getINSTANCE().getType())) {
				throw new PortUserError(kafkaInputPort, "connection.mismatched_type",
										getTypeName(KafkaConnectionHandler.getINSTANCE().getType()),
										getTypeName(kafkaConnection.getConnectionInformation()
																   .getConfiguration()
																   .getType()));
			}
			
			// Retrieve the collection of streaming backends for the optimization
			List<IOObject> streamingSites = streamingBackendsInputPort.getData(IOObject.class,
																			   false);
			List<String> siteNames = ParameterTypeEnumeration.transformString2List(
					getParameterAsString(PARAMETER_STREAMING_SITES_NAMES));
			Map<String,List<ConnectionInformationContainerIOObject>> availableSites = new LinkedHashMap<>();
			// Throw a User Error if the collection contains non-supported connections.
			Map<String,StreamConnectionHandler> supportedConnections = StreamingConnectionHelper.createConnectionHandlerMap();
			int i = 0;
			for (IOObject site : streamingSites) {
				List<ConnectionInformationContainerIOObject> currentPlatforms = new ArrayList<>();
				if (site instanceof ConnectionInformationContainerIOObject) {
					currentPlatforms.add((ConnectionInformationContainerIOObject) site);
				} else if (site instanceof IOObjectCollection) {
					IOObjectCollection<ConnectionInformationContainerIOObject> collection = (IOObjectCollection<ConnectionInformationContainerIOObject>) site;
					currentPlatforms = collection.getObjects();
				} else {
					// throw exception
				}
				for (ConnectionInformationContainerIOObject conn : currentPlatforms) {
					if (!supportedConnections.containsKey(
							conn.getConnectionInformation().getConfiguration().getType())) {
						throw new PortUserError(streamingBackendsInputPort.getManagedPorts().get(i),
												"connection.mismatched_type",
												conn.getConnectionInformation()
													.getConfiguration()
													.getType(),
												StringUtils.join(supportedConnections.keySet(),
																 " or "));
					}
				}
				String siteName = "site" + i;
				if (siteNames.size() > i) {
					siteName = siteNames.get(i);
				} else {
					for (int j = 0; availableSites.containsKey(siteName); j++) {
						siteName = "site" + ( i + j );
					}
				}
				availableSites.put(siteName, currentPlatforms);
				i++;
			}
			
			// We don't need to optimize again
			// TODO: This may change if we want to do runtime adaptions
			if (!alreadyOptimized) {
				try {
					performOptimization(availableSites);
				} catch (IOException | OperatorCreationException e) {
					LOGGER.warning("Exception!: " + e.getMessage());
				}
			}
			
			// Deliver the KafkaConnection to the (inner) kafkaOutputPort and the connections to
			// the streaming backends to the corresponding inner connection ports (which were
			// created in performOptimization)
			kafkaOutputPort.deliver(kafkaConnection);
			for (Entry<OutputPort,ConnectionInformationContainerIOObject> entry : innerConnectionPorts
					.entrySet()) {
				entry.getKey().deliver(entry.getValue());
			}
			// Only execute the updated inner subprocess if we don't do a dummy optimization
			if (!getParameterAsBoolean(PARAMETER_DUMMY_OPTIMIZATION)) {
				super.doWork();
			}
		}
	}
	
	/**
	 * This methods performs the optimization of the designed logical workflow.
	 * <p>
	 * First the needed configurations ({@link Network}, {@link OperatorDictionary} JSONs are
	 * created, and the {@link AgnosticWorkflow} is received from the inner subprocess of the
	 * operator. The {@link OptimizerVersion} and {@link OptimizerAlgorithm} are received from the
	 * corresponding parameters. Finally with the names of the different configuration JSON files,
	 * an {@link OptimizerRequest} is created.
	 * <p>
	 * If {@link #writeAndReadFromJSON} is {@code true}, the corresponding files are written to
	 * disks.
	 * <p>
	 * A connection to the Optimizer Service is opened by using an {@link OptimizerConnection}
	 * instance. The workflow is optimized by calling {@link OptimizerConnection#optimizeWorkflow(String,
	 * String, String, String)}.
	 * <p>
	 * If {@link #writeAndReadFromJSON} is {@code true}, the optimizer response is written to disk
	 * and the response which is used in the operator is updated by reading from disk.
	 * <p>
	 * The response is used to update the inner subprocess (see {@link
	 * AgnosticWorkflowConversion#updateOptimizedWorkflow(AgnosticWorkflow)}), to create the inner
	 * {@link #kafkaOutputPort} and create and fill the {@link #innerConnectionPorts} and also
	 * splitted connections are updated ({@link #updateSplittedConnections(ExecutionUnit, List)}).
	 *
	 * @param availableSites
	 * @throws IOException
	 * @throws UserError
	 * @throws OperatorCreationException
	 */
	private void performOptimization(
			Map<String,List<ConnectionInformationContainerIOObject>> availableSites) throws
			IOException,
			UserError,
			OperatorCreationException {
		ExecutionUnit subprocess = null;
		
		// Create the necessary objects for the optimizer
		Network network = Network.create(getParameterAsString(PARAMETER_NETWORK_NAME),
										 availableSites);
		OperatorDictionary dictionary = OperatorDictionary.create(
				getParameterAsString(PARAMETER_DICTIONARY_NAME));
		AgnosticWorkflow workflow = AgnosticWorkflowConversion.processToAgnosticWorkflow(
				getSubprocess(0)).setWorkflowName(getParameterAsString(PARAMETER_WORKFLOW_NAME));
		OptimizerVersion version = OptimizerVersion.getMethod(
				getParameterAsString(PARAMETER_VERSION));
		OptimizerAlgorithm algorithm = OptimizerAlgorithm.getMethod(
				getParameterAsString(PARAMETER_ALGORITHM));
		OptimizerRequest request = OptimizerRequest.create("request", network.getNetwork(),
														   dictionary.getDictionaryName(),
														   workflow.getWorkflowName(), version,
														   algorithm);
		// Copy the original workflow, so that we can restore it.
		originalSubprocess = workflow.copy();
		
		// Convert the objects for the optimizer to JSON strings
		String networkJSON = JsonUtil.objectToJsonString(network);
		String dictionaryJSON = JsonUtil.objectToJsonString(dictionary);
		String workflowJSON = JsonUtil.objectToJsonString(workflow);
		String requestJSON = JsonUtil.objectToJsonString(request);
		
		// If writeAndReadFromJSON is true, write the json strings to disk
		if (writeAndReadFromJSON) {
			try (FileWriter networkWriter = new FileWriter(
					getParameterAsFile(PARAMETER_NETWORK_JSON));
				 FileWriter requestWriter = new FileWriter(
						 getParameterAsFile(PARAMETER_REQUEST_JSON));
				 FileWriter dictionaryWriter = new FileWriter(
						 getParameterAsFile(PARAMETER_DICTIONARY_JSON));
				 FileWriter workflowWriter = new FileWriter(
						 getParameterAsFile(PARAMETER_OUTPUT_WORKFLOW_JSON))) {
				networkWriter.write(networkJSON);
				requestWriter.write(requestJSON);
				dictionaryWriter.write(dictionaryJSON);
				workflowWriter.write(workflowJSON);
			} catch (IOException e) {
				LOGGER.warning("Exception!: " + e.getMessage());
			}
		}
		OptimizerConnection optimizerConnection = new OptimizerConnection(
				getParameterAsString(PARAMETER_HOST), getParameterAsString(PARAMETER_PORT),
				getParameterAsString(PARAMETER_USER), getParameterAsString(PARAMETER_PASSWORD),
				getLogger());
		// Let the OptimizerConnection perform the actual optimization. It will send the json
		// strings to the optimizer service, receive back the response, correctly
		// format it and return it.
		String optimizerResponseJSON = optimizerConnection.optimizeWorkflow(networkJSON,
																			dictionaryJSON,
																			workflowJSON,
																			requestJSON);
		
		// If writeAndReadFromJSON is true, write the response of the optimizer to disk. And read in
		// another json file which is used as the optimizer response. Copy the written file, change
		// entries to test the placement of the operators and store it as the input optimizer
		// response.
		if (writeAndReadFromJSON) {
			try (FileWriter optimizerResponseWriter = new FileWriter(
					getParameterAsFile(PARAMETER_OPTIMIZER_RESPONSE))) {
				optimizerResponseWriter.write(optimizerResponseJSON);
			} catch (IOException e) {
				LOGGER.warning("Exception!: " + e.getMessage());
			}
			try (BufferedReader inputOptimizerResponseReader = new BufferedReader(
					new FileReader(getParameterAsFile(PARAMETER_INPUT_OPTIMIZER_RESPONSE_JSON)))) {
				optimizerResponseJSON = inputOptimizerResponseReader.lines()
																	.collect(Collectors.joining(
																			" "));
			}
		}
		
		// Convert the optimizer response back to an AgnosticWorkflow object.
		AgnosticWorkflow optimizerResponseAW = JsonUtil.jsonStringToObject(optimizerResponseJSON,
																		   OptimizerResponse.class)
													   .getWorkflow();
		// Perform an update of the AgnosticWorkflow, which actually creates the Streaming Nest
		// operators and place all operators in the corresponding nests. Also the the operator
		// connections which are inside one nest are placed in the corresponding nest. All other
		// connections (which are splitted between two nests) are returned in the second value of
		// the updateAWPair.
		Pair<AgnosticWorkflow,List<SplittedConnection>> updateAWPair = AgnosticWorkflowConversion.updateOptimizedWorkflow(
				optimizerResponseAW);
		AgnosticWorkflow optimizedWorkflow = updateAWPair.getFirst();
		
		// Update the subprocess with the optimized AgnosticWorkflow.
		subprocess = updateSubprocess(optimizedWorkflow);
		// Create the inner kafka port, which is used for the splitted streaming connections
		// (connections between two streaming operators)
		kafkaOutputPort = getSubprocess(0).getInnerSources().createPort("kafka connection");
		// Create the inner streaming backend connection ports and connect them to the corresponding
		// Streaming Nests
		createAndConnectStreamingBackends(optimizedWorkflow, subprocess, availableSites);
		
		// Update the splitted connections, by adding Kafka Sink and Kafka Source operators (using
		// the KafkaConnection from the kafkaOutputPort) for splitted streaming connections, and
		// wire other connections through the throughput ports of the Streaming Nest operators.
		subprocess = updateSplittedConnections(subprocess, updateAWPair.getSecond());
		
		// arrange all operators
		ProcessPanel processPanel = RapidMinerGUI.getMainFrame().getProcessPanel();
		ProcessRendererView view = processPanel.getProcessRenderer();
		ProcessRendererController controller = new ProcessRendererController(view, view.getModel());
		// without the next line, the operators won't have positions yet and arrange & annotations
		// will fail...
		controller.autoFit();
		
		// we collect all subprocesses here - it turns out that we can actually arrange operators in
		// subprocesses,
		// even if they are currently not visible...
		List<ExecutionUnit> allSubprocesses = new ArrayList<>();
		addSubprocesses(allSubprocesses, this);
		controller.autoArrange(allSubprocesses);
		
		alreadyOptimized = true;
	}
	
	/**
	 * Adds all execution units of the current operator (if it is a chain) to the list. Invokes the
	 * method recursively for all children.
	 *
	 * @param allSubprocesses
	 * 		the list to add the execution units to
	 * @param operator
	 * 		the operator
	 */
	private void addSubprocesses(List<ExecutionUnit> allSubprocesses, Operator operator) {
		if (operator instanceof OperatorChain) {
			OperatorChain operatorChain = (OperatorChain) operator;
			for (int i = 0; i < operatorChain.getNumberOfSubprocesses(); i++) {
				allSubprocesses.add(operatorChain.getSubprocess(i));
			}
			for (Operator innerOperator : operatorChain.getAllInnerOperators()) {
				addSubprocesses(allSubprocesses, innerOperator);
			}
		}
	}
	
	/**
	 * Updates the inner subprocess of the {@link StreamingOptimizationOperator} with the
	 * information in the provided {@link AgnosticWorkflow}.
	 * <p>
	 * First the inner subprocess is cleared by removing the {@link #kafkaOutputPort} and all inner
	 * {@link OutputPort}s in the {@link #innerConnectionPorts} map. Also all inner operators are
	 * removed.
	 * <p>
	 * Then the inner subprocess is updated by calling {@link AgnosticWorkflowConversion#agnosticWorkflowToProcess(OperatorChain,
	 * AgnosticWorkflow, int)}.
	 *
	 * @param workflow
	 *        {@link AgnosticWorkflow} used to update the inner subprocess
	 * @return inner subprocess of the {@link StreamingOptimizationOperator} updated with the
	 * information in the provided {@link AgnosticWorkflow}.
	 * @throws OperatorCreationException
	 */
	private ExecutionUnit updateSubprocess(AgnosticWorkflow workflow) throws
			OperatorCreationException {
		ExecutionUnit subprocess = getSubprocess(0);
		if (kafkaOutputPort != null) {
			subprocess.getInnerSources().removePort(kafkaOutputPort);
			kafkaOutputPort = null;
		}
		for (Operator operator : getAllInnerOperators()) {
			operator.remove();
		}
		for (OutputPort port : innerConnectionPorts.keySet()) {
			subprocess.getInnerSources().removePort(port);
		}
		innerConnectionPorts.clear();
		subprocess = AgnosticWorkflowConversion.agnosticWorkflowToProcess(this, workflow, 0);
		fireUpdate();
		return subprocess;
	}
	
	/**
	 * Updates the provided {@code subprocess} with the provided list of {@link
	 * SplittedConnection}.
	 * <p>
	 * The methods loops through the {@link SplittedConnection}s. If the current connection is a
	 * streaming connections a pair of {@link StreamKafkaSink} and {@link StreamKafkaSource}
	 * operators is created, which recreates the splitted connections for the workflow. The kafka
	 * connection provided at the {@link #kafkaInputPort} (and through putted to the {@link
	 * #kafkaOutputPort}) is used for the Kafka Sink and Source operators. A corresponding {@link
	 * IOMultiplier} operator is created as well to provide the kafka connection to all new sink and
	 * source operators.
	 * <p>
	 * If the connection is not a streaming connection, it is restored by wiring the connection
	 * through the throughput ports of the corresponding {@link StreamingNest} operators.
	 *
	 * @param subprocess
	 *        {@link ExecutionUnit} for which the splitted connections shall be updated
	 * @param connections
	 *        {@link SplittedConnection}s which shall be restored
	 * @return subprocess with restored splited connections
	 * @throws OperatorCreationException
	 */
	private ExecutionUnit updateSplittedConnections(ExecutionUnit subprocess,
			List<SplittedConnection> connections) throws OperatorCreationException {
		// check if we need to create a Multiply operator, cause we have splitted streaming
		// connections.
		int multiplyPortIndex = 0;
		IOMultiplier multiply = null;
		for (SplittedConnection connection : connections) {
			if (connection.isStreamingConnection()) {
				// if this multiply is still null, create it
				if (multiply == null) {
					multiply = OperatorService.createOperator(IOMultiplier.class);
					subprocess.addOperator(multiply, 0);
					// if the kafkaOutputPort was connected, connect the first outputPort of
					// multiply to the original target of the kafkaOutputPort
					if (kafkaOutputPort.isConnected()) {
						InputPort target = kafkaOutputPort.getDestination();
						multiply.getOutputPorts()
								.getPortByIndex(multiplyPortIndex)
								.connectTo(target);
						multiplyPortIndex++;
					}
					// Connect the kafkaOutputPort to the multiply input port
					kafkaOutputPort.connectTo(multiply.getInputPort());
					// fireUpdate to ensure that the port extender of the multiply creates a new
					// outputport
					fireUpdate(multiply);
				}
				multiplyPortIndex = addKafkaOperator(subprocess, multiply, true, connection,
													 multiplyPortIndex);
				fireUpdate(multiply);
				multiplyPortIndex = addKafkaOperator(subprocess, multiply, false, connection,
													 multiplyPortIndex);
				fireUpdate(multiply);
			} else {
				StreamingNest fromNest = (StreamingNest) subprocess.getOperatorByName(
						connection.getFromStreamingNestName());
				StreamingNest toNest = (StreamingNest) subprocess.getOperatorByName(
						connection.getToStreamingNestName());
				OutputPort fromPort = fromNest.getSubprocess(0)
											  .getOperatorByName(connection.getOriginalConnection()
																		   .getFromOperator())
											  .getOutputPorts()
											  .getPortByName(connection.getOriginalConnection()
																	   .getFromPort());
				InputPort toPort = toNest.getSubprocess(0)
										 .getOperatorByName(
												 connection.getOriginalConnection().getToOperator())
										 .getInputPorts()
										 .getPortByName(
												 connection.getOriginalConnection().getToPort());
				int numberOfPortPairsFrom = fromNest.getOutputPortPairExtender()
													.getManagedPairs()
													.size();
				int numberOfPortPairsTo = toNest.getInputPortPairExtender()
												.getManagedPairs()
												.size();
				//
				fromPort.connectTo(fromNest.getSubprocess(0)
										   .getInnerSinks()
										   .getPortByIndex(numberOfPortPairsFrom - 1));
				fromNest.getOutputPorts()
						.getPortByIndex(numberOfPortPairsFrom - 1)
						.connectTo(toNest.getInputPorts().getPortByIndex(numberOfPortPairsTo));
				toNest.getSubprocess(0)
					  .getInnerSources()
					  .getPortByIndex(numberOfPortPairsTo - 1)
					  .connectTo(toPort);
				fireUpdate(toNest);
				fireUpdate(fromNest);
			}
		}
		return subprocess;
	}
	
	private int addKafkaOperator(ExecutionUnit subprocess, IOMultiplier multiply, boolean kafkaSink,
			SplittedConnection connection, int multiplyPortIndex) throws OperatorCreationException {
		String nestName = null;
		Operator kafkaOperator = null;
		String targetOperatorName = null;
		String targetPortName = null;
		AWOperatorConnection originalConnection = connection.getOriginalConnection();
		String topicName = originalConnection.getFromOperator() + "." + originalConnection.getFromPort() + " to " + originalConnection
				.getToOperator() + "." + originalConnection.getToPort();
		if (kafkaSink) {
			// A StreamKafkaSink operator has to be placed in the fromStreamingNest. The
			// targetOperator (the fromOperator) has to be connected to the kafkaSink.
			nestName = connection.getFromStreamingNestName();
			kafkaOperator = OperatorService.createOperator(StreamKafkaSink.class);
			kafkaOperator.setParameter(StreamKafkaSink.PARAMETER_TOPIC, topicName);
			targetOperatorName = originalConnection.getFromOperator();
			targetPortName = originalConnection.getFromPort();
		} else {
			// A StreamKafkaSource operator has to be placed in the toStreaming. The kafkaSource has
			// to be connected to the targetOperator (the toOperator).
			nestName = connection.getToStreamingNestName();
			kafkaOperator = OperatorService.createOperator(StreamKafkaSource.class);
			kafkaOperator.setParameter(StreamKafkaSource.PARAMETER_TOPIC, topicName);
			targetOperatorName = originalConnection.getToOperator();
			targetPortName = originalConnection.getToPort();
		}
		// get the streaming nest operator from the subprocess
		StreamingNest nest = (StreamingNest) subprocess.getOperatorByName(nestName);
		// get the number of port pairs of the streaming nest
		int numberOfPortPairs = nest.getInputPortPairExtender().getManagedPairs().size();
		// Connect the next outputPort of the multiply to the last input port of the port
		// extender (numberOfPortPairs + 1 (for the 'connection' port of the nest) - 1
		// (cause we need an index))
		multiply.getOutputPorts()
				.getPortByIndex(multiplyPortIndex)
				.connectTo(nest.getInputPorts().getPortByIndex(numberOfPortPairs));
		multiplyPortIndex++;
		// Get the nest subprocess
		ExecutionUnit nestSubprocess = nest.getSubprocess(0);
		// Add the kafkaOperator to the nest subprocess
		nestSubprocess.addOperator(kafkaOperator);
		// connect the inner throughput port to the kafkaOperator connection port
		nestSubprocess.getInnerSources()
					  .getPortByIndex(numberOfPortPairs - 1)
					  .connectTo(kafkaOperator.getInputPorts().getPortByIndex(0));
		// Get the target operator
		Operator targetOperator = nestSubprocess.getOperatorByName(targetOperatorName);
		// Connect targetOperator and kafkaOperator
		if (kafkaSink) {
			targetOperator.getOutputPorts()
						  .getPortByName(targetPortName)
						  .connectTo(kafkaOperator.getInputPorts().getPortByIndex(1));
		} else {
			kafkaOperator.getOutputPorts()
						 .getPortByIndex(0)
						 .connectTo(targetOperator.getInputPorts().getPortByName(targetPortName));
		}
		fireUpdate(nest);
		return multiplyPortIndex;
	}
	
	private void createAndConnectStreamingBackends(AgnosticWorkflow agnosticWorkflow,
			ExecutionUnit subprocess,
			Map<String,List<ConnectionInformationContainerIOObject>> availableSites) {
		Map<String,InputPort> map = new LinkedHashMap<>();
		for (AWOperator operator : agnosticWorkflow.getOperators()) {
			if (operator.getOperatorClass().equals(StreamingNest.class)) {
				String platformName = operator.getPlatformName();
				Operator op = subprocess.getOperatorByName(operator.getName());
				map.put(platformName, op.getInputPorts().getPortByName("connection"));
			}
		}
		for (Entry<String,List<ConnectionInformationContainerIOObject>> entry : availableSites.entrySet()) {
			String siteName = entry.getKey();
			for (ConnectionInformationContainerIOObject connection : entry.getValue()) {
				
				ConnectionConfiguration connConfig = connection.getConnectionInformation()
															   .getConfiguration();
				String platformName = connConfig.getType().replace("streaming:", "");
				OutputPort port = subprocess.getInnerSources().createPort(connConfig.getName());
				port.connectTo(
						map.get(AgnosticWorkflowConversion.constructStreamingNestName(siteName,
																					  platformName)));
				innerConnectionPorts.put(port, connection);
			}
		}
	}
	
	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> params = super.getParameterTypes();
		
		params.add(new ParameterTypeLinkButton(PARAMETER_RESTORE_ORIGINAL_PROCESS,
											   "Restore the original process.", action));
		
		params.add(new ParameterTypeBoolean(PARAMETER_WRITE_AND_READ_FROM_JSON, "", false, true));
		
		ParameterType type = new ParameterTypeFile(PARAMETER_NETWORK_JSON, "", "json", false);
		type.registerDependencyCondition(
				new BooleanParameterCondition(this, PARAMETER_WRITE_AND_READ_FROM_JSON, true,
											  true));
		params.add(type);
		
		type = new ParameterTypeFile(PARAMETER_REQUEST_JSON, "", "json", false);
		type.registerDependencyCondition(
				new BooleanParameterCondition(this, PARAMETER_WRITE_AND_READ_FROM_JSON, true,
											  true));
		params.add(type);
		
		type = new ParameterTypeFile(PARAMETER_DICTIONARY_JSON, "", "json", false);
		type.registerDependencyCondition(
				new BooleanParameterCondition(this, PARAMETER_WRITE_AND_READ_FROM_JSON, true,
											  true));
		params.add(type);
		
		type = new ParameterTypeFile(PARAMETER_OUTPUT_WORKFLOW_JSON, "", "json", false);
		type.registerDependencyCondition(
				new BooleanParameterCondition(this, PARAMETER_WRITE_AND_READ_FROM_JSON, true,
											  true));
		params.add(type);
		
		type = new ParameterTypeFile(PARAMETER_OPTIMIZER_RESPONSE, "", "json", false);
		type.registerDependencyCondition(
				new BooleanParameterCondition(this, PARAMETER_WRITE_AND_READ_FROM_JSON, true,
											  true));
		params.add(type);
		
		type = new ParameterTypeFile(PARAMETER_INPUT_OPTIMIZER_RESPONSE_JSON, "", "json", false);
		type.registerDependencyCondition(
				new BooleanParameterCondition(this, PARAMETER_WRITE_AND_READ_FROM_JSON, true,
											  true));
		params.add(type);
		
		type = new ParameterTypeFile(PARAMETER_OUTPUT_OPTIMIZED_WORKFLOW_JSON, "", "json", false);
		type.registerDependencyCondition(
				new BooleanParameterCondition(this, PARAMETER_WRITE_AND_READ_FROM_JSON, true,
											  true));
		params.add(type);
		
		params.add(
				new ParameterTypeString(PARAMETER_HOST, "The host of the optimizer", false, false));
		
		params.add(
				new ParameterTypeString(PARAMETER_PORT, "The port of the optimizer", false, false));
		
		params.add(new ParameterTypeString(PARAMETER_USER,
										   "The user name used to log into the optimizer", false,
										   false));
		params.add(new ParameterTypeString(PARAMETER_PASSWORD,
										   "The password used to log into the optimizer", false,
										   false));
		
		params.add(new ParameterTypeCategory(PARAMETER_VERSION,
											 "The version of the optimizer to be used.",
											 Arrays.stream(OptimizerVersion.values())
												   .map(OptimizerVersion :: getDescription)
												   .toArray(String[] :: new), 0, false));
		
		params.add(new ParameterTypeCategory(PARAMETER_ALGORITHM,
											 "The algorithm of the optimizer to be used.",
											 Arrays.stream(OptimizerAlgorithm.values())
												   .map(OptimizerAlgorithm :: getDescription)
												   .toArray(String[] :: new), 3, false));
		
		params.add(new ParameterTypeString(PARAMETER_NETWORK_NAME,
										   "The unique name of the dictionary used by the optimizer.",
										   "network1", false));
		
		params.add(new ParameterTypeString(PARAMETER_DICTIONARY_NAME,
										   "The unique name of the dictionary used by the optimizer.",
										   "dictionary1", false));
		
		params.add(new ParameterTypeString(PARAMETER_WORKFLOW_NAME,
										   "The unique name of the workflow used by the optimizer.",
										   "Streaming", false));
		
		params.add(new ParameterTypeEnumeration(PARAMETER_STREAMING_SITES_NAMES,
												"names of the available streaming sites",
												new ParameterTypeString("site_name",
																		"name of the streaming site",
																		true)));
		
		params.add(new ParameterTypeBoolean(PARAMETER_DUMMY_OPTIMIZATION, "", false, true));
		
		return params;
	}
	
}
