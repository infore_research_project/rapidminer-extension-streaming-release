/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator;

import java.util.List;
import java.util.logging.Logger;

import com.rapidminer.extension.streaming.ioobject.StreamDataContainer;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.extension.streaming.utility.graph.transform.ConnectTransformer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.LogService;


/**
 * Connect operator for stream graphs
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamConnect extends Operator {

	private static final Logger LOGGER = LogService.getRoot();

	private static final String PARAMETER_CONTROL_KEY = "control_key";

	private static final String PARAMETER_DATA_KEY = "data_key";

	private final InputPort controlStream = getInputPorts().createPort("control stream", StreamDataContainer.class);

	private final InputPort dataStream = getInputPorts().createPort("data stream", StreamDataContainer.class);

	private final OutputPort output = getOutputPorts().createPort("output stream");

	public StreamConnect(OperatorDescription description) {
		super(description);

		getTransformer().addPassThroughRule(controlStream, output);
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();

		ParameterType leftKey = new ParameterTypeString(
			PARAMETER_CONTROL_KEY,
			"Key of the value to be used on the control-stream.",
			false);
		types.add(leftKey);

		ParameterType rightKey = new ParameterTypeString(
			PARAMETER_DATA_KEY,
			"Key of the value to be used on the data-stream.",
			false);
		types.add(rightKey);

		return types;
	}

	@Override
	public void doWork() throws OperatorException {
		StreamDataContainer controlStream = this.controlStream.getData(StreamDataContainer.class);
		StreamDataContainer dataStream = this.dataStream.getData(StreamDataContainer.class);

		StreamGraph graph = controlStream.getStreamGraph();
		LOGGER.fine("Processing CONNECT for: " + graph.getName());

		ConnectTransformer control = new ConnectTransformer.Builder(graph)
			.withControlKey(getParameterAsString(PARAMETER_CONTROL_KEY))
			.withDataKey(getParameterAsString(PARAMETER_DATA_KEY))
			.withControlParent(controlStream.getLastNode())
			.withDataParent(dataStream.getLastNode())
			.build();

		StreamDataContainer outData = new StreamDataContainer(graph, control);
		output.deliver(outData);
	}

}