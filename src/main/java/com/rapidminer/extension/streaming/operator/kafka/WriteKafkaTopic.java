/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.operator.kafka;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.NumericalAttribute;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ProcessStoppedException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.conditions.BooleanParameterCondition;
import com.rapidminer.parameter.conditions.EqualTypeCondition;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 *
 * Basic Operator that writes an example set to a registered kafka topic.
 * The message frequency can be defined, so either the data is written in a given interval or in bulk as quickly as possible.
 * The output format can also be defined as either simple String or a JSON representing the example
 *
 * @author David Arnu
 * @since 0.1.0
 *
 */
public class WriteKafkaTopic extends Operator {


    static final String PARAMETER_KAFKA_CLUSTER = "kafka_cluster";

    static final String PARAMETER_KAFKA_PORT = "kafka_port";

    static final String PARAMETER_TOPIC = "kafka_topic";

    static final String PARAMETER_BULK = "bulk_sending";

    static final String PARAMETER_MESSAGE_INTERVAL = "message_interval";

    static final String PARAMETER_MESSAGE_FORMAT = "message_format";

    //  static  final String PARAMETER_ID_AS_KEY = "id_as_key";

    static final String PARAMETER_ATTRIBUTE_SEPARATOR= "attribute_separator";


    private static final String[] FORMAT_TYPES = { "String", "JSON"};
    private static final int FORMAT_STRING = 0;
    private static final int FORMAT_JSON = 1;



    private final InputPort inputPort = getInputPorts().createPort("input");
    private final OutputPort outputPort = getOutputPorts().createPort("throughput");


    /**
     *
     * * @param description
     */
    public WriteKafkaTopic(OperatorDescription description) {
        super(description);

        getTransformer().addPassThroughRule(inputPort, outputPort);


    }

    @Override
    public void doWork() throws OperatorException {

        String kafka_cluster = getParameterAsString(PARAMETER_KAFKA_CLUSTER);
        int kafkaPort = getParameterAsInt(PARAMETER_KAFKA_PORT);

        //   boolean idAsKey = getParameterAsBoolean(PARAMETER_ID_AS_KEY);
        String kafkaTopic = getParameterAsString(PARAMETER_TOPIC);

        boolean sendAsBulk = getParameterAsBoolean(PARAMETER_BULK);
        int messageInterval = getParameterAsInt(PARAMETER_MESSAGE_INTERVAL);

        String stringSeparator = getParameterAsString(PARAMETER_ATTRIBUTE_SEPARATOR);
        String messageFormat = getParameterAsString(PARAMETER_MESSAGE_FORMAT);

        ExampleSet exampleSet = (ExampleSet) inputPort.getData(ExampleSet.class).copy();

        outputPort.deliver(exampleSet);


        Thread.currentThread().setContextClassLoader(null);


        Properties kafkaProps = new Properties();




        kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,kafka_cluster+":"+kafkaPort);
        kafkaProps.put(ProducerConfig.ACKS_CONFIG, "all");
        kafkaProps.put(ProducerConfig.RETRIES_CONFIG, 0);
        kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer< String, String > producer = new KafkaProducer < > (kafkaProps);


        try {

            for (Example exa : exampleSet) {

                String key = "";
//                if (idAsKey) {
//                    exampleSet.getAttributes().getId().getMapping().mapIndex((int) exa.getId());
//                    key = String.valueOf(exa.getId());
//                    System.out.println(key);
//                }

                StringBuilder message = new StringBuilder();
                Attributes attributes = exa.getAttributes();
                Iterator<Attribute> attributeIterator = attributes.allAttributes();

                while (attributeIterator.hasNext()) {

                    if (messageFormat.equals(FORMAT_TYPES[FORMAT_JSON])) {
                        Attribute att = attributeIterator.next();
                        String attributeName = "\"" + att.getName() + "\"";

                        message.append(attributeName ).append(":").append(exa.getValueAsString(att, NumericalAttribute.DEFAULT_NUMBER_OF_DIGITS, true)).append(",");


                    } else {
                        message.append(exa.getValueAsString(attributeIterator.next())).append(stringSeparator);
                    }
                }


                // remove the last trailing stringSeparator from the message
                message = new StringBuilder(message.substring(0, message.length() - stringSeparator.length()));

                if (messageFormat.equals(FORMAT_TYPES[FORMAT_JSON])) {
                    // add "{" and "}" around the JSON string
                    message = new StringBuilder("{" + message + "}");
                }

                ProducerRecord<String, String> data = new ProducerRecord<>(kafkaTopic, key, message.toString());

                producer.send(data);

                checkForStop();

                if (!sendAsBulk) {
                    try {
                        TimeUnit.MILLISECONDS.sleep(messageInterval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        } catch (ProcessStoppedException e){
            producer.flush();
            producer.close();
        }


        producer.flush();
        producer.close();


    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();

        ParameterType host = new ParameterTypeString(PARAMETER_KAFKA_CLUSTER, "address of kafka host", "localhost");
        types.add(host);

        ParameterType port = new ParameterTypeInt(PARAMETER_KAFKA_PORT, "port number of the kafka host", 0, 65_535, 9092);
        types.add(port);

        ParameterType topic = new ParameterTypeString(PARAMETER_TOPIC, "Kafka topic to write to.", false);
        types.add(topic);

        ParameterType separator = new ParameterTypeString(PARAMETER_ATTRIBUTE_SEPARATOR, "string separator when writing attributes as string message.", ";");
        separator.registerDependencyCondition(new EqualTypeCondition(this, PARAMETER_MESSAGE_FORMAT, FORMAT_TYPES, true, FORMAT_STRING));
        types.add(separator);

        //    ParameterType key_id = new ParameterTypeBoolean(PARAMETER_ID_AS_KEY, "id attribute as key value", true);
        //   types.add(key_id);

        ParameterType bulk = new ParameterTypeBoolean(PARAMETER_BULK, "sending message in one bulk", false);
        types.add(bulk);

        ParameterType interval = new ParameterTypeInt(PARAMETER_MESSAGE_INTERVAL, "interval between two messages in miliseconds", 1, Integer.MAX_VALUE, 1);
        interval.registerDependencyCondition(new BooleanParameterCondition(this, PARAMETER_BULK,true, false));
        types.add(interval);

        ParameterType format = new ParameterTypeCategory(PARAMETER_MESSAGE_FORMAT, "format of the send messages", FORMAT_TYPES, FORMAT_JSON);
        types.add(format);

        return types;
    }

}
