/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.connection;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rapidminer.connection.ConnectionHandler;
import com.rapidminer.connection.ConnectionInformation;
import com.rapidminer.connection.ConnectionInformationBuilder;
import com.rapidminer.connection.configuration.ConfigurationParameter;
import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.configuration.ConnectionConfigurationBuilder;
import com.rapidminer.connection.util.ParameterUtility;
import com.rapidminer.connection.util.TestExecutionContext;
import com.rapidminer.connection.util.TestResult;
import com.rapidminer.connection.util.ValidationResult;
import com.rapidminer.extension.streaming.PluginInitStreaming;

import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;


/**
 * Kafka connection handler
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public final class KafkaConnectionHandler implements ConnectionHandler {

	public static final String TYPE = PluginInitStreaming.STREAMING_NAMESPACE + ":" + "kafka";

	private static final KafkaConnectionHandler INSTANCE = new KafkaConnectionHandler();

	private static final String GROUP_CONNECTION_PROPS = "connection_properties";

	private static final String GROUP_CLUSTER_CONF = "cluster_config";

	private static final String PARAMETER_HOST_PORTS = "host_ports";

	private KafkaConnectionHandler() {
	}

	public static KafkaConnectionHandler getINSTANCE() {
		return INSTANCE;
	}

	@Override
	public ConnectionInformation createNewConnectionInformation(String name) {
		List<ConfigurationParameter> clusterConfig = Lists.newArrayList(
			ParameterUtility.getCPBuilder(PARAMETER_HOST_PORTS).build()
		);

		ConnectionConfiguration config = new ConnectionConfigurationBuilder(name, getType())
			.withDescription("This is an Apache Kafka (cluster) connection")
			.withKeys(GROUP_CLUSTER_CONF, clusterConfig)
			.build();

		return new ConnectionInformationBuilder(config).build();
	}

	@Override
	public void initialize() {
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public ValidationResult validate(ConnectionInformation object) {
		return ValidationResult.success(ValidationResult.I18N_KEY_SUCCESS);
	}

	@Override
	public TestResult test(TestExecutionContext<ConnectionInformation> testContext) {
		return TestResult.success(TestResult.I18N_KEY_SUCCESS);
	}

	public Properties buildClusterConfiguration(ConnectionConfiguration config) {
		Properties props = new Properties();
		props.setProperty(BOOTSTRAP_SERVERS_CONFIG, config.getValue(GROUP_CLUSTER_CONF + "." + PARAMETER_HOST_PORTS));

		// Add arbitrary properties to the result
		config.getKeyMap().entrySet()
			.stream()
			.filter(entry -> entry.getKey().startsWith(GROUP_CONNECTION_PROPS + "."))
			.map(Map.Entry::getValue)
			.forEach(entry -> props.put(entry.getName(), entry.getValue()));

		return props;
	}

	/**
	 * This method returns a LinkedHashMap representing the menu-groups for the UI ("ordered" map!).
	 * The key is the i18n key for the menu group name and the value marks if this group will be a container
	 * for an arbitrary number of properties.
	 *
	 * @return see above
	 */
	public LinkedHashMap<String, Boolean> menuGroups() {
		LinkedHashMap<String, Boolean> orderedMenu = Maps.newLinkedHashMap();
		orderedMenu.put(GROUP_CLUSTER_CONF, false);
		orderedMenu.put(GROUP_CONNECTION_PROPS, true);
		return orderedMenu;
	}

}