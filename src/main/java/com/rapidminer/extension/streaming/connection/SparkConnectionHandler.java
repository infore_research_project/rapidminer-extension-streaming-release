/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.connection;

import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.RM_CONF_HDFS_PATH;
import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.RM_CONF_HDFS_URI;
import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.RM_CONF_SPARK_HOST;
import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.RM_CONF_SPARK_PORT;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rapidminer.connection.ConnectionHandler;
import com.rapidminer.connection.ConnectionInformation;
import com.rapidminer.connection.ConnectionInformationBuilder;
import com.rapidminer.connection.configuration.ConfigurationParameter;
import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.configuration.ConnectionConfigurationBuilder;
import com.rapidminer.connection.util.ParameterUtility;
import com.rapidminer.connection.util.TestExecutionContext;
import com.rapidminer.connection.util.TestResult;
import com.rapidminer.connection.util.ValidationResult;
import com.rapidminer.extension.streaming.PluginInitStreaming;
import com.rapidminer.extension.streaming.deploy.StreamRunnerType;


/**
 * Spark connection handler
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public final class SparkConnectionHandler implements ConnectionHandler, StreamConnectionHandler {

	public static final String TYPE = PluginInitStreaming.STREAMING_NAMESPACE + ":" + "spark";

	private static final SparkConnectionHandler INSTANCE = new SparkConnectionHandler();

	private static final String GROUP_SPARK_CONNECTION_PROPS = "spark_connection_properties";

	private static final String GROUP_SPARK_CLUSTER_CONF = "spark_cluster_config";

	private static final String PARAMETER_SPARK_HOST = "spark_host";

	private static final String PARAMETER_SPARK_PORT = "spark_port";

	private static final String GROUP_HDFS_CONNECTION_PROPS = "hdfs_connection_properties";

	private static final String GROUP_HDFS_CLUSTER_CONF = "hdfs_cluster_config";

	private static final String PARAMETER_HDFS_URI = "hdfs_uri";

	private static final String PARAMETER_HDFS_PATH = "hdfs_path";

	private SparkConnectionHandler() {
	}

	public static SparkConnectionHandler getINSTANCE() {
		return INSTANCE;
	}

	@Override
	public ConnectionInformation createNewConnectionInformation(String name) {
		List<ConfigurationParameter> sparkClusterConfig = Lists.newArrayList(
			ParameterUtility.getCPBuilder(PARAMETER_SPARK_HOST).build(),
			ParameterUtility.getCPBuilder(PARAMETER_SPARK_PORT).build()
		);
		List<ConfigurationParameter> hdfsClusterConfig = Lists.newArrayList(
			ParameterUtility.getCPBuilder(PARAMETER_HDFS_URI).build(),
			ParameterUtility.getCPBuilder(PARAMETER_HDFS_PATH).build()
		);

		ConnectionConfiguration config = new ConnectionConfigurationBuilder(name, getType())
			.withDescription("This is an Apache Spark connection")
			.withKeys(GROUP_SPARK_CLUSTER_CONF, sparkClusterConfig)
			.withKeys(GROUP_HDFS_CLUSTER_CONF, hdfsClusterConfig)
			.build();

		return new ConnectionInformationBuilder(config).build();
	}

	@Override
	public void initialize() {
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public StreamRunnerType getRunnerType() {
		return StreamRunnerType.SPARK;
	}

	@Override
	public Properties buildClusterConfiguration(ConnectionConfiguration config) {
		Properties props = new Properties();
		props.setProperty(RM_CONF_SPARK_HOST, config.getValue(GROUP_SPARK_CLUSTER_CONF + "." + PARAMETER_SPARK_HOST));
		props.setProperty(RM_CONF_SPARK_PORT, config.getValue(GROUP_SPARK_CLUSTER_CONF + "." + PARAMETER_SPARK_PORT));
		props.setProperty(RM_CONF_HDFS_URI, config.getValue(GROUP_HDFS_CLUSTER_CONF + "." + PARAMETER_HDFS_URI));
		props.setProperty(RM_CONF_HDFS_PATH, config.getValue(GROUP_HDFS_CLUSTER_CONF + "." + PARAMETER_HDFS_PATH));

		// Add arbitrary properties for both Spark and HDFS to the result (with a special prefix)
		String[] propPrefixes = new String[]{GROUP_SPARK_CONNECTION_PROPS + ".", GROUP_HDFS_CONNECTION_PROPS + "."};
		config.getKeyMap().entrySet()
			.stream()
			.filter(entry -> StringUtils.startsWithAny(entry.getKey(), propPrefixes))
			.map(Map.Entry::getValue)
			.forEach(configParam -> props.put(configParam.getName(), configParam.getValue()));

		return props;
	}

	@Override
	public ValidationResult validate(ConnectionInformation object) {
		return ValidationResult.success(ValidationResult.I18N_KEY_SUCCESS);
	}

	@Override
	public TestResult test(TestExecutionContext<ConnectionInformation> testContext) {
		return TestResult.success(TestResult.I18N_KEY_SUCCESS);
	}

	/**
	 * This method returns a LinkedHashMap representing the menu-groups for the UI ("ordered" map!).
	 * The key is the i18n key for the menu group name and the value marks if this group will be a container
	 * for an arbitrary number of properties.
	 *
	 * @return see above
	 */
	public LinkedHashMap<String, Boolean> menuGroups() {
		LinkedHashMap<String, Boolean> orderedMenu = Maps.newLinkedHashMap();
		orderedMenu.put(GROUP_SPARK_CLUSTER_CONF, false);
		orderedMenu.put(GROUP_SPARK_CONNECTION_PROPS, true);
		orderedMenu.put(GROUP_HDFS_CLUSTER_CONF, false);
		orderedMenu.put(GROUP_HDFS_CONNECTION_PROPS, true);
		return orderedMenu;
	}

}