/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.connection;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rapidminer.connection.ConnectionHandler;
import com.rapidminer.connection.ConnectionInformation;
import com.rapidminer.connection.ConnectionInformationBuilder;
import com.rapidminer.connection.configuration.ConfigurationParameter;
import com.rapidminer.connection.configuration.ConnectionConfiguration;
import com.rapidminer.connection.configuration.ConnectionConfigurationBuilder;
import com.rapidminer.connection.util.ParameterUtility;
import com.rapidminer.connection.util.TestExecutionContext;
import com.rapidminer.connection.util.TestResult;
import com.rapidminer.connection.util.ValidationResult;
import com.rapidminer.extension.streaming.PluginInitStreaming;
import com.rapidminer.extension.streaming.deploy.StreamRunnerType;

import static com.rapidminer.extension.streaming.deploy.flink.FlinkConstants.*;


/**
 * Flink connection handler
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public final class FlinkConnectionHandler implements ConnectionHandler, StreamConnectionHandler {

	public static final String TYPE = PluginInitStreaming.STREAMING_NAMESPACE + ":" + "flink";

	private static final FlinkConnectionHandler INSTANCE = new FlinkConnectionHandler();

	private static final String GROUP_CONNECTION_PROPS = "connection_properties";

	private static final String GROUP_CLUSTER_CONF = "cluster_config";

	private static final String PARAMETER_HOST = "host";

	private static final String PARAMETER_PORT = "port";

	private static final String PARAMETER_PARALLELISM = "parallelism";

	private FlinkConnectionHandler() {
	}

	public static FlinkConnectionHandler getINSTANCE() {
		return INSTANCE;
	}

	@Override
	public ConnectionInformation createNewConnectionInformation(String name) {
		List<ConfigurationParameter> clusterConfig = Lists.newArrayList(
			ParameterUtility.getCPBuilder(PARAMETER_HOST).build(),
			ParameterUtility.getCPBuilder(PARAMETER_PORT).build(),
			ParameterUtility.getCPBuilder(PARAMETER_PARALLELISM).build()
		);

		ConnectionConfiguration config = new ConnectionConfigurationBuilder(name, getType())
			.withDescription("This is an Apache Flink connection")
			.withKeys(GROUP_CLUSTER_CONF, clusterConfig)
			.build();

		return new ConnectionInformationBuilder(config).build();
	}

	@Override
	public void initialize() {
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public StreamRunnerType getRunnerType() {
		return StreamRunnerType.FLINK;
	}

	@Override
	public Properties buildClusterConfiguration(ConnectionConfiguration config) {
		Properties props = new Properties();
		props.setProperty(RM_CONF_FLINK_CLUSTER_HOST, config.getValue(GROUP_CLUSTER_CONF + "." + PARAMETER_HOST));
		props.setProperty(RM_CONF_FLINK_CLUSTER_PORT, config.getValue(GROUP_CLUSTER_CONF + "." + PARAMETER_PORT));
		props.setProperty(RM_CONF_FLINK_PARALLELISM, config.getValue(GROUP_CLUSTER_CONF + "." + PARAMETER_PARALLELISM));

		// TODO: we may need properties later, if not, just remove this section at some point (check menuGroups() as well)
		// Add arbitrary properties to the result
		/*config.getKeyMap().entrySet()
			.stream()
			.filter(entry -> entry.getKey().startsWith(GROUP_CONNECTION_PROPS + "."))
			.map(Map.Entry::getValue)
			.forEach(entry -> props.put(entry.getName(), entry.getValue()));*/

		return props;
	}

	@Override
	public ValidationResult validate(ConnectionInformation object) {
		return ValidationResult.success(ValidationResult.I18N_KEY_SUCCESS);
	}

	@Override
	public TestResult test(TestExecutionContext<ConnectionInformation> testContext) {
		return TestResult.success(TestResult.I18N_KEY_SUCCESS);
	}

	/**
	 * This method returns a LinkedHashMap representing the menu-groups for the UI ("ordered" map!).
	 * The key is the i18n key for the menu group name and the value marks if this group will be a container
	 * for an arbitrary number of properties.
	 *
	 * @return see above
	 */
	public LinkedHashMap<String, Boolean> menuGroups() {
		LinkedHashMap<String, Boolean> orderedMenu = Maps.newLinkedHashMap();
		orderedMenu.put(GROUP_CLUSTER_CONF, false);
		// TODO: we may need properties later, if not, just remove this section
		//orderedMenu.put(GROUP_CONNECTION_PROPS, true);
		return orderedMenu;
	}

}