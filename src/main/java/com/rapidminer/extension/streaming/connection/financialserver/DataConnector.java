/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.connection.financialserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.NoRouteToHostException;
import java.net.Socket;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.rapidminer.tools.LogService;


/**
 * Class template to handle TCP/IP data connection
 * <p>
 * Note: need to adjust access modifiers.
 *
 * @author Stefan Burkard, Holger Arndt, Edwin Yaqub (made some modifications)
 * @since 0.1.0
 */
public class DataConnector extends Thread {

	private String serverIP = null;
	private int serverPort = 9003;// hard coded for now to this default

	private volatile boolean running = false;
	private Socket tcpSocket = null;
	private DataOutputStream out = null;
	private DataInputStream in = null;
	public static final int OK = 0;
	public static final int NO_INTERNET = 1;
	public static final int CONNECTION_ERROR = 2;
	private String userName = "";
	private final QuoteInterface quote;
	//private Operator operator = null;
	public boolean sending = false;
	private static final Logger LOGGER = LogService.getRoot();

	/*
	public DataConnector(QuoteInterface quote, Operator operator) {
		this.quote = quote;
		this.operator = operator;
	}
	 */

	public DataConnector(QuoteInterface quote, String serverIP, int serverPort) {
		this.quote = quote;
		this.serverIP = serverIP;
		this.serverPort = serverPort;
	}

	/**
	 * connect to server using socket connection
	 *
	 * @return Returns connection status, one of DataConnector.OK, DataConnector.NO_INTERNET,
	 * DataConnector.CONNECTION_ERROR
	 */
	public int connect() {
		try {
			tcpSocket = new Socket(serverIP, serverPort);
			out = new DataOutputStream(new BufferedOutputStream(
				tcpSocket.getOutputStream()));
			in = new DataInputStream(new BufferedInputStream(
				tcpSocket.getInputStream()));

			running = true;
			start();
			return OK;
		} catch (NoRouteToHostException e) {
			System.out.println(e);
			return NO_INTERNET;
		} catch (IOException e) {
			System.out.println(e);
			return CONNECTION_ERROR;

		}

	}

	/**
	 * Run method of the extended Thread class, to read server response stream
	 */
	@Override
	public void run() {

		while (running) {
			if (!sending && tcpSocket != null && in != null) {

				try {
					StringBuilder response = new StringBuilder();
					int c;
					while (in != null && (c = in.read()) != -1) {
						response.append((char) c);
						if ((char) c == '!') {// All messages are separated by
							// '!'
							break;
						}

					}

					resolveResponse(response.toString());
				} catch (Exception ex) {
					System.err
						.println("Server read error : " + ex.getMessage());
				}
			}

		}
		System.out.println("< Server response reader stopped >");

	}

	/*
	 * Login to server with username and password
	 */
	public void login(String username, String pw) throws IOException {
		sendData(username + ",login," + pw + "!");
		this.userName = username;
		// DataOutputController.init();
	}

	/**
	 * Request server to send symbols list
	 *
	 * @throws IOException
	 */
	public void getSymbols() throws IOException {
		sendData(userName + ",quotelist!");
	}

	/**
	 * Request server to start quotes for symbol
	 *
	 * @param Object symbol - The Object symbol to send to server
	 * @throws IOException
	 */
	public void startQuote(Object symbol) throws IOException {
		String request = userName + ",orderquote,";
		sendData(request, false);
		sendSymbol(symbol + "");
		System.out.println("sent data : " + request + symbol + "!");
		LOGGER.log(Level.INFO, "CLIENT: sent startQuote data : " + request + symbol + "!");
	}

	/**
	 * Request server to to stop quotes for symbol
	 *
	 * @param Object symbol - The Object symbol to send to server
	 * @throws IOException
	 */
	public void stopQuote(Object symbol) throws IOException {
		String request = userName + ",cancelquote,";
		sendData(request, false);
		sendSymbol(symbol + "");
		System.out.println("sent data : " + request + symbol + "!");
	}

	/**
	 * Request server to to start depth for symbol
	 *
	 * @param Object symbol - The Object symbol to send to server
	 * @throws IOException
	 */
	public void startDepth(Object symbol) throws IOException {
		String request = userName + ",orderdepth,";
		sendData(request, false);
		sendSymbol(symbol + "");
		LOGGER.log(Level.INFO, "sent data : " + request + symbol + "!");
	}

	/**
	 * Request server to to stop depth for symbol
	 *
	 * @param Object symbol - The Object symbol to send to server
	 * @throws IOException
	 */
	public void stopDepth(Object symbol) throws IOException {
		String request = userName + ",canceldepth,";
		sendData(request, false);
		sendSymbol(symbol + "");
		System.out.println("sent data : " + request + symbol + "!");
	}

	/**
	 * private method to only send symbol data to server (this method invoke from an other method inside this class)
	 *
	 * @param String symbol - The String symbol to send to server
	 * @throws IOException
	 */
	private void sendSymbol(String symbol) throws IOException {

		if (out != null && symbol != null && !symbol.trim().isEmpty()) {

			symbol = symbol.trim();
			LOGGER.log(Level.INFO, "--- symbol string = " + symbol);
			//String ar[] = symbol.split(Character.toString((char)183));
			String[] ar = symbol.split("\\|");
			LOGGER.log(Level.INFO, "--- symbol string (After split on 183 = " + ar.length + "ar[0]" + ar[0]);
			out.writeBytes(ar[0]);
			out.write(183);
			out.writeBytes(ar[1]);
			out.write(183);
			out.writeBytes(ar[2]);
			out.writeBytes("!");
			out.flush();

		}
	}

	/**
	 * Send data to server
	 *
	 * @param String line - The String line to send to server
	 * @throws IOException
	 */
	public void sendData(String line) throws IOException {
		sendData(line, true);
	}

	/**
	 * Send data to server and prints sent data in the standard output (command line)
	 *
	 * @param line  - The String line to send to server
	 * @param print - Whether it should print in the command line, or not
	 * @throws IOException
	 */
	public void sendData(String line, boolean print) throws IOException {
		// All messages are separated by '!'

		if (out != null) {
			out.writeBytes(line);
			out.flush();
			LOGGER.log(Level.INFO, "CLIENT: Sending Data to Server: " + line);
			if (print) {
				System.out.println("sent data : " + line);
			}
		}
	}

	/**
	 * Decide the actions for the received response from the server
	 *
	 * @param String response- The response line received from server
	 */
	private void resolveResponse(String response) {
		if (response == null) {
			return;
		}

		if (response.replace("!", "").trim().isEmpty()) {
			return;
		}
		response = response.trim();
		if (response.toUpperCase().startsWith("LOGIN,")) {
			// Pass on the response to APIWrapper
			String responseSoFar = this.quote.getLoginCommandResponse();
			this.quote.setLoginCommandResponse(responseSoFar + "\n" + response);
		} else if (response.toUpperCase().startsWith("ORDERQUOTE,")) {
			// this.quote.log("SERVER:" + response);
			LOGGER.log(Level.INFO, "--- Received a quote from Server = " + response);
			this.quote.addQuote(response);

			// /counter++;
		} else if (response.toUpperCase().startsWith("QUOTELIST,")) {

			response = response.substring(10);
			String[] lines = response.split("\n");
			Vector list = new Vector();
			for (int i = 0; i < lines.length; i++) {
				if (lines[i] != null) {
					String line = lines[i];
					String[] data = line.split("\\|");
					if (data.length > 3) {
						//list.add(data[0] + "[^\\u{183}" + data[1] + "[^\\u{183}" + data[3]);
						list.add(data[0] + "|" + data[1] + "|" + data[3]);
					}
				}
			}
			// implemented this in APIWrapper:
			this.quote.setQuotesList(list, false);

			// Set response in Operator
			// String responseSoFar = ((InvokeCommand) this.operator).getSymbolsListCommandResponse();
			// ((InvokeCommand) this.operator).setSymbolsListCommandResponse(responseSoFar + "\n" + list.toString());

		} else if (response.toUpperCase().startsWith("HEARTBEAT,")) {
			try {
				sendData(userName + ",heartbeatanswer,!");
			} catch (IOException ex) {
				System.err.println(ex.getMessage());
			}
		} else if (response.toUpperCase().startsWith("QUOTE,")) {
			String[] ar = response.split(",");
			if (ar.length > 7) {
				String line = ar[6] + "," + ar[7] + "," + ar[4] + "," + ar[5];
				quote.addQuote(ar[1] + " " + line);
				/*
				if (quote.shouldSaveQuote()) {// Save quote data to file
					String fileName = ar[1];
					try {
						DataOutputController.saveQuoteData(fileName, line);
					} catch (Exception ex) {
						quote.log("Error Writing to File, " + fileName + " : "
								+ ex.getMessage());
					}
				}
				 */
			}

		} else if (response.toUpperCase().startsWith("DEPTH,")) {

			String line = response.endsWith("!") ? response.substring(0,
				response.length() - 1) : response;
			quote.addDepth(line);

		} else if (response.toUpperCase().startsWith("ERROR,")) {
			quote.log(response);
		} else {
			System.out.println("got data  : " + response);
			quote.log("SERVER:" + response);
		}

	}

	/**
	 * Stop the server TCP IP connection
	 */
	public void stopRunning() {
		running = false;
		try {
			stop();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.log(Level.WARNING, e.getMessage());
		}
		try {
			in.close();
			in = null;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.log(Level.WARNING, e.getMessage());
		}
		try {
			out.close();
			out = null;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.log(Level.WARNING, e.getMessage());
		}
		try {
			tcpSocket.shutdownInput();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.log(Level.WARNING, e.getMessage());
		}
		try {
			tcpSocket.shutdownOutput();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.log(Level.WARNING, e.getMessage());
		}
		try {
			tcpSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.log(Level.WARNING, e.getMessage());
		}
	}

	public boolean isThreadRunning() {
		return running;
	}

	/**
	 * @return
	 */
	public int getDefaultServerPort() {
		return serverPort;
	}

}