/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.deploy.flink;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.apache.flink.runtime.webmonitor.handlers.JarRunRequestBody;
import org.apache.flink.runtime.webmonitor.handlers.JarRunResponseBody;
import org.apache.flink.runtime.webmonitor.handlers.JarUploadResponseBody;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import com.rapidminer.extension.streaming.deploy.RestClient;
import com.rapidminer.tools.LogService;


/**
 * REST client for communicating with a Flink cluster (JAR upload, job submission, etc.)
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class FlinkRestClient {

	private static final Logger LOGGER = LogService.getRoot();

	private static final ObjectMapper MAPPER = new ObjectMapper();

	private static final ContentType JAR_CONTENT_TYPE = ContentType.create("application/x-java-archive");

	private static final String URL_UPLOAD = "jars/upload";

	private static final String URL_RUN_TEMPLATE = "jars/%s/run";

	private final RestClient restClient;

	/**
	 * Constructor for creating a Flink cluster client for the given host and port
	 *
	 * @param host
	 * @param port
	 */
	public FlinkRestClient(String host, String port) {
		this.restClient = new RestClient(host, port);
	}

	/**
	 * Uploads a JAR to the Flink cluster
	 *
	 * @param jarPath
	 * @return
	 * @throws IOException
	 */
	public String uploadJar(String jarPath) throws IOException {
		LOGGER.fine("Uploading JAR: " + jarPath);

		// Build multi-part body with file
		File jarFile = new File(jarPath);
		HttpEntity entity = MultipartEntityBuilder
			.create()
			.addBinaryBody("jarFile", jarFile, JAR_CONTENT_TYPE, jarFile.getName())
			.build();

		// Send and process response (get JarId)
		HttpEntity response = restClient.post(URL_UPLOAD, entity, "Unsuccessful job upload");
		String jarId = MAPPER
			.readValue(response.getContent(), JarUploadResponseBody.class)
			.getFilename();

		return new File(jarId).getName();
	}

	/**
	 * Submits a job with the given parameters
	 *
	 * @param jarId
	 * @param args        list of arguments, e.g.: <b>(--parallelism,8,--clusterAddr,localhost:9092)</b>
	 * @param parallelism
	 * @param mainClass
	 * @return jobId for the submitted job returned by Flink
	 * @throws IOException
	 */
	public String submitJob(String jarId, List<String> args, int parallelism, String mainClass) throws IOException {
		LOGGER.fine("Submitting job for JAR: " + jarId);
		StringEntity requestEntity;
		JarRunRequestBody requestBody = new JarRunRequestBody(mainClass, null, args, parallelism, null, null, null);

		// Serializing request into JSON
		try {
			String requestStr = MAPPER.writerFor(JarRunRequestBody.class).writeValueAsString(requestBody);
			requestEntity = new StringEntity(requestStr);
		} catch (JsonProcessingException e) {
			LOGGER.severe("Could not submit job for JAR: " + jarId + ", error: " + e.getMessage());
			throw new IOException(e);
		}

		// Send and process response
		String url = String.format(URL_RUN_TEMPLATE, jarId);
		HttpEntity response = restClient.post(url, requestEntity, "Unsuccessful job submission");

		return MAPPER
			.readValue(response.getContent(), JarRunResponseBody.class)
			.getJobId()
			.toString();
	}

	/**
	 * Uploads a JAR to the Flink cluster and initiates (submits) the job contained by that JAR
	 *
	 * @param jarPath     file system path to the fat-JAR
	 * @param args        list of arguments, e.g.: <b>(--parallelism,8,--clusterAddr,localhost:9092)</b>
	 * @param parallelism
	 * @param mainClass
	 * @return jobId
	 * @throws IOException
	 */
	public String uploadAndSubmit(String jarPath, List<String> args, int parallelism, String mainClass) throws IOException {
		LOGGER.fine("Uploading & Submitting job for JAR: " + jarPath);
		// Upload JAR
		String jarId = uploadJar(jarPath);

		// Submit job
		return submitJob(jarId, args, parallelism, mainClass);
	}

}