/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.deploy;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.rapidminer.extension.streaming.utility.KryoHandler;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;


/**
 * Utility class for holding functionality useful to {@link StreamRunner}-s
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamRunnerUtil {

	public static final String JAR_RESOURCE_PREFIX = "jar:";

	public static final String JAR_EXT = ".jar";

	public static final String STREAM_GRAPH_EXT = ".graph";

	/**
	 * Embeds files into a copy of the original JAR.
	 * Since the structure is flattened, it is very important that the file-names differ!
	 *
	 * @param origJar       original fat-JAR to embed the files into
	 * @param tempJarPrefix prefix for the new fat-JAR (temporary file)
	 * @param files         to be embedded
	 * @return path to new fat-JAR (a copy of the original) that contains the embedded files
	 * @throws IOException
	 */
	public static Path embedFiles(File origJar, String tempJarPrefix, List<File> files) throws IOException {
		Map<String, String> env = Collections.singletonMap("create", "true");

		// Create temporary jar file
		Path copyPath = Files.createTempFile(tempJarPrefix, JAR_EXT);
		URI copyURI = URI.create(JAR_RESOURCE_PREFIX + copyPath.toUri());

		// Copy original jar into temporary
		Files.copy(origJar.toPath(), copyPath, StandardCopyOption.REPLACE_EXISTING);

		// Open temporary jar with FileSystem and place every file, one after another into it
		try (FileSystem jarFs = FileSystems.newFileSystem(copyURI, env)) {
			for (File file : files) {
				Path filePath = file.toPath();
				Path pathInJarfile = jarFs.getPath(file.getName());
				Files.copy(filePath, pathInJarfile, StandardCopyOption.REPLACE_EXISTING);
			}
		}

		return copyPath;
	}

	/**
	 * Serializes graph using Kryo
	 *
	 * @param tempFilePrefix prefix for the temporary file (will hold binary serialized graph)
	 * @param graph          to be serialized
	 * @return path to the file that contains the serialized graph
	 * @throws IOException
	 */
	public static Path serializeGraph(String tempFilePrefix, StreamGraph graph) throws IOException {
		Path graphPath = createTempFile(tempFilePrefix, STREAM_GRAPH_EXT);

		// Serialize object to file
		new KryoHandler().write(graphPath.toFile(), graph);

		return graphPath.toAbsolutePath();
	}

	/**
	 * Creates a temporary file and registers it for cleanup at shutdown by RapidMiner.
	 *
	 * @param prefix
	 * @param suffix
	 * @return path to newly created temporary file
	 * @throws IOException
	 */
	public static Path createTempFile(String prefix, String suffix) throws IOException {
		File tempFile = File.createTempFile(prefix, suffix);
		tempFile.deleteOnExit();
		return tempFile.toPath();
	}

}