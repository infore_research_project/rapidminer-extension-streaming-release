/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.deploy;


/**
 * Supported backend/platform types for streaming workflow execution
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public enum StreamRunnerType {

	FLINK, SPARK

}