/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.deploy;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;


/**
 * Client for REST API calls (support: HTTP)
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class RestClient {

	private static final String URL_TEMPLATE = "http://%s:%s/%s";

	private final String host;

	private final String port;

	private final HttpClient httpClient = HttpClients.createDefault();

	/**
	 * Constructs the client for the given endpoint (host:port)
	 *
	 * @param host
	 * @param port
	 */
	public RestClient(String host, String port) {
		this.host = host;
		this.port = port;
	}

	/**
	 * @param url         API URL specification (e.g.: "/api/jobs/123")
	 * @param body        string content of the HTTP body section
	 * @param contentType header type of the body
	 * @param err         error message to use if the communication was not successful (not 2xx)
	 * @return response if the communication was successful
	 * @throws IOException
	 */
	public HttpEntity post(String url, String body, ContentType contentType, String err) throws IOException {
		return post(url, new StringEntity(body, contentType), err);
	}

	/**
	 * Sends a HTTP POST request with the given URL to the endpoint using the body in the request
	 *
	 * @param url  API URL specification (e.g.: "/api/jobs/123")
	 * @param body
	 * @param err  error message to use if the communication was not successful (not 2xx)
	 * @return response if the communication was successful
	 * @throws IOException
	 */
	public HttpEntity post(String url, HttpEntity body, String err) throws IOException {
		// Build request
		HttpUriRequest request = RequestBuilder
			.post(String.format(URL_TEMPLATE, host, port, StringUtils.stripStart(url, "/")))
			.setEntity(body)
			.build();

		// Send request
		return sendRequest(request, err);
	}

	/**
	 * Sends a HTTP DELETE request with the given URL to the endpoint
	 *
	 * @param url API URL specification (e.g.: "/api/jobs/123")
	 * @param err error message to use if the communication was not successful (not 2xx)
	 * @throws IOException
	 */
	public void delete(String url, String err) throws IOException {
		// Build request
		HttpUriRequest request = RequestBuilder
			.delete(String.format(URL_TEMPLATE, host, port, url))
			.build();

		// Send request
		sendRequest(request, err);
	}

	/**
	 * Takes the response body and returns a UTF-8 string representation of it
	 *
	 * @param response
	 * @return see above
	 * @throws IOException
	 */
	public String getResponseBody(HttpEntity response) throws IOException {
		StringWriter writer = new StringWriter();
		IOUtils.copy(response.getContent(), writer, StandardCharsets.UTF_8);
		return writer.toString();
	}

	/**
	 * Decides if the HTTP code should be considered successful or not
	 *
	 * @param httpCode
	 * @return true if the http-code is 2XX, false otherwise
	 */
	private boolean isSuccessful(int httpCode) {
		return (httpCode / 100) == 2;
	}

	/**
	 * "Executes" the request, i.e. actual HTTP communication takes place
	 *
	 * @param request executable object, represents request
	 * @param err     error message to use if the communication was not successful (not 2xx)
	 * @return response object
	 * @throws IOException
	 */
	private HttpEntity sendRequest(HttpUriRequest request, String err) throws IOException {
		// Send request
		HttpResponse response = httpClient.execute(request);

		// Get response details and handle response
		StatusLine statusLine = response.getStatusLine();

		if (isSuccessful(statusLine.getStatusCode())) {
			return response.getEntity();
		} else {
			throw new IOException(err + " --> " + statusLine.getReasonPhrase());
		}
	}

}