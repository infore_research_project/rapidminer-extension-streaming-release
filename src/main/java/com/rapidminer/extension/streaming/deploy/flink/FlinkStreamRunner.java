/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.deploy.flink;

import static com.rapidminer.extension.streaming.deploy.flink.FlinkConstants.RM_CONF_FLINK_CLUSTER_HOST;
import static com.rapidminer.extension.streaming.deploy.flink.FlinkConstants.RM_CONF_FLINK_CLUSTER_PORT;
import static com.rapidminer.extension.streaming.deploy.flink.FlinkConstants.RM_CONF_FLINK_PARALLELISM;
import static java.util.Collections.singletonList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.rapidminer.extension.streaming.PluginInitStreaming;
import com.rapidminer.extension.streaming.deploy.StreamRunner;
import com.rapidminer.extension.streaming.deploy.StreamRunnerException;
import com.rapidminer.extension.streaming.deploy.StreamRunnerUtil;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.tools.LogService;


/**
 * Flink implementation of the interface that is capable of initiating jobs remotely on a cluster
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class FlinkStreamRunner implements StreamRunner {

	private static final Logger LOGGER = LogService.getRoot();

	private final int parallelism;

	private final Path jarFile;

	private final FlinkRestClient restClient;

	public FlinkStreamRunner(Properties config) {
		jarFile = PluginInitStreaming.getFlinkJar();
		parallelism = Integer.valueOf(config.getProperty(RM_CONF_FLINK_PARALLELISM));

		String host = config.getProperty(RM_CONF_FLINK_CLUSTER_HOST);
		String port = config.getProperty(RM_CONF_FLINK_CLUSTER_PORT);
		restClient = new FlinkRestClient(host, port);
	}

	@Override
	public void execute(StreamGraph graph) throws StreamRunnerException {
		String graphName = graph.getName();
		LOGGER.fine("Starting execution on Flink: " + graphName);

		try {
			// Kryo serialize graph
			LOGGER.fine("Serializing graph: " + graph.getName());
			String graphPrefix = "rapidminer_rmx_streaming_flink_graph_";
			File graphFile = StreamRunnerUtil.serializeGraph(graphPrefix, graph).toFile();

			// Put serialized graph into the new JAR
			LOGGER.fine("Embedding graph file: " + graphFile.getAbsolutePath());
			String jarPrefix = "rapidminer_rmx_streaming_flink_job_";
			Path newJarPath = StreamRunnerUtil.embedFiles(jarFile.toFile(), jarPrefix, singletonList(graphFile));

			// Upload JAR
			String jarId = restClient.uploadJar(newJarPath.toString());

			// Start job
			List<String> args = singletonList(graphFile.getName());
			String jobId = restClient.submitJob(jarId, args, parallelism, FlinkConstants.FLINK_MAIN_CLASS);
			LOGGER.fine("Started Flink job: " + jobId);
		} catch (IOException e) {
			LOGGER.severe("Could not initiate Flink job: " + e.getMessage());
			throw new StreamRunnerException(String.format("Could not submit job: '%s'", graphName), e);
		}
	}

}