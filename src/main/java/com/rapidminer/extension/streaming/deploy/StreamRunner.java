/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.deploy;

import com.rapidminer.extension.streaming.utility.graph.StreamGraph;


/**
 * Functionality for executing StreamGraph-s (platform specific implementations differ greatly)
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public interface StreamRunner {

	/**
	 * Executes/initiates graph execution for the workflow defined in the StreamGraph
	 *
	 * @param graph workflow
	 * @throws StreamRunnerException
	 */
	void execute(StreamGraph graph) throws StreamRunnerException;

}