/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.deploy.spark;

import java.util.Set;

import com.google.common.collect.Sets;


/**
 * Spark specific constants on RapidMiner Studio side
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public final class SparkConstants {

	public static final String HDFS_FOLDER_JARS = "jars/";

	public static final String RM_CONF_HDFS_URI = "HDFS.URI";

	public static final String RM_CONF_HDFS_PATH = "HDFS.PATH";

	public static final String RM_CONF_SPARK_HOST = "SPARK.HOST";

	public static final String RM_CONF_SPARK_PORT = "SPARK.PORT";

	public static final String SPARK_MAIN_CLASS = "com.rapidminer.extension.streaming.spark.StreamGraphProcessor";

	public static final String SPARK_CONFIG_LINE_PATTERN = "%s\t%s\n";

	public static final String LIVY_SUBMIT_URL = "/batches";

	public static final Set<String> LIVY_BLACKLISTED_PROPERTIES = Sets.newHashSet("spark.master");

	private SparkConstants() {
	}

}