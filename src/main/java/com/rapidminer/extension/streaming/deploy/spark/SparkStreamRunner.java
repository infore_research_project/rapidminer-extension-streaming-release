/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.deploy.spark;

import static com.google.common.collect.Lists.newArrayList;
import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.HDFS_FOLDER_JARS;
import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.RM_CONF_HDFS_PATH;
import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.RM_CONF_HDFS_URI;
import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.RM_CONF_SPARK_HOST;
import static com.rapidminer.extension.streaming.deploy.spark.SparkConstants.RM_CONF_SPARK_PORT;
import static com.rapidminer.extension.streaming.utility.HdfsHandler.HDFS_PATH_SEPARATOR;
import static com.rapidminer.extension.streaming.utility.JsonUtil.toJson;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.joinWith;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;

import com.rapidminer.extension.streaming.PluginInitStreaming;
import com.rapidminer.extension.streaming.deploy.RestClient;
import com.rapidminer.extension.streaming.deploy.StreamRunner;
import com.rapidminer.extension.streaming.deploy.StreamRunnerException;
import com.rapidminer.extension.streaming.deploy.StreamRunnerUtil;
import com.rapidminer.extension.streaming.utility.HdfsHandler;
import com.rapidminer.extension.streaming.utility.api.livy.SubmitRequest;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;
import com.rapidminer.tools.LogService;


/**
 * Spark implementation of the interface that is capable of initiating jobs remotely on a cluster
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class SparkStreamRunner implements StreamRunner {

	private static final Logger LOGGER = LogService.getRoot();

	private final Properties config;

	private final Path jarFile;

	private final RestClient restClient;

	public SparkStreamRunner(Properties config) {
		this.config = buildMergedConfig(config);
		this.jarFile = PluginInitStreaming.getSparkJar();

		String host = config.getProperty(RM_CONF_SPARK_HOST);
		String port = config.getProperty(RM_CONF_SPARK_PORT);
		this.restClient = new RestClient(host, port);
	}

	@Override
	public void execute(StreamGraph graph) throws StreamRunnerException {
		LOGGER.fine("Starting execution procedure for: " + graph.getName());

		try {
			// Kryo serialises graph into a temporary file
			LOGGER.fine("Serializing graph: " + graph.getName());
			String graphPrefix = "rapidminer_rmx_streaming_spark_graph_";
			File graphFile = StreamRunnerUtil.serializeGraph(graphPrefix, graph).toFile();

			// Put serialized graph + configuration into the temporary JAR(-copy)
			File sparkConfig = saveConfig(config);
			LOGGER.fine("Embedding (graph, config): " + graphFile.getAbsolutePath() + ", " + sparkConfig.getAbsolutePath());
			String jarPrefix = "rapidminer_rmx_streaming_spark_job_";
			List<File> filesToEmbed = newArrayList(graphFile, sparkConfig);
			Path newJarPath = StreamRunnerUtil.embedFiles(jarFile.toFile(), jarPrefix, filesToEmbed);

			// Copy fat-JAR to HDFS destination
			HdfsHandler hdfsHandler = buildHdfsHandler();
			String hdfsJarDst = hdfsHandler.copyLocalFile(
				newJarPath.toFile(),
				joinWith(HDFS_PATH_SEPARATOR, config.getProperty(RM_CONF_HDFS_PATH, HDFS_PATH_SEPARATOR), HDFS_FOLDER_JARS));

			// Submit via REST (Apache-Livy on cluster side)
			List<String> args = filesToEmbed
				.stream()
				.map(File::getName)
				.collect(Collectors.toList());

			submitViaREST(
				hdfsJarDst,
				graph.getName() + "-" + RandomStringUtils.randomAlphanumeric(10),
				args);
		} catch (URISyntaxException e) {
			LOGGER.severe("Invalid URI: " + e.getMessage());
			throw new StreamRunnerException("Invalid URI -> unsuccessful submission", e);
		} catch (IOException e) {
			LOGGER.severe("Unsuccessful file operation: " + e.getMessage());
			throw new StreamRunnerException("File operation could not be executed -> unsuccessful submission", e);
		}
	}

	/**
	 * This method merges the configuration coming from the UI (~User) and the default values stored in the resources.
	 *
	 * @return merged configuration for HDFS + Spark
	 */
	private Properties buildMergedConfig(Properties config) {
		// Load default config first
		File sparkConfig = PluginInitStreaming.getSparkConfig().toFile();
		Properties merged = new Properties();
		try {
			merged.load(new FileInputStream(sparkConfig));
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Could not load default configuration for Spark", e);
		}

		// Merge UI config into it
		config.forEach((key, value) -> merged.setProperty((String) key, (String) value));

		return merged;
	}

	/**
	 * Serializes the configuration into a temporary file (by simply storing them in a single key-value file)
	 *
	 * @return file object to this new, temporary file
	 */
	private File saveConfig(Properties config) throws IOException {
		// Save configuration into a temporary file (format: "key<TAB>value<NEWLINE>")
		File file = StreamRunnerUtil.createTempFile("rm-spark_", ".conf").toFile();
		try (FileWriter writer = new FileWriter(file)) {
			for (Map.Entry<Object, Object> entry : config.entrySet()) {
				String key = (String) entry.getKey();
				String value = (String) entry.getValue();
				writer.write(format(SparkConstants.SPARK_CONFIG_LINE_PATTERN, key, value));
			}
		}

		return file;
	}

	/**
	 * @return newly build instance, properly configured
	 * @throws URISyntaxException
	 */
	private HdfsHandler buildHdfsHandler() throws URISyntaxException {
		LOGGER.fine("Building HdfsHandler");
		Configuration hdfsConfig = new Configuration();

		// Add arbitrary HDFS properties
		this.config.entrySet()
			.stream()
			.forEach(entry -> hdfsConfig.set((String) entry.getKey(), (String) entry.getValue()));

		return new HdfsHandler(this.config.getProperty(RM_CONF_HDFS_URI), hdfsConfig);
	}

	/**
	 * Submits a job via Apache-Livy REST API
	 *
	 * @param hdfsJarDst
	 * @param name
	 * @param args
	 */
	private void submitViaREST(String hdfsJarDst, String name, List<String> args) throws IOException {
		// Build configuration map without Apache-Livy "blacklisted" configs
		Map<String, String> conf = this.config.entrySet()
			.stream()
			.filter(entry -> !SparkConstants.LIVY_BLACKLISTED_PROPERTIES.contains(entry.getKey()))
			.collect(Collectors.toMap(
				entry -> (String)entry.getKey(),
				entry -> (String)entry.getValue()
			));

		// Submit job via REST
		SubmitRequest request = new SubmitRequest(hdfsJarDst, SparkConstants.SPARK_MAIN_CLASS, name, args, conf);
		HttpEntity resp = restClient.post(
			SparkConstants.LIVY_SUBMIT_URL,
			toJson(request),
			ContentType.APPLICATION_JSON,
			"Could not initiate job via Apache-Livy");
		LOGGER.info("Spark submission: " + restClient.getResponseBody(resp));
	}

}