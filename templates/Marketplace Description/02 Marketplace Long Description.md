# Streaming

The extension adds Operators to design and deploy streaming analytic process on Flink or Spark.

This is an alpha version. Please use it carefully, as features are still heavily under development.

The streaming analytic process is designed inside the Streaming Nest operator. Processes are
 deployed by creating and providing a connection object to the corresponding cluster.

Processes designed are platform independent. They can be deployed on other platforms (changing
 also from Flink to Spark and vise-versa) by just changing the provided connection object.

The extension provides the following Operators:

- Streaming
  - Financial Server
    - Get Quote Symbols
    - Get Quotes
    - Get Depths
  - Kafka
    - Read Kafka Topic
    - Write Kafka Topic 
  - Streaming Optimization
  - Streaming Nest
  - Aggregate
  - Duplicate Stream
  - Filter Stream
  - Join Streams
  - Connect Streams
  - Kafka Sink
  - Kafka Source
  - Map Stream
  - Select Stream
  - Parse Field Stream
  - Stringify Field Stream
  - Synopsis Data Engine
  - Athena Online Machine Learning Engine
  - Complex Event Forecasting Engine
  - Maritime Event Detection

  

**Version 0.1.0 (2020-10-26)**

- Initial alpha version with the main functionality implemented.
- Main functionality for the deployment of streaming analytic processes implemented
    - Added the Streaming Nest operator for designing and deploying streaming analytic processes
- Added streaming operators to pull and push from/to Kafka (Kafka Source, Kafka Sink)
- Added operators to pull from Kafka and convert to ExampleSet (Read Kafka Topic) and to convert
 ExampleSet to data events and push to Kafka (Write Kafka Topic)
- Added operators to access the financial data server from SPRING (Get Quote Symbols, Get Quotes
, Get Depths)
- Added streaming operators to perform several basic streaming functionalities (Aggregate Stream
, Duplicate Stream, Join Stream, Connect Stream, Filter Stream, Map Stream, Select Stream, Parse
 Field Stream, Stringify Stream)
- Added integration with the INFORE Optimizer (Streaming optimization operator)
- Added integration with different INFORE Components (Synopsis Data Engine, Athena Online Machine
 Learning Engine, Complex Event Forecasting Engine, Maritime Event Detection) 
