
# Write Kafka Topic

Additional Tags: 

Tags to be removed:

Operator Key: streaming:write_kafka_topic

Group: Kafka

## Description

This operator sends all examples of an ExampleSet as messages to a Kafka topic.

It can either send all examples directly after each other, or can wait a specified amount of time between each message.
The examples can be either written as a plain string or as a JSON formatted message.

## Tutorial Process

#### Tutorial 1 (Simple Write Kafka Topic)

In this tutorial process the usage of the Write Kafka Topic operator is demonstrated.

```xml
<process version="9.8.000">
  <context>
    <input/>
    <output/>
    <macros/>
  </context>
  <operator activated="true" class="process" compatibility="9.8.000" expanded="true" name="Process">
    <parameter key="logverbosity" value="init"/>
    <parameter key="random_seed" value="2001"/>
    <parameter key="send_mail" value="never"/>
    <parameter key="notification_email" value=""/>
    <parameter key="process_duration_for_mail" value="30"/>
    <parameter key="encoding" value="SYSTEM"/>
    <process expanded="true">
      <operator activated="true" class="retrieve" compatibility="9.8.000" expanded="true" height="68" name="Retrieve Titanic" width="90" x="246" y="85">
        <parameter key="repository_entry" value="//Samples/data/Titanic"/>
        <description align="center" color="transparent" colored="false" width="126">Retrieve the Titanic Samples data set</description>
      </operator>
      <operator activated="true" class="streaming:write_kafka_topic" compatibility="0.1.000-SNAPSHOT" expanded="true" height="68" name="Write Kafka Topic" width="90" x="715" y="85">
        <parameter key="kafka_cluster" value="localhost"/>
        <parameter key="kafka_port" value="9092"/>
        <parameter key="kafka_topic" value="output"/>
        <parameter key="attribute_separator" value=";"/>
        <parameter key="bulk_sending" value="false"/>
        <parameter key="message_interval" value="1"/>
        <parameter key="message_format" value="JSON"/>
        <description align="center" color="transparent" colored="false" width="126">Push the Examples as individual events to the Kafka topic</description>
      </operator>
      <connect from_op="Retrieve Titanic" from_port="output" to_op="Write Kafka Topic" to_port="input"/>
      <portSpacing port="source_input 1" spacing="0"/>
      <portSpacing port="sink_result 1" spacing="0"/>
    </process>
  </operator>
</process>
```

## Parameters

#### kafka cluster

The address of the Kafka cluster.

#### kafka port

The port of the Kafka cluster.

#### kafka topic

The name of the Kafka topic which should be read.

#### bulk sending

Should all messages be send directly?
If true, each example will still be send as an individual message, but directly after each other.
If false there will be a delay specified by the "message interval" parameter.

#### message interval

Delay between the sending of each example in milliseconds.

#### message format

The format of the message send.
It will be either a JSON format string with the attribute names and values or a simple string that contains only the attribute values.

#### attribute separator

If the message format is a single string, this is the separator between the attributes.


## Input

#### example set (Data Table)

The input ExampleSet which should be pushed to the Kafka Topic

## Output

#### out (Data Table)

The unchanged ExampleSet is passed through.