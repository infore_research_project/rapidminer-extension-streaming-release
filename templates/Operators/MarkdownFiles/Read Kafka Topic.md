
# Read Kafka Topic

Additional Tags: 

Tags to be removed:

Operator Key: streaming:read_kafka_topic

Group: Kafka

## Description

This operator reads the messages from kafka topic on a specific Kafka cluster.

It can either retrieve all previous messages available on this topic, or can collect new incoming messages.
New messages are either collected for a specified amount of time or until a specific number of messages are retrieved.

## Tutorial Process

#### Tutorial 1 (Train and Apply Clustering on data from Kafka Topic)

In this tutorial process the usage of the Read Kafka Topic operator is demonstrated.

```xml
<process version="9.8.000">
  <context>
    <input/>
    <output/>
    <macros/>
  </context>
  <operator activated="true" class="process" compatibility="9.8.000" expanded="true" name="Process" origin="GENERATED_TUTORIAL">
    <parameter key="logverbosity" value="init"/>
    <parameter key="random_seed" value="2001"/>
    <parameter key="send_mail" value="never"/>
    <parameter key="notification_email" value=""/>
    <parameter key="process_duration_for_mail" value="30"/>
    <parameter key="encoding" value="SYSTEM"/>
    <process expanded="true">
      <operator activated="true" class="streaming:read_kafka_topic" compatibility="0.1.000-SNAPSHOT" expanded="true" height="68" name="Read Kafka Topic" width="90" x="313" y="34">
        <parameter key="kafka_cluster" value="localhost"/>
        <parameter key="kafka_port" value="9092"/>
        <parameter key="kafka_topic" value="input"/>
        <parameter key="offset_strategy" value="earliest"/>
        <parameter key="retrieval_time_out" value="2"/>
        <parameter key="collection_strategy" value="number"/>
        <parameter key="counter" value="1000"/>
        <parameter key="time_out" value="120"/>
        <description align="center" color="transparent" colored="false" width="126">Read all messages until now to train the clustering model on it</description>
      </operator>
      <operator activated="true" class="concurrency:k_means" compatibility="9.8.000" expanded="true" height="82" name="Clustering" width="90" x="514" y="34">
        <parameter key="add_cluster_attribute" value="true"/>
        <parameter key="add_as_label" value="false"/>
        <parameter key="remove_unlabeled" value="false"/>
        <parameter key="k" value="5"/>
        <parameter key="max_runs" value="10"/>
        <parameter key="determine_good_start_values" value="true"/>
        <parameter key="measure_types" value="MixedMeasures"/>
        <parameter key="mixed_measure" value="MixedEuclideanDistance"/>
        <parameter key="nominal_measure" value="NominalDistance"/>
        <parameter key="numerical_measure" value="EuclideanDistance"/>
        <parameter key="divergence" value="SquaredEuclideanDistance"/>
        <parameter key="kernel_type" value="radial"/>
        <parameter key="kernel_gamma" value="1.0"/>
        <parameter key="kernel_sigma1" value="1.0"/>
        <parameter key="kernel_sigma2" value="0.0"/>
        <parameter key="kernel_sigma3" value="2.0"/>
        <parameter key="kernel_degree" value="3.0"/>
        <parameter key="kernel_shift" value="1.0"/>
        <parameter key="kernel_a" value="1.0"/>
        <parameter key="kernel_b" value="0.0"/>
        <parameter key="max_optimization_steps" value="100"/>
        <parameter key="use_local_random_seed" value="false"/>
        <parameter key="local_random_seed" value="1992"/>
      </operator>
      <operator activated="true" class="streaming:read_kafka_topic" compatibility="0.1.000-SNAPSHOT" expanded="true" height="68" name="Read Kafka Topic (2)" width="90" x="313" y="238">
        <parameter key="kafka_cluster" value="localhost"/>
        <parameter key="kafka_port" value="9092"/>
        <parameter key="kafka_topic" value="input"/>
        <parameter key="offset_strategy" value="latest"/>
        <parameter key="retrieval_time_out" value="2"/>
        <parameter key="collection_strategy" value="number"/>
        <parameter key="counter" value="100"/>
        <parameter key="time_out" value="120"/>
        <description align="center" color="transparent" colored="false" width="126">Read 100 new message to apply the clustering on it</description>
      </operator>
      <operator activated="true" class="apply_model" compatibility="9.8.000" expanded="true" height="82" name="Apply Model" width="90" x="782" y="136">
        <list key="application_parameters"/>
        <parameter key="create_view" value="false"/>
      </operator>
      <connect from_op="Read Kafka Topic" from_port="output data" to_op="Clustering" to_port="example set"/>
      <connect from_op="Clustering" from_port="cluster model" to_op="Apply Model" to_port="model"/>
      <connect from_op="Read Kafka Topic (2)" from_port="output data" to_op="Apply Model" to_port="unlabelled data"/>
      <connect from_op="Apply Model" from_port="labelled data" to_port="result 1"/>
      <portSpacing port="source_input 1" spacing="0"/>
      <portSpacing port="sink_result 1" spacing="0"/>
      <portSpacing port="sink_result 2" spacing="0"/>
    </process>
  </operator>
</process>
```

## Parameters

#### kafka cluster

The address of the Kafka cluster.

#### kafka port

The port of the Kafka cluster.

#### kafka topic

The name of the Kafka topic which should be read.

#### offset strategy

The polling strategy for the topic.

- **earliest**: messages are retrieved beginning the earliest available messages

- **latest**: only new incoming messages are collected

#### retrieval time out

Time out when retrieving old messages.
Typically relatively short, unless retrieving millions of records. Only applicable if the offset strategy is set to "earliest".

#### collection strategy

The strategy to collect new messages. It's either by "duration", meaning the operator will wait and collect all new messages incoming in the next n seconds or "number", meaning it waits until n messages are retrieved.

- **duration**: the operator will wait and collect all new messages incoming in the next n seconds

- **number**: the operator will wait until n messages are retrieved

#### counter

Counter for the collection strategy.
It's either the duration in seconds the operator to wait or the number of messages to collect.

#### time out

If the collection strategy is "number" this is an additional time out, to prevent the operator waiting too long until enough messages are retrieved, for example in case the message producer is inactive.


## Input

#### example set (Data Table)

The ExampleSet which contains the time series data as attributes.

## Output

#### out (Data Table)

The ExampleSet with the collected messages.