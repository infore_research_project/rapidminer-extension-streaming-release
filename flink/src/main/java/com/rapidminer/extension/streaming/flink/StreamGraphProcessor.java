/*
 * RapidMiner Streaming Extension
 *
 * Copyright (C) 2019-2020 RapidMiner GmbH
 */
package com.rapidminer.extension.streaming.flink;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rapidminer.extension.streaming.utility.KryoHandler;
import com.rapidminer.extension.streaming.utility.graph.StreamGraph;


/**
 * Main entry point for Flink jobs
 *
 * @author Mate Torok
 * @since 0.1.0
 */
public class StreamGraphProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(StreamGraphProcessor.class);

	/**
	 * Driver logic:
	 * <ol>
	 *     <li>Fetches serialized graph from JAR</li>
	 *     <li>Translates graph into Flink specific DSL and starts its execution</li>
	 *     <li>Start execution</li>
	 * </ol>
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		int argc = args.length;
		if (argc < 1) {
			throw new IllegalArgumentException("Job cannot be started without StreamGraph URI");
		}

		// Fetch arguments
		String graphFileName = args[0];

		// Read graph from JAR
		StreamGraph graph = getGraph(graphFileName);
		String graphName = graph.getName();

		// Create environment + translation manager
		StreamExecutionEnvironment executionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
		FlinkTranslationManager translationManager = new FlinkTranslationManager(executionEnvironment);

		// Visit sources
		LOGGER.info("Starting translation");
		graph.getSources().forEach(source -> source.accept(translationManager));

		// Initiate execution
		LOGGER.info("Starting workflow execution: {}", graphName);
		executionEnvironment.execute(graphName);
	}

	/**
	 * Retrieves the serialized workflow from this JAR
	 *
	 * @param fileName
	 * @return de-serialized workflow (StreamGraph)
	 * @throws IOException
	 */
	private static StreamGraph getGraph(String fileName) throws IOException {
		LOGGER.info("Fetching graph: {}", fileName);

		// Graph file sits in the JAR
		String rootFileName = StringUtils.startsWith(fileName, "/") ? fileName : "/" + fileName;
		InputStream graphFileStream = StreamGraphProcessor.class.getResourceAsStream(rootFileName);
		if (graphFileStream == null) {
			LOGGER.error("Graph could not be found: {}", rootFileName);
			throw new IOException("Graph not found: " + rootFileName);
		}

		// Deserialize file(-stream)
		return new KryoHandler().read(graphFileStream, StreamGraph.class);
	}

}